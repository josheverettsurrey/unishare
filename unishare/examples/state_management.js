import React from 'react';

// We want to import makeAutoObservable from mobx so that if we use this
// store in our application, we can use the mobx decorators to make it
// observable. Which means it will re-render when any of its properties
// change.
import { makeAutoObservable } from "mobx";

// We want to import observer from mobx-react so that we can use the
// observer decorator to make our component reactive to changes in the stores
// that it depends on.
import { observer } from "mobx-react-lite";

// Firstly we need to create a store.
// The store is a singleton object that holds the state of the application or a feature.

// We can define a class that represents the store. In practice this store should
// be exported so it can be imported by other components.
class _ExampleStore {

  // We can define some state that the store will hold, that can be accessed,
  // updated and changed throughout the application/feature.
  // Here we have a property that represents the current users name.
  name;

  // In the constructor we can set the initial state of the store.
  constructor() {
    makeAutoObservable(this);
    // So here we are setting the name to be blank as the default.
    this.name = '';
  }

  // We can define methods that will be used to update the state of the store.
  setName(newName) {
    this.name = newName;
  }
}


// Now we are going to instantiate the store. This is done so that we can
// include this in the application global store context below.
const _exampleStore = new _ExampleStore();

// Now we are going to register the example store on the global store context.
// We do this so that we can access the store from anywhere in the application.
// This allows our components to use this store and update it.

// In practice we need to export this StoreContext so that it can be used 
// in our application.
const _StoreContext = React.createContext({ _exampleStore });


/// Now we can create 2 different screens that will be used to show the current
// state of the store. We can also update the state of the store. And show that 
// the state of the store remains the same when we navigate to another screen.
// this is the key reason we have state management.


// Now we are going to create a component that will be used to show the current
// state of the store. And that we can change its contents.
const _page1 = () => {
  // We can use the useContext hook to get the current state of the store.
  const { _exampleStore } = React.useContext(_StoreContext);

  // We can then use the current state of the store and extract the name.
  const name = _exampleStore.name;

  // We can define a function that will be used to update the state of the store.
  const handleChange = (event) => {
    // Setting the name value in the store to be what is entered in the input.
    _exampleStore.name = event.target.value;
  }


  // Returning jsx that will be rendered to the screen.
  return (
    <div>
      {/* Showing the name of the component that we are rendering */}
      <h1>Example Page 1</h1>

      {/* Showing the current state of the store */}
      <p>The current value of the name property is {name}.</p><br />

      {/* Showing the input field that we can use to update the state of the store */}
      <input value={name} onChange={handleChange} />
    </div>
  );
}

// Exporting the component that we created above, while wrapping it with an observer so that it will
// re-render when the state of the store changes.
export const _ExamplePage1 = observer(_page1);


// Now we are going to create a component that will be used to show the current
// state of the store. And that we can change its contents.
const _page2 = () => {
  // We can use the useContext hook to get the current state of the store.
  const { _exampleStore } = React.useContext(_StoreContext);

  // We can then use the current state of the store and extract the name.
  const name = _exampleStore.name;

  // We can define a function that will be used to update the state of the store.
  const handleChange = (event) => {
    // Setting the name value in the store to be what is entered in the input.
    _exampleStore.name = event.target.value;
  }


  // Returning jsx that will be rendered to the screen.
  return (
    <div>
      {/* Showing the name of the component that we are rendering */}
      <h1>Example Page 2</h1>

      {/* Showing the current state of the store */}
      <p>The current value of the name property is {name}.</p><br />

      {/* Showing the input field that we can use to update the state of the store */}
      <input value={name} onChange={handleChange} />
    </div>
  );
}
// Exporting the component that we created above, while wrapping it with an observer so that it will
// re-render when the state of the store changes.
export const _ExamplePage2 = observer(_page2);

// Now goto the pages/examples/state_management.jsx file to see how we can use the store in our application.