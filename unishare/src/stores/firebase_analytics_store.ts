// Imports firebase components needed to use firebase analytics
import { initializeApp } from "firebase/app";
import { getAnalytics, logEvent } from "firebase/analytics";
import {firebaseConfig} from '../utils/firebaseConfig';

// Initialize Firebase
const app = initializeApp(firebaseConfig);
var analytics;

// Checks that the window is defined to avoid a ReferenceError when the store context
// is used on a page without a window, this is due to firebase version 9 not checking by default
if (typeof window !== "undefined") {
  analytics = getAnalytics(app);
}

// Creates an Analytics Store that will be used to log events to firebase
export class AnalyticsStore {

    // Logs a search event
    logSearch(content: {}) {
        logEvent(analytics, "user_search", content);
        console.log("Logged Search event");
    }

    // Logs a search event
    logDownload(content: {}) {
        logEvent(analytics, "file_download", content);
        console.log("Logged Download event");
    }

    logPageView(content: {}){
      logEvent(analytics, "page_view", content);
      console.log("Logged Page View event");
    }

}
