import { makeAutoObservable } from "mobx";
import { Subscription } from "rxjs";
import { ServerConnectivityEvent, ServerConnectivityService } from "src/services/server_connectivity_service";



export class ServerConnectivityState { }

export class ServerConnectivityStateSuccess extends ServerConnectivityState { }

export class ServerConnectivityStateError extends ServerConnectivityState { }



export class ServerConnectivityStore {

  public _state: ServerConnectivityState = new ServerConnectivityStateSuccess();

  private _serverConnectivitySubscription: Subscription;

  constructor(_serverConnectivityService: ServerConnectivityService) {
    makeAutoObservable(this);

    this._serverConnectivitySubscription = _serverConnectivityService._serverConnectivityStreamController.subscribe(event => {
      this.state = event.connected ? new ServerConnectivityStateSuccess() : new ServerConnectivityStateError();
    });

  }

  public get state() {
    return this._state;

  }

  public set state(value: ServerConnectivityState) {
    this._state = value;
  }

  public dispose() {
    this._serverConnectivitySubscription.unsubscribe();
  }
}