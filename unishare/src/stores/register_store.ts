import { makeAutoObservable } from "mobx";
import { RegisterRequest } from "src/entities/requests/register_request";
import { IAuthenticationService } from "src/services/authentication_service";
import { ifBrowser } from "src/utils/global_utils";

export abstract class RegisterStoreState { }

export class RegisterStoreStateInitial extends RegisterStoreState { }

export class RegisterStoreStateSuccess extends RegisterStoreState {
  constructor(public message: string) {
    super()
  }
}

export class RegisterStoreStateLoading extends RegisterStoreState { }

export class RegisterStoreStateError extends RegisterStoreState {
  constructor(public error: string) {
    super();
  }
}


export class RegisterStore {

  private _state: RegisterStoreState = new RegisterStoreStateInitial();
  private authenticationService: IAuthenticationService;

  constructor(
    authenticationService: IAuthenticationService
  ) {
    makeAutoObservable(this);
    this.authenticationService = authenticationService;
  }

  dismissError() {
    this.state = new RegisterStoreStateInitial();
  }


  get state(): RegisterStoreState {
    return this._state;
  }

  set state(newState: RegisterStoreState) {
    this._state = newState;
  }

  async register(registerRequest: RegisterRequest, passwordConfirmation: string) {
    try {

      if (registerRequest.password !== passwordConfirmation) {
        this.state = new RegisterStoreStateError("Your passwords do not match");
        return;
      }

      this.state = new RegisterStoreStateLoading();
      const response = await this.authenticationService.register(registerRequest);

      if (response.isSuccessful) {
        this.state = new RegisterStoreStateSuccess(response.content.message);
        ifBrowser(() => { window.location.href = '/home/home'; });
        return response.content;
      }
      else {
        this.state = new RegisterStoreStateError(response.failure);
      }
    }
    catch (e) {
      this.state = new RegisterStoreStateError(e);
    }
  }
}