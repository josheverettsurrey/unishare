import { Subscription } from "rxjs/internal/Subscription";
import { User } from "src/entities/core/user";
import { IAuthenticationService } from "src/services/authentication_service";
import { CurrentUserService, OnCurrentUserPropertyChangedEvent } from "src/services/current_user_service";

export class HomeStoreState {
}

export class HomeStoreStateUnauthenticated extends HomeStoreState { }

export class HomeStoreStateAuthenticated extends HomeStoreState {
  public user: User;

  constructor(user: User) {
    super();
    this.user = user;
  }
}



export default class HomeStore {

  private _state: HomeStoreState;

  private _currentUserService: CurrentUserService;
  private _currentUserSubscription: Subscription;

  constructor(currentUserService: CurrentUserService) {
    this._currentUserService = currentUserService;
    this._currentUserSubscription = this._currentUserService.userStreamController.subscribe(this.onCurrentUserPropertyChangedEvent)

    if (this._currentUserService.user) {

      this.state = new HomeStoreStateAuthenticated(this._currentUserService.user);
    }
    else {
      this.state = new HomeStoreStateUnauthenticated();
    }

  }

  private onCurrentUserPropertyChangedEvent = (event: OnCurrentUserPropertyChangedEvent) => {
    this.state = new HomeStoreStateAuthenticated(event.user);
  }

  get state(): HomeStoreState {
    return this._state;
  }

  set state(newState: HomeStoreState) {
    this._state = newState;
  }

  dispose(): void {
    this._currentUserSubscription.unsubscribe();
  }

}