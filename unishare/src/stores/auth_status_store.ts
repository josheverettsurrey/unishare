import { IAuthenticationService } from "src/services/authentication_service";
import { CurrentUserService } from "src/services/current_user_service";

export class AuthStatusStore {

  private authenticationService: IAuthenticationService;

  constructor(authenticationService: IAuthenticationService) {
    this.authenticationService = authenticationService;
  }

  get isAuthenticated(): boolean {
    return this.authenticationService.isAuthenticated;
  }
}