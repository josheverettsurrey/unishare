import ChatService, {SocketConnectionChangedEvent, SocketConnectionChangedEventType} from "../services/chat_service";
import {makeAutoObservable} from "mobx";
import {GlobalException} from "../entities/exceptions/global";
import {ChatMessage} from "../entities/core/chat_message";
import {CurrentUserService} from "../services/current_user_service";
import {User} from "../entities/core/user";
import {University} from "../entities/core/university";

interface ChatRoomStateParams {
    messages?: ChatMessage[];
    isLoading: boolean;
    successful: boolean;
    errorMessage?: string;
    members?: []
}


export class ChatRoomState{

    /**
     * List of the users in the chat room
     */
    public readonly members: [];

    /**
     * List of messages that will be shown in the chat room.
     */
    public readonly messages: ChatMessage[] | null;

    /**
     * Whether the state is loading - whether we should show a loading view to the user
     */
    public readonly isLoading: boolean;

    /**
     * Whether the state is successful, and we have received a list of messages
     */
    public readonly isSuccessful: boolean;

    /**
     * The error message if there is one.
     */
    public readonly errorMessage?: string;

    /**
     * Whether an error has occurred or not.
     */
    public get hasError(): boolean{
        return this.errorMessage != null;
    }

    private constructor(params: ChatRoomStateParams) {
        this.messages = params.messages;
        this.isLoading = params.isLoading;
        this.isSuccessful = params.successful;
        this.errorMessage = params.errorMessage;
    }

    static initial(): ChatRoomState {
        return new ChatRoomState({messages: null, isLoading: false, successful: false});
    }

    static loading(): ChatRoomState{
        return new ChatRoomState({messages: null, isLoading: true, successful: false});
    }

    static connected(): ChatRoomState{
        return new ChatRoomState({messages: null, isLoading: false, successful: true});
    }

    static content(messages: ChatMessage[]): ChatRoomState{
        return new ChatRoomState({messages: messages, isLoading: false, successful: true});
    }

    static error(failure: string) : ChatRoomState {
        return new ChatRoomState({messages: null, isLoading: false, successful: false, errorMessage: failure});
    }

    public setMembers(members: []) {
        return new ChatRoomState({members: members ,messages: this.messages , isLoading: this.isLoading, successful: this.isSuccessful, errorMessage: this.errorMessage})
    }

    public update({messages, members} : {messages:  ChatMessage[], members?: []}) {
        return new ChatRoomState({members: members ?? this.members, messages: (this.messages ?? []).concat(messages), isLoading: this.isLoading, successful: this.isSuccessful, errorMessage: this.errorMessage})
    }

    public disconnected(){
        return new ChatRoomState({messages: this.messages, isLoading: false, successful: false, errorMessage: this.errorMessage})
    }

}

export class ChatRoomStore {

    /**
     * The current state of the chat room.
     */
    private _state: ChatRoomState = ChatRoomState.initial();

    /**
     * The service that will be used to send messages
     * @private
     */
    private readonly chatService: ChatService;
    private readonly currentUserService: CurrentUserService;

    private readonly socketStream;

    /**
     * The id of the room that the user has opened.
     * @private
     */
    private readonly roomId;

    constructor(chatService: ChatService, currentUserService: CurrentUserService) {
        makeAutoObservable(this);

        this.chatService = chatService;
        this.currentUserService = currentUserService;

        /**
         * Handle the changes in the socket connection.
         */
        this.socketStream = chatService.socketConnectionController.subscribe(event => {
            this.handleSocketChanges(event);
        });
    }

    set state(newState: ChatRoomState){
        this._state = newState;
    }

    get state() {
        return this._state;
    }

    get currentUser() : User {
        return this.currentUserService.user;
    }


    async joinRoom(id: string): Promise<void>{
        try{
            this.state = ChatRoomState.loading();

             await this.chatService.connect(id);
        }
        catch(e){
            this.state = ChatRoomState.error(GlobalException.UnknownError);
        }
    }

    handleSocketChanges (event: SocketConnectionChangedEvent) {
        switch (event.type){
            case SocketConnectionChangedEventType.CONNECTED:
                this.state = ChatRoomState.connected( );
                break;
            case SocketConnectionChangedEventType.DISCONNECTED:
                this.state = this.state.disconnected( );
                break;
            case SocketConnectionChangedEventType.INCOMING_MESSAGE:
                this.state = this.state.update({messages: event.messages ?? [], members: event.members});
                break;
            case SocketConnectionChangedEventType.UPDATE:
                this.state = this.state.update({messages: event.messages, members: event.members});
                break;
            case SocketConnectionChangedEventType.ERROR:
                console.log('emitting error',event.errorMessage )
                this.state = ChatRoomState.error(event.errorMessage ?? GlobalException.ServerError);
                break;
        }
    }

    send(message: string, roomId: string) {
        this.chatService.sendMessage(message, roomId);
    }

}
