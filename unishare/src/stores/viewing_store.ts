import {DocumentService} from "../services/document_service";
import {GlobalException} from "../entities/exceptions/global";
import {makeAutoObservable} from "mobx";
import {FetchRequest} from "../entities/requests/fetch_request";
import {UnishareDocument} from "../entities/core/unishare_document";

interface ViewingStoreStateParams{
    file?: any;
    document?: UnishareDocument;
    isLoading: boolean;
    error?: string;
    success?: boolean;

}

export class ViewingStoreState {

    // The file that will be shown
    file: any;

    // The meta data about the file such as title, description, university etc.
    _document: UnishareDocument;

    // Private boolean that represents whether the
    // page should show a loading state. (private visibility so that it cannot be mutated
    // outside this class). We have a public getter for this property.
    private readonly _isLoading: boolean;

    // String property that represents the error message that the needs to be displayed on
    // the page.
    private readonly _error?: string;

    // Boolean property that represents the success of the upload
    private readonly _success: boolean;

    constructor(params: ViewingStoreStateParams){
        this.file = params.file;
        this._isLoading = params.isLoading;
        this._error = params.error;
        this._document = params.document;
        this._success = params.success ?? false;
    };

    // Initial named constructor for better readability
    static initial() : ViewingStoreState{
        return new ViewingStoreState({file: null, isLoading: false});
    }

    // Loading state constructor
    static loading(): ViewingStoreState{
        return new ViewingStoreState({file: null, isLoading: true});
    }

    static data(file: File): ViewingStoreState{
        return new ViewingStoreState({file: file, isLoading: false, error: null});
    }

    static error(failure: string) {
        return new ViewingStoreState({isLoading: false, error: failure});
    }

    static success(file, document: UnishareDocument){
        return new ViewingStoreState({file: file, document: document, isLoading: false, error: null, success: true})
    }

    // Getter to check if the results are empty
    public get isEmpty(): boolean{
        return this.file == null;
    }

    public get isLoading(): boolean {
        return this._isLoading;
    }

    public get errorMessage(): string {
        return this._error;
    }

    public get hasError(): boolean {
        return this._error != null;
    }

    public get success() : boolean{
        return this._success;
    }

    public get document(): UnishareDocument {
        return this._document;
    }
}

export class ViewingStore {

    private _state = ViewingStoreState.initial();
    private readonly _documentService: DocumentService;
    constructor(documentService: DocumentService) {
        makeAutoObservable(this);
        this._documentService = documentService;
    }

    get state(): ViewingStoreState{
        return this._state;
    }
    set state(newState) {
        this._state = newState;
    }

    async fetch(request: FetchRequest): Promise<void> {
        if (request.id== null ) return;
        try{
            this.state = ViewingStoreState.loading();

            const response = await this._documentService.fetch(request);

            if (!response.isSuccessful) {
                this.state = ViewingStoreState.error(response.failure);
                return;
            }

            this.state = ViewingStoreState.success(response.content.file, response.content.document);
        }

        catch(e){
            this.state = ViewingStoreState.error(GlobalException.UnknownError);
        }
    }
}