import { makeAutoObservable } from "mobx";
import {UploadRequest} from "../entities/requests/upload_request";
import {GlobalException} from "../entities/exceptions/global";
import {DocumentService} from "../services/document_service";

interface UploadStoreStateParams{
    file?: File;
    isLoading: boolean;
    error?: string;
    success?: boolean;
}

export class UploadStoreState {

    // List of search results
    file: File;

    // Private boolean that represents whether the
    // page should show a loading state. (private visibility so that it cannot be mutated
    // outside this class). We have a public getter for this property.
    private readonly _isLoading: boolean;

    // String property that represents the error message that the needs to be displayed on
    // the page.
    private readonly _error?: string;

    // Boolean property that represents the success of the upload
    private readonly _success: boolean;

    constructor(params: UploadStoreStateParams){
        this.file = params.file;
        this._isLoading = params.isLoading;
        this._error = params.error;
        this._success = params.success ?? false;
    };

    // Initial named constructor for better readability
    static initial() : UploadStoreState{
        return new UploadStoreState({file: null, isLoading: false});
    }

    // Loading state constructor
    static loading(): UploadStoreState{
        return new UploadStoreState({file: null, isLoading: true});
    }

    static data(file: File): UploadStoreState{
        return new UploadStoreState({file: file, isLoading: false, error: null});
    }

    static error(failure: string, file?) {
        return new UploadStoreState({file: file, isLoading: false, error: failure});
    }

    static success(file){
        return new UploadStoreState({file: file, isLoading: false, error: null, success: true})
    }

    // Getter to check if the results are empty
    public get isEmpty(): boolean{
        return this.file == null;
    }

    public get isLoading(): boolean {
        return this._isLoading;
    }

    public get errorMessage(): string {
        return this._error;
    }

    public get hasError(): boolean {
        return this._error != null;
    }

    public get success() : boolean{
        return this._success;
    }

}

// This is a store used for the file uploading component / store that is used
// to hold the file that has been selected to be uploaded (empty by default).
// This is the store that will be accessed by the application when an upload attempt
// is made.
export class UploadStore {
    //defining private variables that will be used to store information associated with the file
    //and the file itself
    private _file: File;

    private readonly _documentService: DocumentService;

    private _state: UploadStoreState = UploadStoreState.initial();

    // The constructor is called when the object is created.
    constructor(documentService: DocumentService) {
        // Here we are making the store observable so that we can use mobx to
        // observe the changes in the store and have our components update when this
        // store mutates.
        makeAutoObservable(this);
        // We are setting the file to null initially as there is no file to be uploaded by default.
        this._file = null;
        this._documentService = documentService;
    }

    // Defining a setter for the file object so that when the file has been uploaded successfully the
    // store will be updated to reflect that.
    set file(fileObject: File) {

        if (!fileObject || fileObject.type != 'application/pdf') {
            this.state = UploadStoreState.error('File is either empty or the file type is not a PDF.', this.state.file)
            return;
        }
        this.state = UploadStoreState.data(fileObject);
    }

    set state(newState: UploadStoreState){
        this._state = newState;
    }

    get state(){
        return this._state;
    }

    // Defines an upload file function for setting the file to be uploaded, returns true if there were no issues with
    // the upload and false if there were
    // Performs checks on the file (is it not null and is it a pdf type)
    public async upload(request: UploadRequest) : Promise<void>{

        if (this.state.isEmpty) {
            this.state = UploadStoreState.error('You need to upload a file to send it.', this.state.file)
            return;
        }
        else if (request.title.trim() == null || request.title.length == 0){
            this.state = UploadStoreState.error('You need to enter a title.', this.state.file);
            return;
        }

        try{
            this.state = UploadStoreState.loading();
            const response = await this._documentService.upload(request);
            if  (!response.isSuccessful) {
                this.state = UploadStoreState.error(response.failure!, this.state.file);
                return;
            }
            this.state = UploadStoreState.success(this.state.file);

        }catch(e){
            console.log(e)
            this.state = UploadStoreState.error(GlobalException.UnknownError, this.state.file);
        }
    }
}
