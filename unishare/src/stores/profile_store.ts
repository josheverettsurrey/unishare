import { makeAutoObservable } from "mobx";
import { User } from "src/entities/core/user";
import { UpdateUserRequest } from "src/entities/requests/update_user_request";
import { IAuthenticationService } from "src/services/authentication_service";
import { CurrentUserService } from "src/services/current_user_service";
import { UserUpdateService } from "src/services/user_update_service";


export class ProfileStoreState {
  public user: User;
  public error?: string;
  public loading: boolean;
  constructor(user: User, error?: string, loading?: boolean) {
    this.user = user;
    this.error = error;
    this.loading = loading ?? false;
  }

  setLoading(newVal: boolean) {
    this.loading = newVal;
  }

  setError(error: string) {
    this.error = error;
  }

  copyWith({ user, error, loading }: Partial<ProfileStoreState>) {
    return new ProfileStoreState(user ?? this.user, error ?? this.error, loading ?? this.loading);
  }
}

export class ProfileStore {

  private currentUserService: CurrentUserService;
  private updateUserService: UserUpdateService;
  private authentictionService: IAuthenticationService;
  private _state: ProfileStoreState = new ProfileStoreState(undefined, undefined, false);

  constructor(
    currentUserService: CurrentUserService,
    updateUserService: UserUpdateService,
    authenticationService: IAuthenticationService,
  ) {
    makeAutoObservable(this);
    this.currentUserService = currentUserService;
    this.updateUserService = updateUserService;
    this.authentictionService = authenticationService;
    this.state = new ProfileStoreState(currentUserService.user);
  }

  set state(newState: ProfileStoreState) {
    this._state = newState;
  }

  get state(): ProfileStoreState {
    return this._state;
  }

  async updateProfile(newUser: UpdateUserRequest, passwordConfirmation?: string) {

    if (this.state.user.displayName == newUser.user.displayName && this.state.user.email == newUser.user.email && !newUser.password) {
      this.state = this.state.copyWith({ error: "You haven't made any changes yet." });
      return;
    }

    else if (newUser.password && newUser.password !== passwordConfirmation) {
      this.state = this.state.copyWith({ error: "Your password and confirmation need to be the same." });
      return;
    }

    // At this point the user has made a change to something (displayName, email or password);
    this.state = this.state.copyWith({ loading: true });
    const response = await this.updateUserService.updateUser(newUser);

    if (response.isSuccessful) {
      this.currentUserService.setUser(response.content.user);
      this.state = new ProfileStoreState(response.content.user, null)
    }
    else {
      this.state = new ProfileStoreState(this.state.user, response.failure);
    }


  }

  async logout(): Promise<void> {
    try {
      await this.authentictionService.logout();

    }
    catch (e) {
      this._state = this._state.copyWith({ error: 'Unable to log you out' });
    }
  }

}