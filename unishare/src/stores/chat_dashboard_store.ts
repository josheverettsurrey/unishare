
import ChatService from "../services/chat_service";
import {makeAutoObservable} from "mobx";
import {ChatRoom} from "../entities/core/chat_room";
import {GlobalException} from "../entities/exceptions/global";
import {CreateRoomRequest} from "../entities/requests/create_room_request";

interface ChatDashboardStateParams {
    rooms?: ChatRoom[];
    isLoading: boolean;
    success: boolean;
    errorMessage?: string;
    hardError?: boolean;
}


export class ChatDashboardState {

    /**
     * List of messages that will be shown in the chat room.
     */
    public readonly rooms: ChatRoom[] | null;

    /**
     * Whether the state is loading - whether we should show a loading view to the user
     */
    public readonly isLoading: boolean;

    /**
     * Whether the state is successful, and we have received a list of messages
     */
    public readonly success: boolean;

    /**
     * The error message if there is one.
     */
    public readonly errorMessage?: string | null;

    /**
     * Whether error should be the view of the
     * screen for example a 'hard' error would be if the chats didn't
     * load - if this is the case we want to show this error as the
     * main focus of the screen.
     *
     * A 'soft' error would be like there was a problem creating a
     * new room, the contents of this error should be shown as an overlay
     */
    public readonly hardError: boolean;

    /**
     * Whether an error has occurred or not.
     */
    public get hasError(): boolean{
        return this.errorMessage != null;
    }

    private constructor(params: ChatDashboardStateParams) {
        this.rooms = params.rooms;
        this.isLoading = params.isLoading;
        this.success = params.success;
        this.errorMessage = params.errorMessage;
        this.hardError = params.hardError ?? true;
    }

    static initial(): ChatDashboardState {
        return new ChatDashboardState({rooms: null, isLoading: false, success: false});
    }

    static loading(): ChatDashboardState{
        return new ChatDashboardState({rooms: null, isLoading: true, success: false});
    }

    static success(rooms: ChatRoom[]): ChatDashboardState{
        return new ChatDashboardState({rooms, isLoading: false, success: true});
    }

    static error(failure: string): ChatDashboardState{
        return new ChatDashboardState({rooms: null, isLoading: false, success: false, errorMessage: failure});
    }

    /**
     * Use when we want to show a loading state over the success
     */
    setLoading(): ChatDashboardState {
        return new     ChatDashboardState({rooms: this.rooms, success: this.success, isLoading: true, errorMessage: this.errorMessage, hardError: this.hardError})
    }


    throwSoftError(failure: string ) : ChatDashboardState {
        return new ChatDashboardState({rooms: this.rooms, success: true, isLoading: false, errorMessage: failure, hardError: false});
    }
}

export class ChatDashboardStore {

    /**
     * The current state of the chat room.
     */
    private _state: ChatDashboardState =  ChatDashboardState.initial();

    /**
     * The service that will be used to fetch all the rooms to display.
     * @private
     */
    private readonly chatService: ChatService;

    public _showingDialog: boolean = false;

    constructor(chatService: ChatService) {
        makeAutoObservable(this);

        this.chatService = chatService;
    }

    set state(newState: ChatDashboardState) {
        this._state = newState;
    }

    get state(): ChatDashboardState{
        return this._state;
    }

    async fetch(): Promise<void> {
        try{
            this.state = ChatDashboardState.loading();

            //Fetch all the chat rooms that have been created for the university
            const response = await this.chatService.fetch();

            // Check to see if the response was successful
            if (!response.isSuccessful) {
                // If not then show an error state with the error from the server
                this.state = ChatDashboardState.error(response.failure!);
                return;
            }

            // If the response was successful then set the state to be successful
            // and pass in the rooms that were returned from the database.
            this.state = ChatDashboardState.success(response.content.rooms);
        }
        catch(e){
            this.state = ChatDashboardState.error(GlobalException.UnknownError);
        }
    }

    async create(request: CreateRoomRequest): Promise<void> {
        // Validate the request
        if (request.name == null || request.name.length == 0) {
             this.state = this.state.throwSoftError('You have entered an invalid name');
             return;
        }

        if (request.moduleCode == null || request.moduleCode.length == 0) {
            this.state = this.state.throwSoftError('You have entered an invalid moduleCode');
            return;
        }

        // Here we can assume that we have been given a valid name and module code.

        // Perform the create request
        try {
            this.state = this.state.setLoading();
            const response = await this.chatService.create(request);
            if (!response.isSuccessful) {
                this.state = this.state.throwSoftError(response.failure!);
                return;
            }
            this.state = ChatDashboardState.success(response.content.rooms);
            this.showingDialog = false;
        }
        catch(e){
            this.state.throwSoftError(GlobalException.UnknownError);
        }
    }

    get showingDialog(): boolean {
        return this._showingDialog;
    }
    set showingDialog(newVal: boolean) {
        this._showingDialog = newVal;
    }

}

