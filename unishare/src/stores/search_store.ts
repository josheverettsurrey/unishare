import {makeAutoObservable} from "mobx";
import {UnishareDocument} from "../entities/core/unishare_document";
import {DocumentService} from "../services/document_service";

// Interface to describe the properties of each SearchStoreState
interface SearchStoreStateParams {
    results: UnishareDocument[];
    isLoading: boolean;
    error?: string;
}

export class SearchStoreState {

    // List of search results
    results: UnishareDocument[];

    // Private boolean that represents whether the
    // page should show a loading state. (private visibility so that it cannot be mutated
    // outside this class). We have a public getter for this property.
    private readonly _isLoading: boolean;

    // String property that represents the error message that the needs to be displayed on
    // the page.
    private readonly _error?: string;

    constructor(params: SearchStoreStateParams){
        this.results = params.results;
        this._isLoading = params.isLoading;
        this._error = params.error;
    };

    // Initial named constructor for better readability
    static initial() : SearchStoreState{
        return new SearchStoreState({results: [], isLoading: false});
    }

    // Loading state constructor
    static loading(): SearchStoreState{
        return new SearchStoreState({results: [], isLoading: true});
    }

    static data(results: UnishareDocument[]): SearchStoreState{
        return new SearchStoreState({results, isLoading: false});
    }

    static error(failure: string) {
        return new SearchStoreState({results: [], isLoading: false, error: failure});
    }

    // Getter to check if the results are empty
    public get isEmpty(): boolean{
        return this.results.length == 0;
    }

    public get isLoading(): boolean {
        return this._isLoading;
    }

    public get errorMessage(): string {
        return this._error;
    }

    public get hasError(): boolean {
        return this._error != null;
    }
}

export class SearchStore {

    private _state : SearchStoreState = SearchStoreState.initial();
private _documentService: DocumentService;

    constructor(documentService: DocumentService) {
        makeAutoObservable(this);
        this._documentService = documentService;
    };

    set state(newState: SearchStoreState) {
        this._state = newState;
    }
    get state() : SearchStoreState {
        return this._state;
    }

    /**
     * The search function is responsible for making a call to the search
     * repository to retrieve Unishare documents that match the search
     * query provided as a parameter.
     *
     * The function will initially emit a loading state before the async
     * call is made so the view can show a loading indicator whilst the call
     * to the repository is being made.
     *
     * The request in the made to the repository that makes the call to the sever
     * to request a list of results for the query and on response we set the state
     * of the store to either an error state or a data state.
     * @param query
     */
    async search(query: string): Promise<void> {
        this.state = SearchStoreState.loading();
        try{
            // Make a call to the repository to search using queried string
            const response = await this._documentService.search(query);

            // Check whether the request was successful or not.
            if(!response.isSuccessful) {
                // if the request returned an error from the server then
                // set the state to an error state with the message to present to the
                // user.
                this.state = SearchStoreState.error(response.failure!);
                return;
            }

            // if the response was a success then set the state to a data state
            // with the results from the server as the result property of the state.
            this.state = SearchStoreState.data(response.content.results);
        }catch(e){
            // If an error was thrown somewhere in the process then show a generic message
            console.log(e);
            this.state = SearchStoreState.error('There was an error whilst searching your query. If this error keeps showing please contact us.');
        }
    }
}