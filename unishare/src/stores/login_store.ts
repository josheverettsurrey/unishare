import { makeAutoObservable } from "mobx";
import { ifBrowser } from "src/utils/global_utils";
import { LoginRequest } from "../entities/requests/login_request";
import { IAuthenticationService } from "../services/authentication_service";

// An abstract representation of the state of the LoginStore.
export abstract class LoginStoreState { }

// An instance of an unauthenticated login state that represents the user has not logged in.
export class LoginStoreStateUnauthenticated extends LoginStoreState { }

// An instance of an authenticated login state that represents the user has logged in.
export class LoginStoreStateAuthenticated extends LoginStoreState { }

// An instance of a loading state for the LoginStore that represents some data loading.
export class LoginStoreStateLoading extends LoginStoreState { }

// An instance of an error state for the LoginStore that represents an errored state that exposes a message property.
export class LoginStoreStateError extends LoginStoreState {
  public error: string;
  constructor(error: string) {
    super();
    this.error = error;
  }
}


// This is a store for the Login logic component / store that will be used
// to maintain the state of the login journey.
// This is the store that will be accessed from the application to check the state of
// the journey and to call methods that act on services.
export class LoginStore {

  private _state: LoginStoreState = new LoginStoreStateUnauthenticated();

  private authenticationService: IAuthenticationService;

  // The constructor is called when the object is created.
  constructor(authenticationService: IAuthenticationService) {
    // Here we are making the store observable so that we can use mobx to 
    // observe the changes in the store and have our components update when this
    // store mutates.
    makeAutoObservable(this);
    this.authenticationService = authenticationService;
  }

  // Here we are defining a setter for the session token so that when the login
  // has been successful, we can store the token in the service, in the future we will be
  // setting the token in the local storage aswell.
  set state(newState: LoginStoreState) {
    this._state = newState;
  }

  get state() {
    return this._state;
  }

  dismissError() {
    this.state = new LoginStoreStateUnauthenticated();
  }


  async login(loginRequest: LoginRequest) {
    try {
      this.state = new LoginStoreStateLoading();
      // Here we are calling the login method from the authentication service.
      const response = await this.authenticationService.login(loginRequest);

      if (response.isSuccessful) {

        // Here we are setting the state to authenticated.
        this.state = new LoginStoreStateAuthenticated();

        ifBrowser(() => { window.location.href = '/home/home'; });

        // Here we are returning the response.
        return response.content;
      }
      else {
        // If the response was not successful then there will be a message that is returned
        // as the failure.
        // We can set the state to the error state and pass in the error message.
        this.state = new LoginStoreStateError(response.failure);
      }

    } catch (e) {

      // We are handling any errors that might occur during the login process.
      this.state = new LoginStoreStateError(e);
    }
  }


}

