import AppImage from "@/components/base/app_image";
import Unauthorized from "@/public/unauthorized.svg";
import AppText from "@/components/base/app_text";
import { AppButton } from "@/components/base/app_button";
import React from 'react';
import Router from "next/router";

export const UnauthorizedPageView = () => {


    return (
        <>
            <title>401</title>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'center',
                alignItems: 'center',
                minHeight: '100vh',
                textAlign: 'center'

            }}>
                <AppImage src={Unauthorized} />
                <AppText text={"Hmm you need to be logged in to do that"} style='heading' />

                <AppText text={'Press the button below to login!'} />

                <AppButton text="RESCUE ME" onPressed={() => { Router.push('/auth/login') }} />


            </div>
        </>);

}

export default UnauthorizedPageView