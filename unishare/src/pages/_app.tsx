
import { useContext, createContext } from 'react';
import { ServerConnectivityStateError } from 'src/stores/server_connectivity_store';
import { IStoreContext, setupContext } from 'src/store_context';
import ServerConnectivityFailed from '../components/global/server_connection_failed';

import '../../styles/globals.scss'
import { observer } from 'mobx-react-lite';
import React from 'react';

// Creating the StoreContext object that will be added to the application context,
// so that we can use it in the application.
// Here we are passing in the stores that are defined in the interface above as we
// have cast the store context to the interface.
export const StoreContext = createContext<IStoreContext>(setupContext());

export const useStoreContext = () => useContext(StoreContext);

const MyApp = ({ Component, pageProps }) => {
  const { serverConnectivityStore } = useStoreContext();

  if (serverConnectivityStore.state instanceof ServerConnectivityStateError) {
    return < ServerConnectivityFailed />;
  }

  return <Component {...pageProps} />;

}


export default observer(MyApp);

