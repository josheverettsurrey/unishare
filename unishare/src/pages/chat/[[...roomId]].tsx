import { useRouter } from "next/router";
import { ChatDashboardState } from "../../stores/chat_dashboard_store";
import AppLoadingSpinner from "@/components/base/app_loading_spinner";
import AppText from "@/components/base/app_text";
import React from 'react';
import { useEffect, useState } from "react";
import { useStoreContext } from "@/pages/_app";
import { observer } from "mobx-react";
import AppDividedLayout from "@/components/global/app_divided_layout";
import ChatWindow from "@/components/chat/chat_window";
import CreateRoomDialog from "@/components/chat/create_room";
import ChatSidebar from "@/components/chat/sidebar";
import { AppLayout } from "@/components/global/app_layout";
import { useRequireAuth } from "../../utils/use_require_auth";
import Router from "next/router";

import { NavBarLoggedInView } from '../../components/navbar/navbar';

const ChatDashboard = observer(() => {

    /**
     *  Make this route a protected route. This will cause an auth check to happen when
     *  the page loads so that any unauthenticated users cannot enter the chat rooms.
     */
    useRequireAuth();

    /**
     * Get the NextJS router so that we can access the roomId from the query.
     */
    const router = useRouter();

    /**
     * Get the room id from the router query
     */
    const roomId = router.query.roomId;

    /**
     * Get the current dashboard store from the store context
     */
    const { chatDashboardStore, authStatusStore } = useStoreContext();
    const isAuthenticated = authStatusStore.isAuthenticated;

    /**
     * Get the current dashboard state
     */
    const state: ChatDashboardState = chatDashboardStore.state;

    /**
     * A use effect that will only be called once when the page loads for the first time
     * and also when the selected roomId changes.
     * This function will get all the chat rooms that the user should be shown for their
     * university
    */
    useEffect(() => {
        // Fetch the chat rooms from the server
        chatDashboardStore.fetch().then();

    }, [roomId]);

    /**
     * If there is an error then we want to show that as there will be no
     * groups to display if there is a hard error.
     * We will continue with loading other components if there is just a soft error
     * as this will want to be displayed on page.
     */
    if (state.hasError && state.hardError) {
        return <AppText text={state.errorMessage} />
    }

    /**
     * If the state is loading then we want to show a loading spinner.
     * We also want to show this when we are in an initial state as we
     * haven't got any content to render.
     */
    if (state.isLoading || !state.success) {
        return <AppLoadingSpinner />
    }

    /**
     * Here we can assume that the store is not loading and the request was successful.
     * We should now have access to the list of chat rooms in the state.
     * If a 'soft' error was thrown then we will be able to access its error using the
     * errorMessage property of the state.
     */
    const rooms = state.rooms!;


    return (
        <>
            <title>Chat</title>

            {isAuthenticated ?
                <>
                    {/* Displays the logged in Navbar as the user must be logged in to access the chat page */}
                    <NavBarLoggedInView />

                    <main style={{ display: 'grid', justifyItems: 'center', justifyContent: 'center', alignItems: 'center', alignContent: 'center' }}>


                        <div style={{ width: '100vw' }}>

                            <AppDividedLayout params={{

                                left: <ChatSidebar rooms={rooms} />,
                                right: <ChatWindow />,
                                rightLarger: true,
                            }} />
                        </div>

                        {/**
             If the user wants to create a new chat room then we show the following component (logic for
             checking if we should show this is handled in the component)
             */}
                        <CreateRoomDialog />

                    </main>
                </>

                // If user is unauthenticated then nothing will be displayed as the RequireAuth check will then redirect the user to the 401 page
                : <></>}



        </>
    );
});


export default ChatDashboard;