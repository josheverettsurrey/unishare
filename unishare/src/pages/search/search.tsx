import { AppLayout } from "../../components/global/app_layout";
import { SearchView } from "../../components/search/search";
import styles from "../../../styles/Search.module.scss";
import { useStoreContext } from "@/pages/_app";
import { NavBarLoggedInView, NavBarNotLoggedInView } from '../../components/navbar/navbar';
import { useEffect } from "react";
import React from 'react';

export const SearchPage = () => {

    const { authStatusStore, analyticsStore } = useStoreContext();
    const isAuthenticated = authStatusStore.isAuthenticated;

    // Logs analytics when the user accesses this page
    useEffect(() => analyticsStore.logPageView("Search"))

    return (
        <div>
            <title>Search</title>
            <AppLayout params={
                {
                    header: <>{/* Checks if user is authenticated and displays the appropriate navbar */}
                        {isAuthenticated ? <NavBarLoggedInView /> :
                            <NavBarNotLoggedInView />}<br />
                        <h1 className={styles.title}>Search</h1>
                        <hr /></>,
                    body: <SearchView />
                }
            } />
        </div>
    );

}

export default SearchPage;