import React from 'react';
import { _ExamplePage1, _ExamplePage2 } from '../../../examples/state_management';

export default function StateManagementExample() {
  return (
    <div>
      <h1>State Management Example</h1><br />

      <h2>The below are 2 different components without any parameter passing between them</h2>
      <p>You can see that the value of the name property updates on both components when either one is changed.<br />
        Ths is because of the state management that we implemented, they are both re-rendering when the state of the
        example store changes. <br />
        When we update the name property we are updating the ExampleStore state which is causing both components to
        re-render.
      </p>
      <br /><br />

      <_ExamplePage1 />

      <_ExamplePage2 />

    </div>
  );
}