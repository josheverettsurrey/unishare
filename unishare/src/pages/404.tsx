import { AppButton } from "@/components/base/app_button";
import AppImage from "@/components/base/app_image";
import AppText from "@/components/base/app_text";
import PageNotFound from '@/public/page_not_found.svg';
import React from 'react';
import Router from "next/router";

export const ErrorPageView = () => {


  return (
    <>
      <title>404</title>
      <div style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        minHeight: '100vh',
        textAlign: 'center'

      }}>
        <AppImage src={PageNotFound} />
        <AppText text={"We seem to have sent you the wrong way"} style='heading' />

        <AppText text={'Quick, press the button below to get back to safety!'} />

        <AppButton text="RESCUE ME" onPressed={() => { Router.push('/') }} />

      </div >
    </>);

}

export default ErrorPageView;