import styles from '../../styles/Home.module.css'
import Logo from '@/components/global/logo.client';
import { ACCENT_COLOR } from '../utils/consts';
import { AppButton } from '@/components/base/app_button';
import { ContactFooter } from '@/components/global/contact_footer';
import { useRouter } from 'next/router';
import { AppLayout } from '@/components/global/app_layout';
import React from 'react';
import { useStoreContext } from "@/pages/_app";
import { NavBarLoggedInView, NavBarNotLoggedInView } from '../components/navbar/navbar';
import { useEffect } from 'react';

export default function LandingPage() {

  const { authStatusStore, analyticsStore } = useStoreContext();
  const isAuthenticated = authStatusStore.isAuthenticated;

  // Logs analytics when the user accesses this page
  useEffect(() => analyticsStore.logPageView("Home"))

  return (

    <div >
      <title>Home</title>

      <AppLayout
        params={{
          header: <> {isAuthenticated ? <NavBarLoggedInView /> :
            <NavBarNotLoggedInView />}</>,
          body: <div>
            <main className={styles.main}>

              <Logo height={null} text={'Logo'} />

              <h1 className='p-5 m-5'>Together we can help you find the best study material for you!</h1>

              <StartSearchingButton />

            </main>
            <div className='flex justify-center items-center p-2'><a href="#page2">
              <svg width="42" height="22" viewBox="0 0 42 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M40.25 1.75L21 21L1.75 1.75" stroke="black" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" /></svg>
            </a></div>


            <hr />
            <main className={styles.main} id="page2">
              <h1 className='p-5 m-5 text-4xl font-bold'>What is UniShare?</h1>
              <div className='pr-20 pl-20 text-center'>
                <p>UniShare is a platform for students to use to help them search for relevant study materials uploaded by their peers and enables them to upload their own materials.</p>
              </div>
            </main>
          </div>,
          footer: <ContactFooter></ContactFooter>,
        }}
      />
    </div >
  )
}


const StartSearchingButton = () => {

  // Get an instance of the router so we can redirect the user to the home page.
  const router = useRouter();

  // Function to redirect the user to the home page using the router, 
  // this is called when the user presses the 'Start Searching' button.
  const handleClick = () => {
    // Route the user to the home page
    router.push('/search/search');
  }

  // We are returing an AppButton with the desired properties.
  return (
    <AppButton onPressed={() => handleClick()} text='START SEARCHING' />
  );

}

const HeaderButtons = () => {

  const router = useRouter();

  const handleLoginClick = () => {
    router.push('/auth/login');
  }

  const handleRegisterClick = () => {
    router.push('/auth/register');
  }



  return (
    <div style={{
      width: '100%',
      display: 'flex',
      justifyContent: 'right',

    }} className=''>
      <AppButton style={{ classStyles: 'pl-10 pr-10' }} onPressed={() => handleLoginClick()} text='LOGIN'></AppButton>
      <AppButton style={{ classStyles: 'pl-10 pr-10', backgroundColor: ACCENT_COLOR }} onPressed={() => handleRegisterClick()} text='REGISTER'></AppButton>
    </div>
  )
}



