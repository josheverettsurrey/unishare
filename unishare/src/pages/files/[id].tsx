import AppText from "@/components/base/app_text";
import { useRouter } from "next/router";
import { useStoreContext } from "@/pages/_app";
import AppLoadingSpinner from "@/components/base/app_loading_spinner";
import { useEffect, useState } from "react";
import { FetchRequest } from "../../entities/requests/fetch_request";
import { observer } from "mobx-react";
import { Document, Page, pdfjs } from 'react-pdf';
import styles from "../../../styles/DocViewer.module.scss";
import { NavBarLoggedInView, NavBarNotLoggedInView } from '../../components/navbar/navbar';
import React from 'react';

const ViewDocumentPage = () => {

    const { authStatusStore, analyticsStore } = useStoreContext();
    const isAuthenticated = authStatusStore.isAuthenticated;

    const router = useRouter();
    const { id } = router.query;

    const { viewingStore } = useStoreContext();

    const state = viewingStore.state;

    /**
     * Similar to componentDidMount and componentDidUpdate:
     * This allows us to call fetch only when anything in the
     * list of the second parameter changes.
     * This will be called once during initial rendering then once
     * when React rebuilds populates with jsx.
     */
    useEffect(() => {
        viewingStore.fetch(new FetchRequest(id as string));
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [id]);

    /**
     * We can check if there is an error by checking the state property 'hasError'
     * then we can get the message if that property was true using the state
     * property 'errorMessage'.
     * We are returning this at the start and not in the body of the main code so that the
     * rest of the code is not run or shown when there is an error as errors would
     * be thrown due to us trying to render a file that we were unable to fetch etc...
     */

    if (state.hasError) {
        return <AppText text={state.errorMessage} />;
    }

    /**
     * When there is a loading state we want to show the loading spinner
     * Also because we want to use the file and the document property of the state
     * in the main of the file (below) we don't want to reach that code until we know
     * the fetch was successful. Therefore, we are only allowing the code to run past
     * this point and after this return if the fetch was successful.
     */

    if (state.isLoading || !state.success) {
        return <AppLoadingSpinner />;
    }

    // Logs a download event to our firebase analytics dashboard with details about the file being downloaded
    const sendAnalytics = () => {
        const fileDetails = {
            title: state.document.title,
            creator: state.document.creator.displayName,
            documentID: state.document.id,
        }
        analyticsStore.logDownload(fileDetails);
    }

    /**
     * Because we know the fetch was successful (state.success is true) - we can now use state.file and state.document
     * Document.creator provides us with the values for the user that uploaded the document.
     */
    return (
        <div>
            <title>View Document</title>

            {/* Checks if user is authenticated and displays the appropriate navbar */}
            {isAuthenticated ? <NavBarLoggedInView /> :
                <NavBarNotLoggedInView />}


            <div style={{ padding: "2rem" }}>
                <h1 style={{ fontSize: "32px", fontWeight: "bold", textAlign: "center" }}>View Document - {state.document.title}</h1>
                <hr /><br />
            </div>

            <div className={styles.container}>

                <div id="Document Details" className={styles.docDetails}>

                    <h1 style={{ fontSize: "20px", fontWeight: "bold" }}>{state.document.title}</h1>
                    <AppText text={`• Description: ${state.document.description}`} />
                    <AppText text={`• Uploaded by: ${state.document.creator.displayName}`} />
                    <AppText text={`• Affiliation: ${state.document.creator.university.university}`} />
                    <br />
                    {/* Example of using the title and document creator properties */}
                    <a href={state.file} download onClick={sendAnalytics}>
                        <button className="bg-gray-300 hover:bg-gray-400 text-gray-800 font-bold py-2 px-4 rounded inline-flex items-center">
                            <svg className="fill-current w-4 h-4 mr-2" viewBox="0 0 20 20"><path d="M13 8V2H7v6H2l8 8 8-8h-5zM0 18h20v2H0v-2z" /></svg>
                            <span>Download File</span>
                        </button>
                    </a>
                    <br />
                </div>

                <div id="View Document" >
                    <DocViewer src={state.file} />
                    <br />
                </div>

            </div>
        </div>
    );
}


export default observer(ViewDocumentPage);

const DocViewer = ({ src }) => {

    pdfjs.GlobalWorkerOptions.workerSrc =
        `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.js`;

    const [numPages, setNumPages] = useState(null);
    const [pageNumber, setPageNumber] = useState(1);

    if (src == null) {
        return <AppText text={'No data'} />;
    }

    function onDocumentLoadSuccess({ numPages }) {
        setNumPages(numPages);
        setPageNumber(1);
    }

    function changePage(offset) {
        setPageNumber(prevPageNumber => prevPageNumber + offset);
    }

    function previousPage() {
        changePage(-1);
    }

    function nextPage() {
        changePage(1);
    }

    var scaleSize = 0.75;

    if (screen.width <= 600) {
        scaleSize = 0.55;
    }


    return (
        <>

            <div className="bg-black text-white border-2">

                <div className="button_control" style={{ display: "flex", justifyContent: "space-between", padding: "10px" }}>
                    <button
                        type="button"
                        disabled={pageNumber <= 1}
                        onClick={previousPage}
                        className="bg-gray-500 hover:bg-red-500 text-white py-2 px-4 rounded-full"

                    >
                        Previous
                    </button>
                    <div className="page_control" style={{ textAlign: "center", paddingTop: "8px" }}>
                        Page {pageNumber || (numPages ? 1 : '--')} of {numPages || '--'}
                    </div>
                    <button
                        type="button"
                        disabled={pageNumber >= numPages}
                        onClick={nextPage}
                        className="bg-gray-500 hover:bg-red-500 text-white py-2 px-4 rounded-full"
                    >
                        Next
                    </button>
                </div>

                <Document
                    file={src}
                    onLoadSuccess={onDocumentLoadSuccess} >
                    <Page pageNumber={pageNumber} className={styles.pageSize} scale={scaleSize} />
                </Document>
            </div>
        </>
    );
}