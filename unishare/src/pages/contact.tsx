import { AppLayout } from "../components/global/app_layout";
import { ContactView } from "../components/contact/contact";
import { useStoreContext } from "@/pages/_app";
import { NavBarLoggedInView, NavBarNotLoggedInView } from '../components/navbar/navbar';
import React from 'react';
import { useEffect } from "react";

export const ContactPage = () => {

    const { authStatusStore, analyticsStore } = useStoreContext();
    const isAuthenticated = authStatusStore.isAuthenticated;

    // Logs analytics when the user accesses this page
    useEffect(() => analyticsStore.logPageView("Contact"))

    return (
        <div>
            <title>Contact</title>
            <AppLayout params={
                {
                    header: <div>
                        {/* Checks if user is authenticated and displays the appropriate navbar */}
                        {isAuthenticated ? <NavBarLoggedInView /> :
                            <NavBarNotLoggedInView />}<br />
                        <h1 style={{ fontSize: "32px", fontWeight: "bold" }}>Contact Us</h1>
                        <hr />
                    </div>,
                    body: <>
                        <p>We’d love to hear what you think about UniShare? </p>
                        <p>Please fill in the fields below to give us your feedback.</p><br />
                        <ContactView /></>
                }

            } />
        </div>
    );

}

export default ContactPage;