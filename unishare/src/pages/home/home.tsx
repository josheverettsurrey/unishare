import { observer } from "mobx-react-lite";
import React from 'react';
import LandingPage from "@/components/home/homePage";

const HomePage = () => {

  return (
    < LandingPage />
  );
};

export default observer(HomePage);