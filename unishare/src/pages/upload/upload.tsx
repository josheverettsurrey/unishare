import { UploadView } from '../../components/upload/upload.client';
import styles from "../../../styles/Upload.module.scss";
import { AppLayout } from "../../components/global/app_layout";
import React from 'react';
import { useStoreContext } from "@/pages/_app";
import { NavBarLoggedInView, NavBarNotLoggedInView } from '../../components/navbar/navbar';
import { ContactFooter } from '@/components/global/contact_footer';
import { useRequireAuth } from "../../utils/use_require_auth";
import { useEffect } from 'react';

export default function UploadPage() {

    /**
     *  Make this route a protected route. This will cause an auth check to happen when
     *  the page loads so that any unauthenticated users cannot enter.
     */
    useRequireAuth();


    const { authStatusStore, analyticsStore } = useStoreContext();
    const isAuthenticated = authStatusStore.isAuthenticated;

    // Logs analytics when the user accesses this page
    useEffect(() => analyticsStore.logPageView("Upload"))

    return (

        <>
            <title>Upload</title>
            {isAuthenticated ?
                <AppLayout params={
                    {
                        header: <>{/* Checks if user is authenticated and displays the appropriate navbar */}
                            {isAuthenticated ? <NavBarLoggedInView /> :
                                <NavBarNotLoggedInView />}<br />
                            <h1 className={styles.title}>Upload</h1>
                            <hr />
                        </>,

                        body: <>
                            <div className={styles.container}>
                                <UploadView />
                            </div>
                        </>,
                    }
                } />
                // If user is unauthenticated then nothing will be displayed as the RequireAuth check will then redirect the user to the 401 page
                : <></>
            }

        </>
    );

}