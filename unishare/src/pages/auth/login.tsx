import LoginView from '@/components/auth/login.client';
import AppDividedLayout from '@/components/global/app_divided_layout';
import { BACKGROUND_COLOR, PRIMARY_COLOR } from 'src/utils/consts';
import Logo from '@/components/global/logo.client';
import React from 'react';

export default function LoginPage() {
  return (
    <AppDividedLayout
      params={{
        left:
          <div style={{
            backgroundColor: BACKGROUND_COLOR,
            display: 'grid',
            justifyContent: 'center',
            alignItems: 'center',

          }} >
            <Logo></Logo>
          </div>,
        right:
          <div style={{
            backgroundColor: PRIMARY_COLOR,
            display: 'grid',
            justifyContent: 'center',
            alignItems: 'center',

          }}>
            <LoginView></LoginView>
          </div>
      }}
    />
  )
}
