
import AppText from "@/components/base/app_text";
import React from 'react';
import { observer } from "mobx-react";
import { useState } from "react";
import { User } from "src/entities/core/user";
import { useRequireAuth } from "src/utils/use_require_auth";
import { useStoreContext } from "../_app";
import styles from "@/styles/auth/Profile.module.scss";
import { AppTextField } from "@/components/base/app_input_field";
import { UpdateUserRequest } from "src/entities/requests/update_user_request";
import AppLoadingSpinner from "@/components/base/app_loading_spinner";
import { ACCENT_COLOR } from "src/utils/consts";
import Logo from '@/components/global/logo.client';

const ProfilePage = () => {
  useRequireAuth();

  return (<main className={styles.main}>

    <div>
      <Logo></Logo>
      <title>Profile</title>
      <ProfileHeading />
      <ErrorView />
      <ProfileBody />

    </div>
  </main>);
}

export default ProfilePage;

const ProfileBody = observer(() => {
  const { profileStore, authStatusStore } = useStoreContext();
  const isAuthenticated = authStatusStore.isAuthenticated;

  const user = profileStore.state.user;


  const handleUpdate = async (e) => {
    e.preventDefault();
    await profileStore.updateProfile(new UpdateUserRequest({ user: new User(email, displayName, user.university), password }), passwordConfirmation)
  }

  const handleLogout = async (e) => {
    e.preventDefault();
    await profileStore.logout();
  }

  const [displayName, setDisplayName] = useState(user?.displayName ?? '');
  const [email, setEmail] = useState(user?.email ?? '');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');

  if (!user || profileStore.state.loading) {
    return <AppLoadingSpinner />
  }
  return (
  
    <>
      {isAuthenticated ?
        <div className={styles.body}>

          <section className="max-w-4xl p-6 mx-auto bg-white rounded-md shadow-md dark:bg-gray-800" >
            <h2 className="text-lg font-semibold text-gray-700 capitalize dark:text-white">Account settings</h2>

            <form autoComplete="off">
              <div className="grid grid-cols-1 gap-6 mt-4 sm:grid-cols-2">
                <AppTextField val={displayName} onChanged={setDisplayName} label="Display Name" />
                <AppTextField val={email} onChanged={setEmail} label="Email" isEnabled={false} />
                <AppTextField val={password} onChanged={setPassword} label="New Password" obscure={true} />
                <AppTextField val={passwordConfirmation} onChanged={setPasswordConfirmation} label="Password Confirmation" obscure={true} />
              </div>

              <div className="flex justify-center mt-6">
                <button onClick={handleUpdate} className="px-8 py-4 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-800">UPDATE</button>

              </div>

              <div className="flex justify-center mt-6">
                <button onClick={handleLogout} className="px-8 py-4 leading-5 text-white transition-colors duration-200 transform bg-gray-700 rounded-md hover:bg-gray-600 focus:outline-none focus:bg-gray-800" style={{ backgroundColor: `${ACCENT_COLOR}` }}>LOGOUT</button>

              </div>
            </form>
          </section>

        </div>
        
        // If user is unauthenticated then nothing will be displayed as the RequireAuth check will then redirect the user to the 401 page
        : <></>
      }
    </>
  
)})

const ErrorView = observer(() => {

  const { profileStore } = useStoreContext();

  if (profileStore.state.error == null) {
    return <div />;
  }
  return (

    <div className="flex w-full max-w-sm mx-auto overflow-hidden bg-white rounded-lg shadow-md dark:bg-gray-800 mb-4">
      <div className="flex items-center justify-center w-12 bg-red-500">
        <svg className="w-6 h-6 text-white fill-current" viewBox="0 0 40 40" xmlns="http://www.w3.org/2000/svg">
          <path d="M20 3.36667C10.8167 3.36667 3.3667 10.8167 3.3667 20C3.3667 29.1833 10.8167 36.6333 20 36.6333C29.1834 36.6333 36.6334 29.1833 36.6334 20C36.6334 10.8167 29.1834 3.36667 20 3.36667ZM19.1334 33.3333V22.9H13.3334L21.6667 6.66667V17.1H27.25L19.1334 33.3333Z" />
        </svg>
      </div>

      <div className="px-4 py-2 -mx-3">
        <div className="mx-3">
          <span className="font-semibold text-red-500 dark:text-red-400">Error</span>
          <p className="text-sm text-gray-600 dark:text-gray-200">{profileStore.state.error}</p>
        </div>
      </div>
    </div>
  );
})

const ProfileHeading = () => {
  return (
    <div className={styles.heading}>
      <AppText text="Profile" style="heading" />

    </div>

  );
}