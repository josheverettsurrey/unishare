export abstract class DataResponse<T> {



  abstract get content(): T;

  abstract get failure(): string;

  abstract get isSuccessful(): boolean;

  static failure(failure: string): DataResponseFailure<any> {
    return new DataResponseFailure<any>(failure);
  }

  static success<T>(content: T): DataResponseSuccess<T> {
    return new DataResponseSuccess<T>(content);
  }

}

export class DataResponseSuccess<T> extends DataResponse<T>{

  private readonly _data: T;

  constructor(data: T) {
    super();
    this._data = data;
  }

  get content(): T {
    return this._data;
  }

  get failure(): string {
    return null;
  }

  get isSuccessful(): boolean {
    return true;
  }
}

export class DataResponseFailure<T> extends DataResponse<T> {
  private _failure: string;

  constructor(failure: string) {
    super();
    this._failure = failure;
  }

  get content(): T {
    return null;
  }

  get failure(): string {
    return this._failure;
  }

  get isSuccessful(): boolean {
    return false;
  }

}