import { env } from "process";


export class AppConfig {

  get inDevEnvironment(): boolean {

    return env.NODE_ENV == undefined || env.NODE_ENV == 'development';
  }

  get baseUrl(): string {
    return this.inDevEnvironment ? 'http://localhost:4534/v1/' : 'https://unishare.herokuapp.com/';
    // return this.inDevEnvironment ? 'http://localhost:4534/v1/' : 'https://unishare.herokuapp.com/';
  }

  get websocketUrl(): string {
    return this.inDevEnvironment ? 'ws://localhost:4534/v1' : '';
  }

  get logApiRequests(): boolean {
    return false;
  }

  get requestTimeout(): number {
    return 0;
  }

  get responseTimeout(): number {
    return 0;
  }




}