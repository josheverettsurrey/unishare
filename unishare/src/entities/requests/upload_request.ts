interface UploadRequestParams {
    title: string;
    description: string;
    moduleCode: string;
    file: File;
}

export class UploadRequest {

    public readonly title: string;
    public readonly description: string;
    public readonly moduleCode: string;
    public readonly file: File;

    constructor(params: UploadRequestParams) {
        this.title = params.title;
        this.description = params.description;
        this.moduleCode = params.moduleCode;
        this.file = params.file;
    }

    public toJson() : Map<string, any> {
        const map = new Map<string, any>();
        map.set('title', this.title);
        map.set('description', this.description);
        map.set('moduleCode', this.moduleCode);
        map.set('file', this.file);
        return map;
    }
}