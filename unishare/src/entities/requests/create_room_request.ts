interface CreateRoomRequestParams{
    moduleCode: string;
    name: string;

}

export class CreateRoomRequest {

    public moduleCode: string;
    public name: string;

    constructor(params: CreateRoomRequestParams) {
        this.moduleCode = params.moduleCode;
        this.name = params.name;
    }

    toJson(): Map<string, any> {
        const map: Map<string, any> = new Map();
        map.set('name', this.name);
        map.set('moduleCode', this.moduleCode);
        return map;
    }
}