export class FetchRequest {
    public id: string;
    constructor(id: string) {
        this.id = id;
    }

    toJson() : Map<string, any>{
        const map = new Map<string, any>();
        map.set('id', this.id);
        return map;
    }
}