import { json } from "stream/consumers";
import { User } from "../core/user";

export class UpdateUserRequest {
  public user?: User;
  public password?: string;

  constructor({ user, password }: { user?: User, password?: string }) {
    this.user = user;
    this.password = password;
  }

  toJson(): Map<string, any> {
    const map = new Map<string, any>();
    map.set('user', this.user);
    map.set('password', this.password);
    return map;
  }

}