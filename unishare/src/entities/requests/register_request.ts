import { User } from "../core/user";

export class RegisterRequest {

  public password: string;
  public user: User;

  constructor(
    user: User,
    password: string,
  ) {
    this.user = user;
    this.password = password;
  }

  toJson(): Map<string, any> {
    const map = new Map<string, any>();
    map.set('user', Object.fromEntries(this.user.toJson()));
    map.set('password', this.password);
    return map;
  }
}