
export class LoginRequest {

  public email: string;
  public password: string;

  constructor(
    email: string,
    password: string
  ) {
    this.email = email;
    this.password = password;
  }

  public toJson(): Map<string, any> {
    const map = new Map<string, any>();
    map.set('email', this.email);
    map.set('password', this.password);
    return map;
  }
}