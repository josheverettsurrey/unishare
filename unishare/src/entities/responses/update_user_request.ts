import { User } from "../core/user";

export class UpdateUserResponse {

  public user: User;
  constructor(user: User) {
    this.user = user;
  }

  static fromJson(json): UpdateUserResponse {
    const user = User.fromJson(json.user);
    return new UpdateUserResponse(user);
  }
}