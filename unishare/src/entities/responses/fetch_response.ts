import {UnishareDocument} from "../core/unishare_document";

export class FetchResponse {

    public document: UnishareDocument;
    public file: any;
    constructor(file: any, document: UnishareDocument) {
        this.file = file;
        this.document =document;
    }

    static fromJson(json: Map<string ,any>): FetchResponse {
        return new FetchResponse(json['file'], UnishareDocument.fromJson(json['document']));
    }
}