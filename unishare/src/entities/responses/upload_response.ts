export class UploadResponse {

    public message: string;
    constructor(message: string) {
        this.message = message;
    }

    static fromJson(json: Map<String, any>) : UploadResponse {
        return new UploadResponse(json['message']);
    }
}