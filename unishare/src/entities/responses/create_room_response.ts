import {ChatRoom} from "../core/chat_room";

export class CreateRoomResponse {

    public rooms: ChatRoom[];
    constructor(rooms: ChatRoom[]) {
        this.rooms = rooms;
    }

    static fromJson(json: Map<string, any>): CreateRoomResponse{
        return new  CreateRoomResponse(
            json['rooms'].map((rawRoom) => ChatRoom.fromJson(rawRoom)),
        );
    }

}
