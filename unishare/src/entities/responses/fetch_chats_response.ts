import {ChatRoom} from "../core/chat_room";

interface FetchChatsResponseParams{
    rooms: ChatRoom[];
}

export class FetchChatsResponse {
    public readonly rooms: ChatRoom[];
    constructor(params: FetchChatsResponseParams) {
        this.rooms = params.rooms;
    }

    static fromJson(json: Map<string, any>): FetchChatsResponse{
        return new FetchChatsResponse({rooms: json['rooms'].map((rawRoom) => ChatRoom.fromJson(rawRoom))});
    }
}