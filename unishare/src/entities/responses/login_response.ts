import { User } from "../core/user";

export class LoginResponse {

  constructor(
    public sessionToken: string,
    public user: User,
  ) { }

  static fromJson(json: Map<string, any>): LoginResponse {
    return new LoginResponse(
      json['token']['authToken'],
      User.fromJson(json['user']),
    );
  }
}