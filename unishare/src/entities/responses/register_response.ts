import { User } from "../core/user";

export class RegisterResponse {

  public authenticationToken: string;
  public user: User;
  public message: string;
  constructor(
    authenticationToken: string,
    user: User,
    message: string
  ) {
    this.authenticationToken = authenticationToken;
    this.user = user;
    this.message= message;
  }

  static fromJson(json: Map<string, any>): RegisterResponse {
    return new RegisterResponse(
      json['token']['authToken'],
      User.fromJson(json['user']),
      json['message'],
    );
  }

}