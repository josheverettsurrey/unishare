import {ChatMessage} from "../core/chat_message";

interface JoinChatRoomResponseParams {
    messages: ChatMessage[];
}

export class JoinChatRoomResponse {

    public messages: ChatMessage[];
    constructor(params: JoinChatRoomResponseParams) {
        this.messages = params.messages;
    }

    static fromJson(json: Map<string, any>) : JoinChatRoomResponse{
        return new JoinChatRoomResponse({
            messages: json['messages'].map((rawMessage) => ChatMessage.fromJson(rawMessage))
        });

    }

}