import {UnishareDocument} from "../core/unishare_document";

export class SearchResponse{
    public results: UnishareDocument[];

    constructor(results: UnishareDocument[]) {
        this.results = results;
    }

    static fromJson(json: Map<string, any>): SearchResponse{
        return new SearchResponse(json['results'].map((rawResult) => UnishareDocument.fromJson(rawResult)));
    }
}