import {AppConfig} from "../config/app_config";
import {ApiClientInterceptor, HttpLoggerInterceptor, InternalErrorInterceptor} from "./interceptors";
import Axios, {AxiosInstance, AxiosRequestConfig, AxiosRequestHeaders, AxiosResponse} from 'axios';
import {Record} from "immutable";

/**
 * Definition of ApiClientOptions class that allows us to define some properties that
 * are options for the client.
 * At the moment the only required options are the possible interceptors that the client.
 */
class ApiClientOptions {

    public interceptors: ApiClientInterceptor[];
    public defaultHeaders: AxiosRequestHeaders;
    constructor(interceptors: ApiClientInterceptor[], defaultHeaders? : AxiosRequestHeaders) {
        this.interceptors = interceptors;
        this.defaultHeaders = defaultHeaders ??  {
            'Content-Type': 'application/json'
        };
    }

}

/**
 * Definition of ApiClientBase class that will be a base for both the unauthenticated client and
 * the authenticated client. This contains the client options, config and instance of the http client
 *
 * This class contains the methods for post and get requests and manages the interceptors.
 */
export class ApiClientBase {
    protected httpClient: AxiosInstance;
    protected config: AppConfig;
    protected options: ApiClientOptions;

    constructor(config: AppConfig) {

        // setting the client config
        this.config = config;

        // creating the client options with an internal error interceptor
        this.options = new ApiClientOptions([new InternalErrorInterceptor()]);

        // Creating the http client
        this.httpClient = Axios.create({
            baseURL: config.baseUrl,
            headers: this.options.defaultHeaders,
        });


        // Check the config to check whether the client should be using http logger interceptor to log
        // each request and response made / received by the client.
        if (this.config.logApiRequests) {
            // creating the response logger interceptor
            const httpLoggerInterceptor = new HttpLoggerInterceptor();

            // Use the http clients interceptors for this interceptor.
            this.httpClient.interceptors.request.use(httpLoggerInterceptor.onRequest, httpLoggerInterceptor.onRequestError);
            this.httpClient.interceptors.response.use(httpLoggerInterceptor.onResponse, httpLoggerInterceptor.onResponseError);
        }
    }

    /**
     * Function to make a post request with the existing API client options
     */
    public async post(path: string, body: Map<string, any>): Promise<AxiosResponse> {


        const requestOptions: AxiosRequestConfig = {
            url: path,
            method: 'POST',
            data: Object.fromEntries(body),
            headers:  this.options.defaultHeaders ,
        };
        return  await this._sendRequest(requestOptions);
    }

    public async file(path: string, body: Map<string, any>, forFile: boolean = false): Promise<AxiosResponse> {
        const formData = new FormData();
        body.forEach((val, key) => formData.append(key, val));

        const requestOptions: AxiosRequestConfig = {
            url: path,
            method: 'POST',
            data: formData,
            headers: forFile ? { 'Content-type': 'multipart/form-data'}:  this.options.defaultHeaders ,
        };
        return  await this._sendRequest(requestOptions);
    }

    /**
     * Function to make a get request with the existing API client options
     */
    public async get(path: string, queryParameters?: {}): Promise<AxiosResponse> {
        const requestOptions: AxiosRequestConfig = {
            url: path,
            method: 'GET',
            params: queryParameters,
        };
        return await this._sendRequest(requestOptions);
    }

    public  connectSocket(path: string, protocols?: string | string[]): WebSocket{

        return new WebSocket(`${this.config.websocketUrl}/${path}`, protocols);
    }

    public async _sendRequest(requestOptions: AxiosRequestConfig): Promise<AxiosResponse> {

        // Mutate the request options with each interceptor [onRequest] function.
        for (let i = 0; i < this.options.interceptors.length; i++) {
            requestOptions = this.options.interceptors[i].onRequest(requestOptions);
        }

        return await this.httpClient.request(requestOptions).then(
            response => {
                // Mutate the response with each interceptor [onResponse] function.
                for (let i = 0; i < this.options.interceptors.length; i++) {
                    response = this.options.interceptors[i].onResponse(response);
                }
                return response;
            },

            error => {
                // Mutate the error with each interceptor [onError] function.
                for (let i = 0; i < this.options.interceptors.length; i++) {
                    error = this.options.interceptors[i].onError(error);
                }
                return error;
            }
        );

    }

}


/**
 * Definition for the ApiClientAuthenticated class
 */
export class ApiClientAuthenticated extends ApiClientBase { }

/**
 * Definition for the ApiClientUnauthenticated class
 */
export class ApiClientUnauthenticated extends ApiClientBase { }

