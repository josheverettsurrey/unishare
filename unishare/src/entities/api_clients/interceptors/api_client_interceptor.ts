import { AxiosRequestConfig, AxiosResponse } from "axios";

export abstract class ApiClientInterceptor {

    public onRequest(options: AxiosRequestConfig): AxiosRequestConfig {
        return options;
    }
    public onResponse(response: AxiosResponse): AxiosResponse {
        return response;
    }

    public onError(error: any): any {
        return error;
    }
}
