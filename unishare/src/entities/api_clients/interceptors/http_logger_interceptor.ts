import { AxiosRequestConfig, AxiosResponse } from "axios";

export class HttpLoggerInterceptor {

  public onResponse(response: AxiosResponse) {

    console.log('RESPONSE BODY', response.data);
    return response;
  }

  public onRequest(request: AxiosRequestConfig) {

    console.log(`${request.method} Request to ${request.url}`);
    console.log('Headers: ', request.headers)
    if (request.data) console.log('Request body: ', request.data)
    return request;
  }

  public onRequestError(error: Object) {

    console.log('REQUEST ERROR', error['message']);
    return error;
  }

  public onResponseError(error: Object) {

    console.log('RESPONSE ERROR', error['message']);
    return error;
  }

}
