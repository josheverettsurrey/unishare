import { AxiosResponse } from "axios";
import { IAuthenticationService } from "src/services/authentication_service";
import { ApiClientInterceptor } from "./api_client_interceptor";

export class UnauthorizedInterceptor extends ApiClientInterceptor {

  private authenticationService: IAuthenticationService;
  constructor(
    authenticationService: IAuthenticationService
  ) {
    super();
    this.authenticationService = authenticationService;
  }

  public onResponse(response): AxiosResponse {

    if (response.response.status === 401) {
      this.authenticationService.logout();
    }
    return super.onResponse(response);
  }
}