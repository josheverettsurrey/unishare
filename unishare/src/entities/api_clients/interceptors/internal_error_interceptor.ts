import { UnishareException } from "src/entities/exceptions/unishare_exception";
import { AxiosResponse } from 'axios';
import { ApiClientInterceptor } from "./api_client_interceptor";


/// Cast any responses that are successful yet have a success of false in the body and handle.
export class InternalErrorInterceptor extends ApiClientInterceptor {

  public onResponse(response): AxiosResponse {


    if (response?.response?.data && response.response.data['success'] === false) {
      const payload = response.response.data;
      throw new UnishareException(response.status, payload['message'], payload['error']);
    }
    return super.onResponse(response);

  }

  public onError(error: any) {
    return super.onError(error);
  }
}