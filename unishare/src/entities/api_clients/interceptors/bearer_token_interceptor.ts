import { AxiosRequestConfig } from "axios";
import { IAuthenticationService } from "src/services/authentication_service";
import { ApiClientInterceptor } from "./api_client_interceptor";

export class BearerTokenInterceptor extends ApiClientInterceptor {

    private authService: IAuthenticationService;

    constructor(service: IAuthenticationService) {
        super();
        this.authService = service;
    }

    /// Working but test didn't like it
    // public onRequest(options: RequestInit): RequestInit {
    //     options.headers[`Authorization: Bearer ${this.authService.authenticationToken}`]
    //     return super.onRequest(options);
    // }

    public onRequest(options: AxiosRequestConfig): AxiosRequestConfig {
        options.headers.Authorization = `Bearer ${this.authService.authenticationToken}`;
        return super.onRequest(options);
    }




}