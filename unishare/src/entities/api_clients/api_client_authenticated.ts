import { IAuthenticationService } from "src/services/authentication_service";
import { ServerConnectivityService } from "src/services/server_connectivity_service";
import { AppConfig } from "../config/app_config";
import { ApiClientAuthenticated } from "./api_client_base";
import { ApiClientUnauthenticatedImpl } from "./api_client_unauthenticated";


export class ApiClientAuthenticatedImpl extends ApiClientUnauthenticatedImpl implements ApiClientAuthenticated {
    constructor(private authService: IAuthenticationService, config: AppConfig, serverConnectivityService: ServerConnectivityService) {
        super(config, serverConnectivityService);

        this.httpClient.interceptors.request.use(
            (request) => {
                request.headers['Authorization'] = `Bearer ${this.authService.authenticationToken}`;
                return request;
            }
        );

        this.httpClient.interceptors.response.use(
            response  => {
                try {
                    // @ts-ignore
                    if (response.response.status === 401) {
                        this.authService.logout();
                    }
                }finally{
                    return response;
                }

            },
        );
    }
}
