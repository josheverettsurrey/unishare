
import { ServerConnectivityService } from "src/services/server_connectivity_service";
import { AppConfig } from "../config/app_config";
import { GlobalException } from "../exceptions/global";
import { UnishareException } from "../exceptions/unishare_exception";
import { ApiClientBase, ApiClientUnauthenticated } from "./api_client_base";

/**
 * Implementation of the unauthenticated api client that extends the base api client previously defined.
 * On top of the existing interceptors this class adds an interceptor to watch for network errors that use
 * the ServerConnectivityService to update the view with the current state of the connectivity to the server.
 */
export class ApiClientUnauthenticatedImpl extends ApiClientBase implements ApiClientUnauthenticated {

    constructor(config: AppConfig, serverConnectivityService: ServerConnectivityService) {

        super(config);
        // Setting up a server connectivity interceptor that will be used to check if the server is
        // reachable or not.
        // We assume that if the error message is 'Network Error' then the server is not reachable.
        this.httpClient.interceptors.response.use(response => { return response }, error => {
            if (error.message === 'Network Error') {
                serverConnectivityService.setConnected(false);
                // Here we are catching the error and throwing a unishare error instead.
                throw new UnishareException(404, GlobalException.ServerError, 'Server is not reachable');
            }
            return error;
        });
    }
}