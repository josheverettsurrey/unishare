export class AppButtonStyle {
  constructor(
    public backgroundColor?: string,
    public color?: string,
    public height?: number,
    public width?: number,
    public borderRadius?: number,
    public fontSize?: number,
    public classStyles?: string,
    public padding?: number | string,
    public margin?: number | string,
    public fontFamily?: string,
    public fontWeight?: string,
    public textAlign?: string,
  ) { }
}