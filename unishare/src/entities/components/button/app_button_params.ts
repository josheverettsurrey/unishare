import { AppButtonStyle } from "../styles";

export interface AppButtonParams {
  style?: AppButtonStyle,
  onPressed: Function,
  text: string,
}
