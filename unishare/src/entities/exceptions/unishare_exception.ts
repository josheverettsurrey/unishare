export class UnishareException extends Error {
  constructor(public readonly errorCode: number, public readonly message: string, public readonly error: string) {
    super(message);
  }
}
