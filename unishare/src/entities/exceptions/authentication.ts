export enum AuthenticationExceptionEnum {
  InvalidCredentials = 'InvalidCredentials',
  InvalidToken = 'InvalidToken',
  BadSession = 'BadSession',
  ValidationError = 'ERR_INVALID_DATA'
}

export class AuthenticationException {

  constructor(public code: number, public error: string, public message: string) {
  }

  static InvalidCredentials: AuthenticationException = new AuthenticationException(401, AuthenticationExceptionEnum.InvalidCredentials, 'Hey, Looks like you\'re using the wrong email or password!');

  static ValidationError: AuthenticationException = new AuthenticationException(400, AuthenticationExceptionEnum.ValidationError, 'Hey, Looks like you didn\'t fill out the form correctly!');


}