export enum GlobalException {
  MethodUndefined = 'MethodUndefined',
  BadState = 'BadState',
  UnfinishedFeature = 'UnfinishedFeature',
  ServerError = 'ServerError - We ran into a problem on our end. Please try again later.',
  UnknownError = 'UnknownError - We ran into a problem on our end. Please contact us and tell us what happened.',
}