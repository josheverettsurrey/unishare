import {User} from "./user";

interface ChatMessageParams {
    id: string;
    sender: User;
    body: string;
    createdAt: number;
}

export class ChatMessage {
    public id: string;
    public sender: User;
    public body: string;
    createdAt: number;

    constructor(params: ChatMessageParams) {
        this.id = params.id;
        this.sender = params.sender;
        this.body = params.body;
        this.createdAt = params.createdAt;
    }

    toJson() : Map<string, any> {
        const map: Map<string, any> = new Map();
        map.set('id', this.id);
        map.set('sender', this.sender);
        map.set('body', this.body);
        return map;
    }

    public static fromJson(json: Map<string, any>) : ChatMessage {
        return new ChatMessage({
            id: json['id'],
            sender: User.fromJson(json['sender']),
            body: json['body'],
            // TODO: Change to actual createdAt
            createdAt: Date.now(),
        });
    }
}