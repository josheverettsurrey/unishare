import { ifBrowser } from "src/utils/global_utils";

export class University {

  public university: string
  public domain: string;

  constructor(university: string, domain: string, logo?: string) {
    this.university = university;
    this.domain = domain;
  }


  static fromJson(json: Map<string, any>): University {
    return new University(
      json['university'],
      json['domain'],
    );
  }

  toJson(): Map<string, any> {
    const map = new Map<string, any>();
    map.set('university', this.university);
    map.set('domain', this.domain);
    return map;
  }
}