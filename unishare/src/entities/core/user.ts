import { University } from "./university";


export class User {

  private readonly _email: string;
  private readonly _displayName: string;
  private readonly _university: University;

  constructor(email: string, displayName: string, university: University) {
    this._email = email;
    this._displayName = displayName;
    this._university = university;
  }

  get university(): University {
    return this._university;
  }

  get email(): string {
    return this._email;
  }

  get displayName(): string {
    return this._displayName;
  }

  static fromJson(json: Map<string, any>): User {
    return new User(
      json['email'],
      json['displayName'],
      University.fromJson(json['university']),
    );
  }

  toJson(): Map<string, any> {
    const map = new Map<string, any>();
    map.set('email', this._email);
    map.set('displayName', this._displayName);
    map.set('university', this._university?.toJson());

    return map;
  }

  static forCreate(email: string, displayName: string): User {
    return new User(email, displayName, null);
  }


}