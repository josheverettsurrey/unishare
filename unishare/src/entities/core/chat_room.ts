/**
 * Interface to define the properties of the ChatRoom class
 */
interface ChatRoomParams {
    id: string;
    name: string;
    module: string;
}

/**
 * Definition of the ChatRoom class that will be used
 * to hold basic information about a given chat room like its
 * name and its module.
 */
export class ChatRoom {
    public readonly id: string;
    public readonly name: string;
    public readonly module: string;

    constructor(params: ChatRoomParams) {
        this.id = params.id;
        this.name = params.name;
        this.module = params.module;
    }

    static fromJson(json: Map<string, any>): ChatRoom {
        return new ChatRoom({
            id: json['id'],
            name: json['name'],
            module: json['module']
        });
    }

    toJson(): Map<string, any> {
        const map: Map<string, any> = new Map();
        map.set('id', this.id);
        map.set('name', this.name);
        map.set('module', this.module);
        return map;
    }
}