import {User} from "./user";

interface UnishareDocumentParams {
    title: string;
    description: string;
    creator: User;
    uuid: string;
    id: string;
    university: string;
}

export class UnishareDocument {
    public title: string;
    public description: string;
    public creator: User;
    public uuid: string;
    public id: string;
    public university:string;

    constructor(params: UnishareDocumentParams) {
        this.title = params.title;
        this.description = params.description;
        this.creator = params.creator;
        this.uuid = params.uuid;
        this.id = params.id;
    }

    static fromJson(json: Map<string, any>): UnishareDocument {
        return new UnishareDocument({
            title: json['title'],
            description: json['description'],
            creator: User.fromJson(json['creator']),
            uuid: json['uuid'],
            id: json['id'],
            university: json['university']['university']
        })
    }
}