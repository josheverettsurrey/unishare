// File that will contain all the constants used in the application

// LocalStorage keys
export const AUTHENTICATION_TOKEN_KEY = 'authToken';
export const USER_KEY = 'user';

// Colours
export const BACKGROUND_COLOR = '#F9F9F9';
export const PRIMARY_COLOR = '#212121';
export const ACCENT_COLOR = '#BB232A';
export const ACCENT_COLOR_DARK = '#872227';
