export function ifBrowser(func: Function) {
  if (typeof window !== 'undefined') {
    func();
  }
}