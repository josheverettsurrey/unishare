import { useStoreContext } from "@/pages/_app";
import { observer } from "mobx-react-lite";
import { useEffect } from "react";
import { ifBrowser } from "./global_utils";
import Router from "next/router";

export function useRequireAuth(redirectUrl = '/401/') {
  const { authStatusStore } = useStoreContext();

  useEffect(() => {
    if (!authStatusStore.isAuthenticated) {
      Router.push(redirectUrl);
    }

  }, [authStatusStore, redirectUrl]);


  return authStatusStore;
}