
import { LoginStore } from './stores/login_store';
import { ServerConnectivityStore } from './stores/server_connectivity_store';
import { RegisterStore } from './stores/register_store';
import HomeStore from './stores/home_store';
import { ApiClientUnauthenticatedImpl } from './entities/api_clients/api_client_unauthenticated';
import { AppConfig } from './entities/config/app_config';
import { AuthenticationServiceImpl } from './services/authentication_service';
import { CurrentUserService } from './services/current_user_service';
import { ServerConnectivityService } from './services/server_connectivity_service';
import { ProfileStore } from './stores/profile_store';
import { ApiClientAuthenticatedImpl } from './entities/api_clients/api_client_authenticated';
import { UserUpdateService } from './services/user_update_service';
import { AuthStatusStore } from './stores/auth_status_store';
import {UploadStore} from "./stores/upload_store";
import {SearchStore} from "./stores/search_store";
import {DocumentService} from "./services/document_service";
import {ViewingStore} from "./stores/viewing_store";
import {ChatDashboardState, ChatDashboardStore} from "./stores/chat_dashboard_store";
import ChatService from "./services/chat_service";
import {ChatRoomStore} from "./stores/chat_room_store";
import { AnalyticsStore } from './stores/firebase_analytics_store';



// Defining an interface that will be used to describe the shape of the store
// object that will be used to create the context provider. We can provide any
// other stores that we want to include in the application here.
export interface IStoreContext {
  serverConnectivityStore: ServerConnectivityStore;
  loginStore: LoginStore;
  registerStore: RegisterStore;
  homeStore: HomeStore;
  profileStore: ProfileStore;
  authStatusStore: AuthStatusStore;
  uploadStore: UploadStore;
  searchStore: SearchStore;
  viewingStore: ViewingStore;
  chatDashboardStore: ChatDashboardStore;
  chatRoomStore: ChatRoomStore;
  analyticsStore: AnalyticsStore;
}


export function setupContext(): IStoreContext {
  // Create instance of the application configuration
  const config = new AppConfig();

  // Create instance of the ServerConnectivityService
  // When the user makes a request to the server and it cannot
  // be reached, the serverConnectivityService will notify the ServerConnectivityStore
  // and that will show in the application
  const serverConnectivityService = new ServerConnectivityService();
  const serverConnectivityStore = new ServerConnectivityStore(serverConnectivityService);

  // Create instance of the CurrentUserService
  // This is used to contain the current user and retrieve it from local storage
  // it will be the source of truth for the current user.
  // When the there is a need to show the current user in the view, the CurrentUserService
  // will need to be injected into the store that will be keeping track of the view and
  // made available to through its current state.
  const currentUserService = new CurrentUserService();

  // Creating an instance of the unauthenticated api client so that we can use it to make the un-authenticated
  // request
  const apiClientUnauthenticated = new ApiClientUnauthenticatedImpl(config, serverConnectivityService);
  const authenticationService = new AuthenticationServiceImpl(apiClientUnauthenticated, currentUserService);
  const apiClientAuthenticated = new ApiClientAuthenticatedImpl(authenticationService, config, serverConnectivityService);

  const registerStore = new RegisterStore(authenticationService);

  const authStatusStore = new AuthStatusStore(authenticationService);


  // Creating the authentication service that can be passed into 
  // any object that needs to use the authentication service.

  // Creating the authenticated api client that will be used to make authenticated requests as it has
  // access to the authentication service and the cached authentication token that will be passed into
  // the header of the authenticated requests.
  // const apiClientAuthenticated: ApiClientAuthenticatedImpl = new ApiClientAuthenticatedImpl(authenticationService, config);

  // Creating the authStore that will be used for authentication, so that we can
  // register it inside the store context provider to use throughout the application.
  const loginStore = new LoginStore(authenticationService);


  // Creating a homeStore that will be used on the home page so that we can 
  // have some state on the home page.
  const homeStore = new HomeStore(currentUserService);

  // Creating a service that will allow us to update the current user.
  const userUpdateService = new UserUpdateService(apiClientAuthenticated);

  // Creating a profileStore that will be used on the profile page so that we can
  // have some state on the profile page and be able to have access to the current 
  // user services.
  const profileStore = new ProfileStore(currentUserService, userUpdateService, authenticationService);

  const documentService = new DocumentService(apiClientAuthenticated);

  // Create the uploadStore that will be used on the upload page so that we can upload
  // documents in the state layer of the application.
  const uploadStore = new UploadStore(documentService);

  // Create the searchStore that will be used on the search page so that we can search the uploaded
  // documents
  const searchStore = new SearchStore(documentService);

  // Create the viewingStore that will be used on the viewing documents page so that we can fetch a specific
  // document and view it on the page.
  const viewingStore = new ViewingStore(documentService);

  // Create a chatService that will be used to make the api calls to the (and hold) the active
  // socket connection between the client and the server during chat
  const chatService = new ChatService(apiClientAuthenticated, authenticationService);

  const chatDashboardStore = new ChatDashboardStore(chatService);

  const chatRoomStore = new ChatRoomStore(chatService, currentUserService);


  const analyticsStore = new AnalyticsStore();

  return {
    serverConnectivityStore,
    loginStore,
    registerStore,
    homeStore,
    profileStore,
    authStatusStore,
    uploadStore,
    searchStore,
    viewingStore,
    chatDashboardStore,
    chatRoomStore,
    analyticsStore
  };

}
