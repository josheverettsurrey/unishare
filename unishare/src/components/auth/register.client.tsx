import { useState } from "react";
import { User } from "src/entities/core/user";
import { RegisterRequest } from "src/entities/requests/register_request";
import { BACKGROUND_COLOR, PRIMARY_COLOR } from "src/utils/consts";
import { AppButton } from "../base/app_button";
import { LoginStoreStateLoading, LoginStoreStateAuthenticated } from "../../stores/login_store";
import { observer } from 'mobx-react';
import { RegisterStoreStateError } from "src/stores/register_store";
import { useStoreContext } from "@/pages/_app";
import React from 'react';
import Router from "next/router";


const RegisterView = () => {
  const { registerStore } = useStoreContext();

  const state = registerStore.state;

  const [displayName, setDisplayName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [passwordConfirmation, setPasswordConfirmation] = useState('');


  if (state instanceof LoginStoreStateLoading) {
    return <div>Loading!</div>
  }

  else if (state instanceof LoginStoreStateAuthenticated) {
    return <div style={{ color: BACKGROUND_COLOR }}>
      <h1>You are logged in</h1>
    </div>
  }

  return (
    <div style={{
      color: BACKGROUND_COLOR,
      display: 'grid',
      gridGap: '5px',
      width: '390px',
      justifyContent: 'right',
      justifyItems: 'right',



    }}>
      <h1 className=' ' style={{
        fontSize: 72,
        fontWeight: 'bold',
        width: '100%',
        textAlign: 'right',

      }}>JOIN US!</h1>
      <h2 style={{ fontSize: 16 }}>Have an account? <span onClick={() => Router.push('/auth/login')} style={{
        paddingLeft: '0.1em',
        textDecorationColor: BACKGROUND_COLOR,
        textDecoration: 'underline',
        fontWeight: 'bold',
      }}>Click here to sign in!</span></h2>

      <_ErrorView />

      <div style={{ paddingTop: 50, display: 'flex', width: '100%', justifyContent: 'space-between', }}>
        <label className={``} style={{ paddingRight: '1em' }}>DISPLAY NAME</label>
        <input type="email" style={{ width: '75%', color: PRIMARY_COLOR }} value={displayName} onChange={(event) => setDisplayName(event.target.value)} />
      </div>

      <div style={{ paddingTop: 50, display: 'flex', width: '100%', justifyContent: 'space-between', }}>
        <label className={``} style={{ paddingRight: '1em' }}>EMAIL</label>
        <input type="email" style={{ width: '75%', color: PRIMARY_COLOR }} value={email} onChange={(event) => setEmail(event.target.value)} />
      </div>


      <div style={{
        paddingTop: 10, display: 'flex', width: '100%', justifyContent: 'space-between',
      }}>
        <label style={{ paddingRight: '1em' }}>PASSWORD</label>
        <input type="password" value={password} style={{ width: '75%', color: PRIMARY_COLOR }} onChange={(event) => setPassword(event.target.value)} />
      </div>

      <div style={{
        paddingTop: 10, display: 'flex', width: '100%', justifyContent: 'space-between',
      }}>
        <label style={{ paddingRight: '1em' }}>CONFIRM PASSWORD</label>
        <input type="password" value={passwordConfirmation} style={{ width: '75%', color: PRIMARY_COLOR }} onChange={(event) => setPasswordConfirmation(event.target.value)} />

      </div>


      <div style={{
        paddingTop: 30, display: 'grid', justifyContent: 'center', width: '100%',
      }}>
        <AppButton text='REGISTER' style={{ height: 50, width: 100, color: PRIMARY_COLOR, backgroundColor: BACKGROUND_COLOR }} onPressed={() => {

          const user = User.forCreate(email, displayName);
          const request = new RegisterRequest(user, password);
          return registerStore.register(request, passwordConfirmation);
        }} />
      </div>


    </div >
  );

};

export default observer(RegisterView);


const _ErrorView = () => {
  const { registerStore } = useStoreContext();

  const state = registerStore.state;
  if (state instanceof RegisterStoreStateError) return (<div onClick={() => { registerStore.dismissError() }} style={{
    color: 'red',
    padding: '5px',
    borderRadius: '5px',
    justifyItems: 'center',
    alignItems: 'center',
    display: 'block',
    width: '320px',

  }}>

    <h1 style={{ fontSize: 14, textAlign: 'center' }}>{state.error} </h1>

  </div>);
  else return (<div />);
}



