import { observer } from 'mobx-react';
import React, { useState } from 'react';
import { BACKGROUND_COLOR, PRIMARY_COLOR } from 'src/utils/consts';
import { LoginStoreStateAuthenticated, LoginStoreStateError, LoginStoreStateLoading } from '../../stores/login_store';
import { useStoreContext } from 'src/pages/_app';
import { AppButton } from '../base/app_button';
import { LoginRequest } from 'src/entities/requests/login_request';
import AppLoadingSpinner from '../base/app_loading_spinner';
import Router from "next/router";


const LoginView = (props) => {

  const { loginStore } = useStoreContext();

  const state = loginStore.state;

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  if (state instanceof LoginStoreStateLoading) {
    return <AppLoadingSpinner />
  }

  else if (state instanceof LoginStoreStateAuthenticated) {
    return <div style={{ color: BACKGROUND_COLOR }}>
      <h1>You are logged in</h1>
    </div>
  }

  return (
    <div style={{
      color: BACKGROUND_COLOR,
      display: 'grid',
      gridGap: '5px',
      width: '320px',
      justifyContent: 'right',
      justifyItems: 'right',



    }}>
      <h1 className=' ' style={{
        fontSize: 72,
        fontWeight: 'bold',
        width: '100%',
        textAlign: 'right',

      }}>LOGIN</h1>
      <h2 style={{ fontSize: 16 }}>Need an account? <span onClick={() => Router.push('/auth/register')} style={{
        paddingLeft: '0.1em',
        textDecorationColor: BACKGROUND_COLOR,
        textDecoration: 'underline',
        fontWeight: 'bold',
      }}>Click here to sign up!</span></h2>

      <_ErrorView />

      <div style={{ paddingTop: 50, display: 'flex', width: '100%', justifyContent: 'space-between', }}>
        <label className={``} style={{ paddingRight: '1em' }}>EMAIL</label>
        <input type="email" style={{ width: '75%', color: PRIMARY_COLOR }} value={email} onChange={(event) => setEmail(event.target.value)} />
      </div>


      <div style={{
        paddingTop: 10,
        display: 'flex',
        width: '100%',
        justifyContent: 'space-between',
      }}>
        <label style={{ paddingRight: '1em' }}>PASSWORD</label>
        <input type="password" value={password} style={{ width: '75%', color: PRIMARY_COLOR }} onChange={(event) => setPassword(event.target.value)} />

      </div>

      <div style={{
        paddingTop: 30,
        display: 'grid',
        justifyContent: 'center',
        width: '100%',
      }}><AppButton text='LOGIN' style={{ height: 50, width: 100, color: PRIMARY_COLOR, backgroundColor: BACKGROUND_COLOR }} onPressed={() => {
        return loginStore.login(new LoginRequest(email ?? "", password ?? ""));
      }} />
      </div>


    </div >
  );

};

export default observer(LoginView);


const _ErrorView = () => {
  const { loginStore } = useStoreContext();

  const state = loginStore.state;
  if (state instanceof LoginStoreStateError) return (<div onClick={() => loginStore.dismissError()} style={{
    color: 'red',
    padding: '5px',
    borderRadius: '5px',
    justifyItems: 'center',
    alignItems: 'center',
    display: 'block',
    width: '320px',

  }}>


    <h1 style={{ fontSize: 14, textAlign: 'center' }}>{state.error} </h1>

  </div>);
  else return (<div></div>);
}