import Image from 'next/image';
import * as LogoSvg from '../../../public/logo.svg';
import React from 'react';
import Router from "next/router";


const LogoView = ({ text, width, height }: { text?: string, width?: number, height?: number }) => {

  return (
    <div onClick={() => Router.push("/")}
      style={{ cursor: "pointer" }}>
      <Image width={width} height={height} src={LogoSvg} alt={text ?? 'Oh no, an image was meant to be here'} />
    </div>
  );
}

export default LogoView;