import styles from '@/styles/layouts/AppDividedLayout.module.scss';
import React from 'react';
export interface AppDividedLayoutProps {
  left: JSX.Element;
  right: JSX.Element;
  rightLarger?: boolean;
}

export default function AppDividedLayout({ params }: { params: AppDividedLayoutProps }) {

  return (
    <div className={styles.main}>

      <div className={params.rightLarger != true ? styles.bodyLeft : styles.bodyRight}>
        {params.left}
      </div>

      <div className={params.rightLarger != true ? styles.bodyRight : styles.bodyLeft}>
        {params.right}
      </div>

    </div>
  );
}