import { BACKGROUND_COLOR, ACCENT_COLOR_DARK } from "src/utils/consts";
import React from 'react';
export const ContactFooter = () => {

  return (
    <div className=""
      style={{
        display: "grid",
        justifyContent: "center",
        alignItems: "center",
        height: '100px',
        backgroundColor: `${ACCENT_COLOR_DARK}`,
      }}
    >

      <h1
        style={{
          color: `${BACKGROUND_COLOR}`
        }}>
        Want to ask a question or give some feedback?
        <span><a
          href={'/contact'}
          style={{
            paddingLeft: '1em',
            textDecorationColor: BACKGROUND_COLOR,
            fontWeight: 'bold',
            textDecoration: 'underline',
          }}
          className={` `}>
          Click here to contact us!
        </a></span>
      </h1>

    </div >
  );
}