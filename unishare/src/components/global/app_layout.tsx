import styles from '@/styles/layouts/AppLayout.module.scss';
import React from 'react';
export interface AppLayoutParams {
  body: JSX.Element;
  header?: JSX.Element;
  footer?: JSX.Element;
}

export const AppLayout = ({ params }: { params: AppLayoutParams }) => {


  return (
    <main className={styles.main}>

      <div className={styles.heading}>
        {params.header}
      </div>
      <div className={styles.body}>
        {params.body}
      </div>
      <div className={styles.footer}>
        {params.footer}
      </div>

    </main>

  );
}