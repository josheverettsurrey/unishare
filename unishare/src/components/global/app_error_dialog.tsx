import AppText from "@/components/base/app_text";
import { useEffect, useState } from "react";
import React from 'react';
const AppErrorDialog = ({ errorMessage = '' }: { errorMessage?: string }) => {

    const [error, setError] = useState('');
    useEffect(() => {
        setError(errorMessage);
    }, [errorMessage]);


    if (error == null || error.length == 0) {
        return <div />;
    }
    return (
        <dialog open onClick={handleTap} style={{ height: '100vh', width: '100%', position: 'absolute', pointerEvents: 'none', background: 'transparent' }} >


            <div style={{ background: "red", width: 300, minHeight: 100 }}>
                <AppText text={error} isCentered={true} />
            </div>

        </dialog>);

    function handleTap(e) {
        e.preventDefault();
        setError('');
    }


}

export default AppErrorDialog;