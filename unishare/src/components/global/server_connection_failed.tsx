import AppText from "../base/app_text";
import AppImage from "../base/app_image";
import ServerDownImage from '../../../public/server_down.svg';
import React from 'react';
const ServerConnectivityFailed = () => {


  return (<div style={{
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    minHeight: '100vh',
    textAlign: 'center'

  }}>
    <AppText text={'Failed to connect to our servers'} style='heading' />

    <AppText text={'It looks like we are having trouble connecting to our servers, please try and refresh the page'} />

    <AppImage src={ServerDownImage} />

  </div >);

}

export default ServerConnectivityFailed;
