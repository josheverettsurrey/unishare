import React, { useState, useRef } from 'react';
import { observer } from 'mobx-react-lite';
import styles from '../../../styles/Contact.module.scss';
import emailjs from "@emailjs/browser";

const ContactPageView = () => {

    const form = useRef();

    const sendEmail = (e) => {
        e.preventDefault();
        emailjs.sendForm('service_bjthz5b', 'template_o8ux38p', form.current, 'i6fl-_rllrqPMKdPX')
            .then((result) => {
                console.log(result.text);
                alert("Your message has been sent successfully!")
            }, (error) => {
                console.log(error.text);
                alert("Oops, something went wrong. Please try again.")
            });
        e.target.reset();
    }

    return (
        <div>
            <div className={styles.container}>
                <form ref={form} onSubmit={sendEmail} className={styles.form && 'content-center'} id="contactForm">
                    <div style={{ display: "flex", flexWrap: "wrap", flexDirection: "column", padding: "0px 0px 32px" }}>
                        <label>Your Name</label>
                        <input type="text" placeholder='Name' name="name" className={styles.input}
                            required />
                    </div>
                    <div style={{ display: "flex", flexWrap: "wrap", flexDirection: "column", padding: "0px 0px 32px" }}>
                        <label>Your Email Address</label>
                        <input type="text" placeholder='Email' name="email" className={styles.input} required />
                    </div>
                    <div style={{ display: "flex", flexWrap: "wrap", flexDirection: "column", padding: "0px 0px 32px" }}>
                        <label>Your Message</label>
                        <textarea placeholder='Your Message' name="message" className={styles.input} required />
                    </div>

                    <button className={' p-5 m-5 rounded-lg bg-black w-40 text-white'} type="submit">
                        Send Message
                    </button>
                </form>
            </div>

        </div>
    )
};

export const ContactView = observer(ContactPageView);