import React, { useState } from 'react';
import { observer } from 'mobx-react-lite';
import { useStoreContext } from "@/pages/_app";
import { UploadRequest } from "../../entities/requests/upload_request";
import AppLoadingSpinner from "@/components/base/app_loading_spinner";
import AppText from "@/components/base/app_text";
import styles from '../../../styles/Upload.module.scss';
import DragAndDrop from './dragDrop';
import { AppButton } from "@/components/base/app_button";

const UploadPageView = () => {

    // Get the upload store from the store context
    const { uploadStore } = useStoreContext();

    // use React hooks to set the title, description and module code.
    // file is not here as we may want to use in other components therefore
    // putting it in the state.
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [module, setModule] = useState('');

    const handleFile = (event) => {

        let fileLocal = null;
        if (event[0] == null) {
            fileLocal = event.target.files[0];
        } else {
            fileLocal = event[0];
        }

        // sets the uploaded file, if the type is a pdf it is accepted
        uploadStore.file = fileLocal;
    }

    // Function that handles sending the file to the server.
    const sendToServer = async () => {
        const request = new UploadRequest({ title: title, description: description, moduleCode: module, file: uploadStore.state.file });
        await uploadStore.upload(request);
    }

    // If the state is loading then show a loading spinner instead
    if (uploadStore.state.isLoading) {
        return <AppLoadingSpinner />;
    }

    // If the state is not loading
    return (
        <div>
            {/*Check if the state is successful then show the upload successful message if it is. Or if there is an error then show that.*/}
            <AppText text={uploadStore.state.success ? 'Upload successful ' : uploadStore.state.hasError ? uploadStore.state.errorMessage! : ``} />

            <div>
                {/* Receiving the properties of the file */}
                <input className={styles.input} onChange={(e) => setTitle(e.target.value)} placeholder="Title" required />
                <input className={styles.input} onChange={(e) => setDescription(e.target.value)} placeholder="Description" />
                <input className={styles.input} onChange={(e) => setModule(e.target.value)} placeholder="Module Code" required />

                <br />

                {/* Uploading the file */}
                <div id="drag-area" className={styles.subBoxPadding}>
                    <DragAndDrop handleDrop={handleFile}>
                        <div className={styles.submissionBox}>
                            <p id="dropbox-text">
                                {uploadStore.state.isEmpty ? 'Drop your file here, or click below.' : 'You have uploaded a file'}
                            </p>
                            <br />

                            <label className={styles.label}>
                                <p>Browse</p>
                                <input id='file' name='file' type='file' onChange={handleFile} className={styles.fileInput} required />
                            </label>
                        </div>
                    </DragAndDrop>
                </div>

            </div>

            {/* Submit the form by sending the file to the server*/}
            <div>
                <AppButton onPressed={sendToServer} text='Upload File' />
            </div>
        </div>
    )
}

export const UploadView = observer(UploadPageView);
