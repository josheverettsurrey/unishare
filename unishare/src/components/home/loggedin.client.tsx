import { useStoreContext } from "@/pages/_app";
import { observer } from "mobx-react";
import { HomeStoreStateAuthenticated } from "src/stores/home_store";
import React from 'react';
export const LoggedIn = observer(() => {

  const { homeStore } = useStoreContext();

  if (homeStore.state instanceof HomeStoreStateAuthenticated)
    return (<div >
      {`Authenticated with user: ${homeStore.state.user.displayName} - You go to ${homeStore.state.user.university.university}`}
    </div>);

  return (<div>
    You are using the application unauthenticated
  </div>);

});
