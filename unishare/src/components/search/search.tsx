import { observer } from "mobx-react";
import { useState } from "react";
import { useStoreContext } from "@/pages/_app";
import { UnishareDocument } from "../../entities/core/unishare_document";
import AppText from "@/components/base/app_text";
import AppLoadingSpinner from "@/components/base/app_loading_spinner";
import styles from "../../../styles/Search.module.scss";
import React from 'react';
import Router from 'next/router';

const _SearchView = () => {

    return (
        <div>
            <SearchBar />
            <br />
            <SearchResults />
        </div>
    );

}

export const SearchView = observer(_SearchView);

const SearchBar = observer(() => {

    const { searchStore, analyticsStore } = useStoreContext();
    const isLoading = searchStore.state.isLoading;

    const [query, setQuery] = useState('');

    async function handleSearch(e) {
        await searchStore.search(query);

        // Logs the search query to our firebase analytics dashboard
        const fileDetails = {
            search_query: query,
        }
        analyticsStore.logSearch(fileDetails);
    }

    return (
        <div className={styles.searchBar}>
            <div className={styles.searchText}>
                <p>Search</p>
            </div>

            <input className={styles.input} placeholder="Search Here" disabled={isLoading} onChange={(e) => setQuery(e.target.value)} />

            <button className={styles.searchButton} onClick={isLoading ? () => { } : handleSearch}>
                <svg
                    width="15"
                    height="15"
                    viewBox="0 0 20 20"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                >
                    <path
                        d="M19 19L13 13L19 19ZM15 8C15 8.91925 14.8189 9.82951 14.4672 10.6788C14.1154 11.5281 13.5998 12.2997 12.9497 12.9497C12.2997 13.5998 11.5281 14.1154 10.6788 14.4672C9.82951 14.8189 8.91925 15 8 15C7.08075 15 6.1705 14.8189 5.32122 14.4672C4.47194 14.1154 3.70026 13.5998 3.05025 12.9497C2.40024 12.2997 1.88463 11.5281 1.53284 10.6788C1.18106 9.82951 1 8.91925 1 8C1 6.14348 1.7375 4.36301 3.05025 3.05025C4.36301 1.7375 6.14348 1 8 1C9.85652 1 11.637 1.7375 12.9497 3.05025C14.2625 4.36301 15 6.14348 15 8Z"
                        stroke="black"
                        strokeWidth="2"
                        strokeLinecap="round"
                        strokeLinejoin="round"
                    />
                </svg>
            </button>
        </div>
    );
});

const SearchResults = observer(() => {
    const { searchStore } = useStoreContext();

    const state = searchStore.state;
    const results: UnishareDocument[] = state.results;


    if (state.hasError) {
        return <AppText text={`An error occurred whilst matching your query. Error message: ${state.errorMessage!}`} />
    }
    else if (state.isLoading) {
        return <AppLoadingSpinner />
    }
    else if (state.isEmpty) {
        return <div>
            <AppText text={'There was no documents that match your query'} />
        </div>
    }

    return (
        <div className={styles.docDiv}>
            {results.map((value, index) => {

                return (
                    <div key={index} style={{ cursor: "pointer" }}>
                        <div onClick={() => Router.push('/files/' + value.id)}>
                            <div className={styles.docObject} style={{ overflow: "hidden", }}>
                                <h3 style={{ fontSize: "20px", fontWeight: "bold" }}>{value.title}</h3>
                                <p>{value.description}</p>
                            </div>

                            <p className={styles.pSm}> {value.creator.university.university} </p>
                            <p className={styles.pMd}> {value.title} </p>
                            <p className={styles.pSm}> Uploaded by: {value.creator.displayName} </p>
                        </div>
                    </div>
                )
            }
            )}
        </div>
    );
});