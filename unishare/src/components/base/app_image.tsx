import styles from '@/styles/base/AppImage.module.scss';
import Image from 'next/image';
import React from 'react';

const AppImage = ({ src, alt }: { src: any, alt?: string }) => {

  return (<div className={styles.main}>
    <Image src={src} alt={alt ?? 'Uh oh, we failed to load this image'}></Image>
  </div >);
}

export default AppImage;