import { PRIMARY_COLOR, BACKGROUND_COLOR } from "../../utils/consts";
import { AppButtonParams } from '../../entities/components/styles';
import React from 'react';
export const AppButton = ({ style, onPressed, text }: AppButtonParams) => {

  return (
    <button className={` p-5 m-5 rounded-lg ${style?.classStyles}`}
      onClick={() => onPressed()}
      style={{
        backgroundColor: `${style?.backgroundColor ?? PRIMARY_COLOR}`,
        color: `${style?.color ?? BACKGROUND_COLOR}`,
        height: style?.height,
        width: style?.width
      }}>
      {text ?? 'Button text'}
    </button>

  );
}