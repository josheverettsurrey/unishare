import styles from '../../../styles/base/AppText.module.css';
import React from 'react';

const AppText = ({ text, style, isCentered = false }: { text: string, style?: string, isCentered?: boolean }) => {

  let textStyle;
  switch (style) {
    case 'heading':
      textStyle = styles.heading;
      break;
    default:
      textStyle = styles.body;
  }


  return (<>
    <label className={textStyle} style={{ textAlign: isCentered ? 'center' : 'left' }}>{text}</label>
  </>);
}

export default AppText;