import React from 'react';

export const AppTextField = ({ val, onChanged, label, obscure, isEnabled = true }: { val: any, onChanged: Function, label: string, obscure?: boolean, isEnabled?: boolean }) => {

  return (<div>
    <label className="text-gray-700 dark:text-gray-200" >{label}</label>
    <input autoComplete='off' disabled={isEnabled == null ? false : !isEnabled} value={val} id={label} type={obscure == null || obscure == false ? 'text' : 'password'} onChange={e => onChanged(e.target.value)} className="block w-full px-4 py-2 mt-2 text-gray-700 bg-white border border-gray-200 rounded-md dark:bg-gray-800 dark:text-gray-300 dark:border-gray-600 focus:border-blue-400 focus:ring-blue-300 focus:ring-opacity-40 dark:focus:border-blue-300 focus:outline-none focus:ring" />
  </div>);
}
