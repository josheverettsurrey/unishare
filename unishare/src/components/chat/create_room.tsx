import { observer } from "mobx-react";
import { useStoreContext } from "@/pages/_app";
import { useState } from "react";
import { CreateRoomRequest } from "../../entities/requests/create_room_request";
import AppText from "@/components/base/app_text";
import { AppTextField } from "@/components/base/app_input_field";
import { AppButton } from "@/components/base/app_button";
import React from 'react';

const CreateRoomDialog = observer(() => {
    const { chatDashboardStore } = useStoreContext();

    const [roomName, setRoomName] = useState('');
    const [moduleCode, setModuleCode] = useState('');

    function handleCreate() {
        const request = new CreateRoomRequest({
            name: roomName,
            moduleCode: moduleCode,
        });

        chatDashboardStore.create(request).then();
    }

    const state = chatDashboardStore.state;
    const showing = chatDashboardStore.showingDialog;

    return <dialog style={{ width: '20%', position: 'absolute', boxShadow: '0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)' }} open={showing}>

        <div style={{ display: 'grid' }}>
            <AppText text={'CREATE A ROOM'} isCentered={true} />

            {!(state.errorMessage == '') ? <AppText text={state.errorMessage} /> : <div />}
            <AppTextField val={roomName} onChanged={setRoomName} label={'Room Name'} />

            <AppTextField val={moduleCode} onChanged={setModuleCode} label={'Module Code'} />

            <AppButton onPressed={() => handleCreate()} text={'Create'} />

            <AppButton onPressed={() => {
                chatDashboardStore.showingDialog = false;
            }} text={'Close'} />
        </div>
    </dialog>;

});

export default CreateRoomDialog;