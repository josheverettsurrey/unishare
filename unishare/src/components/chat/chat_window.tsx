import { useRouter } from "next/router";
import AppText from "@/components/base/app_text";
import { useStoreContext } from "@/pages/_app";
import { ChatRoomStore } from "../../stores/chat_room_store";
import { useEffect, useRef, useState } from "react";
import AppLoadingSpinner from "@/components/base/app_loading_spinner";
import Message from "@/components/chat/message";
import styles from "@/styles/chat/ChatWindow.module.scss";
import { observer } from "mobx-react";
import { AppButton } from "@/components/base/app_button";
import React from 'react';

const ChatWindow = () => {
    /**
     * Get the NextJS router so that we can access the roomId from the query.
     */
    const router = useRouter();

    /**
     * Get the room id from the router query
     */
    const roomId = router.query.roomId;

    /**
     * Get the chat room store from the store context
     */
    const { chatRoomStore } = useStoreContext();

    /**
     * useEffect will be called once when the component renders for the first time and
     * this will cause the chatRoomStore to connect to the desired chat room
     */
    useEffect(() => {
        /**
         * If the room id null then we don't need to make the request as there will be
         * no null room ids
         */
        if (!roomId) return;

        /**
         * We want to open the websocket connection between the client and the server to join the room.
         */
        chatRoomStore.joinRoom(roomId as string).then();
    }, []);


    /**
     * Callback for when the component unmounts, this is so we can disconnect the user
     * from the chat room
     */
    useEffect(() => {
        console.log('unmount')
    }, [])

    /**
     * We want to open the websocket connection between the client and the server to join the room.
     */
    return (<div className={styles.right}>
        <ChatWindowContent roomId={roomId as string} />
    </div>);

};

export default ChatWindow;

const ChatWindowContent = observer(({ roomId }: { roomId?: string }) => {
    const { chatRoomStore } = useStoreContext();
    const state = chatRoomStore.state;

    const [message, setMessage] = useState('');
    const chatParent = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const domNode = chatParent.current;
        if (domNode) {
            domNode.scrollTop = domNode.scrollHeight;
        }

    }, [state.messages]);

    function handleKeyPress(e) {
        e.preventDefault();
        if (e.key == 'Enter') {
            chatRoomStore.send(message, roomId);
            setMessage('');

        }
    }

    if (!roomId) {
        return (<AppText text={'Please select a room to join'} />);
    }

    // From here we know that the id of the group that we want to join is not null / undefined.
    if (state.hasError) {
        return (<div style={{ display: 'grid', width: '100%', height: '100%', justifyContent: 'center', justifyItems: 'centers', alignItems: 'center' }}>
            <AppText text={state.errorMessage} />;
        </div>);
    }

    // If loading then show a loading state.
    if (state.isLoading) {

        return (<div style={{ display: 'grid', width: '100%', height: '100%', justifyContent: 'center', justifyItems: 'centers', alignItems: 'center' }}>

            <div style={{ display: 'grid', justifyContent: 'center', justifyItems: 'centers', alignItems: 'center' }}>
                <AppLoadingSpinner />
                <AppText text={'Connecting'} />
            </div>

        </div>);
    }

    if (state.isSuccessful == false) {
        return (<div style={{ display: 'grid', width: '100%', height: '100%', justifyContent: 'center', justifyItems: 'centers', alignItems: 'center' }}>

            <div style={{ display: 'grid', justifyContent: 'center', justifyItems: 'centers', alignItems: 'center' }}>
                <AppText text={'Connecting to server was unsuccessful'} /><br />
                <AppButton onPressed={() => chatRoomStore.joinRoom(roomId as string)} text={'Try again'} />

            </div>

        </div>);
    }

    const messages = state.messages;

    if (messages == null) {
        return (<div className={styles.chat}>
            <div style={{ height: '10%', width: '100%' }} />

            <AppLoadingSpinner />
            <AppText text={'You are connected - we are just getting your conversation'} isCentered={true} />

        </div>);

    }


    return (<div className={styles.chat}>

        <div ref={chatParent} className={styles.messageContainer}>

            {messages.map((message) => {
                return <Message key={message.id} message={message} />
            })}

        </div>


        <div className={styles.inputContainer}>

            <div className={styles.write}>
                <input type="text" value={message} onKeyUp={handleKeyPress} onChange={(e) => setMessage(e.target.value)} />
            </div>
        </div>
    </div>);
});