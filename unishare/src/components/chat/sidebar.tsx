import { ChatRoom } from "../../entities/core/chat_room";
import { useStoreContext } from "@/pages/_app";
import AppText from "@/components/base/app_text";
import { AppButton } from "@/components/base/app_button";
import ChatRoomTile from "@/components/chat/room_tile";
import styles from '@/styles/chat/Sidebar.module.scss';
import React from 'react';

/**
 * Component that shows a list of available rooms.
 * @constructor
 */
const ChatSidebar = ({ rooms }: { rooms: ChatRoom[] }) => {

    const { chatDashboardStore } = useStoreContext();

    if (rooms.length == 0) {
        return <div style={{ width: 400 }}>
            <AppText text={'There are no available chat rooms. Why dont you create one.'} />
            <br />
            <AppButton onPressed={() => {
                chatDashboardStore.showingDialog = true;
            }} text={'Create a chat room'} />
        </div>
    }
    return <div>
        <div className={`${styles.sidebar}`}>

            <AppText text={'Chats'} style={'heading'} isCentered={true} />

            {rooms.map((room, index) => {
                return (
                    <li key={'room' + '-' + index + '-' + room.id} className={styles.person}>

                        <ChatRoomTile room={room} last={index == rooms.length - 1} />
                    </li>);
            })}

            <AppButton onPressed={() => {
                chatDashboardStore.showingDialog = true;
            }} text={'Create a chat room'} />
        </div>
    </div>
}

export default ChatSidebar;