import { ChatRoom } from "../../entities/core/chat_room";
import { useRouter } from "next/router";
import AppText from "@/components/base/app_text";
import styles from "@/styles/chat/Sidebar.module.scss";
import React from 'react';

const ChatRoomTile = ({ room, last = false }: { room: ChatRoom, last: boolean }) => {
    // Get the next.js router
    const router = useRouter();
    const { roomId } = router.query;

    // When the tile is pressed then we want to navigate to the chat rooms id
    // this will connect to the chat
    async function handleClick() {
        // If the room is already selected then no need to reload the chat.
        if (roomId == room.id) return;
        await router.push(`/chat/${room.id}`)
    }

    return (
        <div className={styles.tile} style={{ marginBottom: last ? 0 : '10px' }} onClick={handleClick}>

            <AppText text={`${room.name} - ${room.module}`} />
        </div>);
};

export default ChatRoomTile;