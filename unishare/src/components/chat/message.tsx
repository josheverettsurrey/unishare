import { ChatMessage } from "../../entities/core/chat_message";
import styles from '@/styles/chat/ChatWindow.module.scss';
import { useStoreContext } from "@/pages/_app";
import React from 'react';

const Message = ({ message }: { message: ChatMessage }) => {

    const { chatRoomStore } = useStoreContext();
    const isMyMessage: boolean = chatRoomStore.currentUser.email == message.sender.email;

    return <div className={`${styles.bubble} ${isMyMessage ? styles.me : styles.you}`} >
        <p style={{ textAlign: isMyMessage ? "right" : "left", fontSize: 10 }}>{message.sender.displayName} - {message.sender.email}</p>
        {message.body}
    </div>;
}


export default Message;