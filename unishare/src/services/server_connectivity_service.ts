import { Subject } from "rxjs";

export class ServerConnectivityEvent {
  constructor(public connected: boolean) { }
}

export class ServerConnectivityService {

  public _serverConnectivityStreamController: Subject<ServerConnectivityEvent>;
  private _isDisposed: boolean = false;

  constructor() {
    /// Makes an object that other classes can observe, when changes are made here
    /// the subscribers will be notified and a function will be called.
    this._serverConnectivityStreamController = new Subject<ServerConnectivityEvent>();
  }

  public setConnected(connected: boolean) {
    if (this._isDisposed) {
    }
    // Adding an event to the stream that will be observed by the subscribers.
    this._serverConnectivityStreamController.next(new ServerConnectivityEvent(connected));
  }


  public dispose() {
    if (this._isDisposed) return;
    // Cloes the stream to avoid memory leaks.
    this._serverConnectivityStreamController.unsubscribe();
    this._isDisposed = true;
  }
}