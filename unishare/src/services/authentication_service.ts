
import { ApiClientUnauthenticated } from "src/entities/api_clients/api_client_base";
import { RegisterResponse } from "src/entities/responses/register_response";
import { DataResponse, DataResponseFailure, DataResponseSuccess } from "../entities/data/data_response";
import { AuthenticationException, AuthenticationExceptionEnum } from "../entities/exceptions/authentication";
import { GlobalException } from "../entities/exceptions/global";
import { UnishareException } from "../entities/exceptions/unishare_exception";
import { LoginRequest } from "../entities/requests/login_request";
import { RegisterRequest } from "../entities/requests/register_request";
import { LoginResponse } from "../entities/responses/login_response";
import { AUTHENTICATION_TOKEN_KEY } from "../utils/consts";
import { ifBrowser } from "../utils/global_utils";
import { CurrentUserService } from "./current_user_service";

/**
 * Definition of the AuthenticationService interface that will be used throughout the
 * application.
 *
 * This interface will contain the publicly visible functions and getters that are required by the
 * application to perform authentication.
 */
export interface IAuthenticationService {

  register(registerRequest: RegisterRequest): Promise<DataResponse<RegisterResponse>>;

  login(loginRequest: LoginRequest): Promise<DataResponse<LoginResponse>>;

  logout(): Promise<void>;

  get authenticationToken(): string;

  get isAuthenticated(): boolean;
}

export class AuthenticationServiceImpl implements IAuthenticationService {

  private _authenticationToken: string = null;
  private _apiClient: ApiClientUnauthenticated;
  private _currentUserService: CurrentUserService;

  constructor(apiClient: ApiClientUnauthenticated, currentUserService: CurrentUserService) {

    this._apiClient = apiClient;
    this._currentUserService = currentUserService;
    ifBrowser(() => {
      this._authenticationToken = localStorage.getItem(AUTHENTICATION_TOKEN_KEY);
    });
  }

  private clearAuthenticationToken() {
    this._authenticationToken = null;
    ifBrowser(() => {
      localStorage.setItem(AUTHENTICATION_TOKEN_KEY, null);
    });
  }

  private setAuthenticationToken(newToken: string) {
    this._authenticationToken = newToken
    ifBrowser(() => {
      localStorage.setItem(AUTHENTICATION_TOKEN_KEY, newToken);
    });
  }

  get isAuthenticated(): boolean {
    if (!this._authenticationToken) {
      return false;
    }
    if (typeof this._authenticationToken != 'undefined' && this._authenticationToken && this._authenticationToken == 'null') {
      return false;
    }
    return !!this._authenticationToken;
  }


  get authenticationToken(): string {
    return this._authenticationToken;
  }

  async register(registerRequest: RegisterRequest): Promise<DataResponse<RegisterResponse>> {

    try {
      const response = await this._apiClient.post("auth/register", registerRequest.toJson());

      // We can try casting the response from the server as a login response.
      const registerResponse = RegisterResponse.fromJson(response.data['payload']);

      this.setAuthenticationToken(registerResponse.authenticationToken);

      this._currentUserService.setUser(registerResponse.user);

      // We can now return the login response.
      return new DataResponseSuccess<RegisterResponse>(registerResponse);
    }
    catch (e) {
      // Here we are catching any errors that might occur during the registration process.
      // We are returning a DataResponseFailure object that will be used to notify the
      // user that the registration process has failed.

      // What we can do is catch the error in the http client and 
      // parse it as a UnishareException, then we can use the UnishareException
      // message to set the error message in the DataResponseFailure object.
      if (e instanceof UnishareException) {
        if (e.error == AuthenticationExceptionEnum.ValidationError) {
          // TODO - review validation message and manage that on the FE
          return DataResponse.failure(AuthenticationException.InvalidCredentials.message);
        }
        return new DataResponseFailure<RegisterResponse>(e.message);
      }

      return new DataResponseFailure<RegisterResponse>(GlobalException.ServerError);
    }
  }

  async login(loginRequest: LoginRequest): Promise<DataResponse<LoginResponse>> {

    try {
      const response = await this._apiClient.post("auth/login", loginRequest.toJson());

      // We can try casting the response from the server as a login response.
      const loginResponse = LoginResponse.fromJson(response.data['payload']);

      this.setAuthenticationToken(loginResponse.sessionToken);

      this._currentUserService.setUser(loginResponse.user);

      // We can now return the login response.
      return new DataResponseSuccess<LoginResponse>(loginResponse);
    }
    catch (e) {
      // Here we are catching any errors that might occur during the login process.
      // We are returning a DataResponseFailure object that will be used to notify the
      // user that the login process has failed.

      // What we can do is catch the error in the http client and 
      // parse it as a UnishareException, then we can use the UnishareException
      // message to set the error message in the DataResponseFailure object.
      if (e instanceof UnishareException) {
        console.log(e);
        if (e.error == AuthenticationExceptionEnum.InvalidCredentials) {
          const exception = AuthenticationException.InvalidCredentials;
          return new DataResponseFailure<LoginResponse>(exception.message);
        }
        return new DataResponseFailure<LoginResponse>(e.message);
      }
      return new DataResponseFailure<LoginResponse>(GlobalException.ServerError);
    }
  }

  async logout(): Promise<void> {
    try {
      if (!this._authenticationToken || !this._currentUserService.user) {
        return;
      }
      // A call to the server to remove the session 
      // doesn't really matter as we will be clearing the token from the client.
      // so won't be able to access the server.
      const response = await this._apiClient.get("auth/logout");
    }
    catch (e) {
      console.log('error was thrown when logging the user out', e);
    } finally {
      ifBrowser(() => {
        this.clearAuthenticationToken();
        this._currentUserService.setUser(null);
      });
      window.location.href = '/401';
    }
  }
}