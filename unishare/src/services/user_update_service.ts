import { ApiClientAuthenticated } from "src/entities/api_clients/api_client_base";
import { DataResponse } from "src/entities/data/data_response";
import { UnishareException } from "src/entities/exceptions/unishare_exception";
import { UpdateUserRequest } from "src/entities/requests/update_user_request";
import { UpdateUserResponse } from "src/entities/responses/update_user_request";

export class UserUpdateService {


  private _apiClient: ApiClientAuthenticated;

  constructor(apiClient: ApiClientAuthenticated) {
    this._apiClient = apiClient;
  }

  async updateUser(request?: UpdateUserRequest): Promise<DataResponse<UpdateUserResponse>> {
    try {

      const response = await this._apiClient.post("user/update", request.toJson());

      return DataResponse.success<UpdateUserResponse>(UpdateUserResponse.fromJson(response.data['payload']));
    } catch (e) {

      if (e instanceof UnishareException) {
        return DataResponse.failure(e.message);
      }

      return DataResponse.failure('Unable to update your profile');
    }
  }

}