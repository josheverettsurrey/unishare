import {Observable, Subject} from 'rxjs';
import {DataResponse} from "../entities/data/data_response";
import {ApiClientAuthenticated} from "../entities/api_clients/api_client_base";
import {FetchChatsResponse} from "../entities/responses/fetch_chats_response";
import {GlobalException} from "../entities/exceptions/global";
import {CreateRoomRequest} from "../entities/requests/create_room_request";
import {CreateRoomResponse} from "../entities/responses/create_room_response";
import {UnishareException} from "../entities/exceptions/unishare_exception";
import {JoinChatRoomResponse} from "../entities/responses/join_chat_room_response";
import {ChatMessage} from "../entities/core/chat_message";
import {User} from "../entities/core/user";
import {University} from "../entities/core/university";
import {IAuthenticationService} from "./authentication_service";

export enum SocketConnectionChangedEventType {
    CONNECTED,
    DISCONNECTED,
    INCOMING_MESSAGE,
    UPDATE,
    ERROR
}

export class SocketConnectionChangedEvent {

    public errorMessage?: string | null;
    public connected: boolean;
    public messages?: ChatMessage[];
    public members?: [];
    public type: SocketConnectionChangedEventType;

    constructor(params: {
        type: SocketConnectionChangedEventType,
        connected: boolean,
        messages?: ChatMessage[],
        errorMessage?: string | null,
        members?: [],
    }) {
        this.connected = params.connected;
        this.messages = params.messages;
        this.errorMessage = params.errorMessage;
        this.type = params.type;
        this.members = params.members;
    }

    public hasError = this.errorMessage == null;
}

export default class ChatService {

    /**
     * Socket instance that will be created when our connection
     * request gets upgraded to a socket connection.
     */
    socketConnection?: WebSocket | null;

    /**
     * Controller exposes a listenable stream for the state layer (MobX store) to listen
     * to for changes in the state of the socket.
     *
     * These events can be added to the stream  (OPEN, CLOSE, LOADING).
     */
    public socketConnectionController: Subject<SocketConnectionChangedEvent>;

    private readonly _apiClient: ApiClientAuthenticated;

    private readonly _authenticationService: IAuthenticationService;

    constructor(apiClient: ApiClientAuthenticated, authenticationService: IAuthenticationService) {
        this._apiClient = apiClient;
        this._authenticationService = authenticationService;
        this.socketConnectionController = new Subject<SocketConnectionChangedEvent>();
    }

    /**
     * Responsible for opening a websocket connection between the client
     * and the server.
     *
     * This function will store that websocket connection in the service
     * so that it can be used in other operations such as messaging etc.
     */
    async connect(id: string) : Promise<void> {

        // Open a websocket connection with the server
        try{
            this.socketConnection = this._apiClient.connectSocket(`chat`, 'message');

            this.socketConnection.onclose = ()=> {
                this.socketConnectionController.next(new SocketConnectionChangedEvent({connected: false, type: SocketConnectionChangedEventType.DISCONNECTED}));
            };

            this.socketConnection.onopen = (connectEvent) => {
                const connectJson = {
                    type: "CONNECT",
                    roomId: id ,
                    token: this._authenticationService.authenticationToken,
                };
                this.socketConnection.send(JSON.stringify(connectJson));
            };

            this.socketConnection.onerror = (error) => {
                this.socketConnectionController.next(new SocketConnectionChangedEvent({connected: false, type: SocketConnectionChangedEventType.ERROR, errorMessage: error.toString()}));
            };

            this.socketConnection.onmessage = (message) => {
                const data = JSON.parse(message.data as string);
                console.info(data);

                switch(data.type) {
                    case 'INCOMING_MESSAGE':
                        this.socketConnectionController.next(new SocketConnectionChangedEvent({
                            connected: true,
                            type: SocketConnectionChangedEventType.INCOMING_MESSAGE,
                            messages: data.messages.map((rawMessage) => ChatMessage.fromJson(rawMessage)),
                        }));
                        break;

                    case 'UPDATE':
                        // cast the messages to their objects and update the store
                        this.socketConnectionController.next(new SocketConnectionChangedEvent({
                            connected: true,
                            type: SocketConnectionChangedEventType.UPDATE,
                            members: data.members,
                            messages: data.messages.map((rawMessage) => ChatMessage.fromJson(rawMessage)),
                        }));
                        break;

                    case 'USER_JOINED':
                        this.socketConnectionController.next(new SocketConnectionChangedEvent({
                            connected: true,
                            type: SocketConnectionChangedEventType.UPDATE,
                            // Add a members field
                        }))
                        break;

                    case 'CONNECTED':
                        this.socketConnectionController.next(new SocketConnectionChangedEvent({connected: true, type: SocketConnectionChangedEventType.CONNECTED}));
                        break;

                    case 'ERROR':
                        const failure = data.failure;
                        console.log('received an error from the server', failure);
                        this.socketConnectionController.next(new SocketConnectionChangedEvent({connected: false, type: SocketConnectionChangedEventType.ERROR, errorMessage: failure}));
                        break;

                    default:
                        this.socketConnectionController.next(new SocketConnectionChangedEvent({connected: true, type: SocketConnectionChangedEventType.ERROR, errorMessage: 'Unknown message received from server'}))
                        break;
                }
            };
        }
        catch(e){
            console.log(e);
        }
    }

    /**
     * Responsible for fetching the chat rooms.
     */
    async fetch(): Promise<DataResponse<FetchChatsResponse>> {
        try {
            const response = await this._apiClient.get('chat/fetch');
            const fetchResponse = FetchChatsResponse.fromJson(response.data['payload']);
            return DataResponse.success(fetchResponse);
        }
        catch(e) {

            // Here we are catching any errors that might occur during the fetch process.
            // We are returning a DataResponseFailure object that will be used to notify the
            // user that the fetch has failed.

            // What we can do is catch the error in the http client and
            // parse it as a UnishareException, then we can use the UnishareException
            // message to set the error message in the DataResponseFailure object.
            if (e instanceof UnishareException) {
                return DataResponse.failure(e.message);
            }
            return  DataResponse.failure(GlobalException.ServerError);
        }
    }

    /**
     * Responsible for sending a message object to the server
     * using the current websocket connection.
     */
    sendMessage(message: string, roomId: string): void {

        if (this.socketConnection == null) {
            this.socketConnectionController.next(new SocketConnectionChangedEvent({
                connected: false,
                type: SocketConnectionChangedEventType.ERROR,
                errorMessage: 'Tried to send a message before connecting to the chat'
            }));
            return;
        }

        this.socketConnection.send(JSON.stringify({type: 'MESSAGE', body: message, roomId}));
    }

    /**
     * Responsible for sending a request to the sever to create
     * a new chat room
     */
    async create(request: CreateRoomRequest) : Promise<DataResponse<CreateRoomResponse>>{

        try {
            const response = await this._apiClient.post('chat/create', request.toJson());
            const createRoomResponse= CreateRoomResponse.fromJson(response.data['payload']);
            return DataResponse.success(createRoomResponse);
        }
        catch(e) {

            // Here we are catching any errors that might occur during the creation process.
            // We are returning a DataResponseFailure object that will be used to notify the
            // user that the creation has failed.

            // What we can do is catch the error in the http client and
            // parse it as a UnishareException, then we can use the UnishareException
            // message to set the error message in the DataResponseFailure object.
            if (e instanceof UnishareException) {
                return DataResponse.failure(e.message);
            }
            return  DataResponse.failure(GlobalException.ServerError);
        }

    }
}



