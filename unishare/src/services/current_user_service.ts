import { User } from "src/entities/core/user";
import { Subject } from 'rxjs';
import { ifBrowser } from "src/utils/global_utils";
import { UpdateUserResponse } from "src/entities/responses/update_user_request";
import { UpdateUserRequest } from "src/entities/requests/update_user_request";
import { DataResponse } from "src/entities/data/data_response";
import { ApiClientAuthenticated } from "src/entities/api_clients/api_client_base";
export class OnCurrentUserPropertyChangedEvent {
  public user: User;
  constructor(user: User) {
    this.user = user;
  }
}

export class CurrentUserService {

  private _user: User;
  private _userStreamController: Subject<OnCurrentUserPropertyChangedEvent> = new Subject<OnCurrentUserPropertyChangedEvent>();


  constructor() {
    this.retrieveUser()

  }

  get userStreamController(): Subject<OnCurrentUserPropertyChangedEvent> {
    return this._userStreamController;
  }

  private retrieveUser(): User {
    ifBrowser(() => {
      const user = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null;
      if (!user) return null;
      const userObject = User.fromJson(user);
      this._user = userObject;

    });
    return this._user;
  }

  setUser = (newUser: User) => {
    this._user = newUser;
    ifBrowser(() => {
      if (newUser == null) {
        localStorage.setItem('user', null);
      } else {
        localStorage.setItem('user', JSON.stringify({ 'email': newUser.email, 'displayName': newUser.displayName, 'university': { 'university': newUser.university.university, 'domain': newUser.university.domain } }));
      }
    }
    )
    this._userStreamController.next(new OnCurrentUserPropertyChangedEvent(newUser));
  }

  get user(): User {
    return this._user ?? this.retrieveUser();
  }
}