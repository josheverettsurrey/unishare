import {ApiClientAuthenticated} from "../entities/api_clients/api_client_base";
import {DataResponse} from "../entities/data/data_response";
import {SearchResponse} from "../entities/responses/search_response";
import {UnishareException} from "../entities/exceptions/unishare_exception";
import {GlobalException} from "../entities/exceptions/global";
import {UploadRequest} from "../entities/requests/upload_request";
import {UploadResponse} from "../entities/responses/upload_response";
import {FetchRequest} from "../entities/requests/fetch_request";
import {FetchResponse} from "../entities/responses/fetch_response";

export class DocumentService {
    private _apiClient: ApiClientAuthenticated;
    constructor(_apiClientAuthenticated) {
        this._apiClient = _apiClientAuthenticated;
    }

    async search(query: string): Promise<DataResponse<SearchResponse>>{

        try{
            const response = await this._apiClient.get(`document/search`, {
                'query': query
            });

            const searchResults = SearchResponse.fromJson(response.data['payload']);
            return DataResponse.success<SearchResponse>(searchResults);

        }
        catch(e) {

            // Here we are catching any errors that might occur during the search process.
            // We are returning a DataResponseFailure object that will be used to notify the
            // user that the searching has failed.

            // What we can do is catch the error in the http client and
            // parse it as a UnishareException, then we can use the UnishareException
            // message to set the error message in the DataResponseFailure object.
            if (e instanceof UnishareException) {
                return DataResponse.failure(e.message);
            }
            return  DataResponse.failure(GlobalException.ServerError);
        }
    }

    async upload(request: UploadRequest): Promise<DataResponse<UploadResponse>>{
        try {
            const response = await this._apiClient.file('document/upload/', request.toJson(), true
            );
            const uploadResponse = UploadResponse.fromJson(response.data['payload']);
            return DataResponse.success<UploadResponse>(uploadResponse);

        }
        catch(e) {

            // Here we are catching any errors that might occur during the upload process.
            // We are returning a DataResponseFailure object that will be used to notify the
            // user that the uploading has failed.

            // What we can do is catch the error in the http client and
            // parse it as a UnishareException, then we can use the UnishareException
            // message to set the error message in the DataResponseFailure object.
            if (e instanceof UnishareException) {
                return DataResponse.failure(e.message);
            }
            return  DataResponse.failure(GlobalException.ServerError);
        }
    }

    async fetch(request: FetchRequest) : Promise<DataResponse<FetchResponse>> {
        try {
            const response = await this._apiClient.post('document/fetch', request.toJson());
            const uploadResponse = FetchResponse.fromJson(response.data['payload']);
            return DataResponse.success<FetchResponse>(uploadResponse);
        }catch (e){
            if (e instanceof UnishareException) {
                return DataResponse.failure(e.message);
            }
            return  DataResponse.failure(GlobalException.ServerError);
        }
    }
}