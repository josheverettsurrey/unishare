import {ViewingStore, ViewingStoreState} from 'unishare/src/stores/viewing_store.ts';
import {FetchResponse} from "unishare/src/entities/responses/fetch_response.ts";
import {FetchRequest} from "unishare/src/entities/requests/fetch_request.ts";
import {DataResponse} from "src/entities/data/data_response";
import {DocumentService} from "unishare/src/services/document_service.ts";
import {UnishareDocument} from "unishare/src/entities/core/unishare_document.ts";
import {Blob} from "buffer";

describe('[ViewingStore]', () => {

    const someValues = [{ name: 'test user' }];
    const str = JSON.stringify(someValues);
    const file = new Blob([str], {type: 'application/pdf'});
    const unishareDocument = new UnishareDocument({title: "Test Title", description: "Test Description", moduleCode: "Test Module Code", creator: "Andrew", uuid: "1", id: "1", university: "University of Surrey"});

    const fetchFailure = {"_document": undefined, "_error": {"_document": undefined, "_error": "ServerError - We ran into a problem on our end. Please try again later.", "_isLoading": false, "_success": false, "file": undefined}, "_isLoading": false, "_success": false, "file": undefined};
    const documentServiceFetchFailure = jest.fn().mockImplementation(()=>{ 
        return DataResponse.failure(fetchFailure);
    });

    const fetchSuccess = new FetchResponse(file, unishareDocument);
    const documentServiceFetchSuccess = jest.fn().mockImplementation(()=>{ 
        return DataResponse.success(fetchSuccess);
    });

    it ('Should enter initial state when store is first created', async () => {
        const documentService = new DocumentService();
        const viewingStore = new ViewingStore(documentService);
        
        expect(viewingStore.state).toStrictEqual(ViewingStoreState.initial());
    });

    it('Should fetch document successfully', async () => {
        const documentService = new DocumentService();
        documentService.fetch = req => documentServiceFetchSuccess(req);

        const viewingStore = new ViewingStore(documentService);
        const fetchRequest = new FetchRequest({id: "1"});
        const result = await viewingStore.fetch(fetchRequest);

        expect(viewingStore.state.document).toEqual(unishareDocument);
        expect(viewingStore.state.file).toEqual(file);     
        expect(viewingStore.state.success).toEqual(true);
    });

    it('Should enter an error state after a failed fetch response', async () => {
        const documentService = new DocumentService();
        documentService.fetch = req => documentServiceFetchFailure(req);

        const viewingStore = new ViewingStore(documentService);
        const fetchRequest = new FetchRequest({id: "1"});
        await viewingStore.fetch(fetchRequest);

        expect(viewingStore.state).toEqual(ViewingStoreState.error(fetchFailure));
        expect(viewingStore.state.success).toEqual(false);
    });


    // as the loading state can only reached at the start of the upload function midway through an async function call that does not return the store with the loading state, we will test that the state can be set to loading instead
    it ('Should be able to set the state to loading', async () => {

        const documentService = new DocumentService();
        const viewingStore = new ViewingStore(documentService);
        viewingStore.state = ViewingStoreState.loading();

        expect(viewingStore.state).toStrictEqual(ViewingStoreState.loading());
        expect(viewingStore.state.isLoading).toEqual(true);

    });

});