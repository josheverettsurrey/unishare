import { BearerTokenInterceptor } from "../../src/entities/api_clients/interceptors";
import { AuthenticationServiceImpl } from "../../src/services/authentication_service";


describe('[BearerTokenInterceptor]', () => {

  const mockToken = 'example-token';
  const mockAuthServiceTokenGetter = jest.fn().mockImplementation(() => mockToken);

  const mockRequestConfig = {
    url: 'example-url',
    method: 'POST',
    data: {},
    headers: {},
  };

  it('Should inject token into axios request options', () => {

    const authService = new AuthenticationServiceImpl();

    jest.spyOn(authService, 'authenticationToken', 'get').mockReturnValue(mockToken)

    const interceptor = new BearerTokenInterceptor(authService);

    expect(mockRequestConfig.headers.Authorization).toBeUndefined();

    const requestConfig = interceptor.onRequest(mockRequestConfig);

    expect(requestConfig.headers.Authorization).toBe(`Bearer ${mockToken}`);
  })
});