
import {
    LoginStore,
    LoginStoreStateAuthenticated,
    LoginStoreStateError,
    LoginStoreStateUnauthenticated
} from "../../src/stores/login_store";

import {LoginRequest} from "../../src/entities/requests/login_request";
import {AuthenticationServiceImpl} from "../../src/services/authentication_service";
import {DataResponse} from "../../src/entities/data/data_response";
import {LoginResponse} from "../../src/entities/responses/login_response";
import {User} from "../../src/entities/core/user";
import {University} from "../../src/entities/core/university";


const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));
const mockAuthToken = 'token';
describe('[LoginStore]', () => {

    const authServiceSuccessLoginFunction = jest.fn().mockImplementation(()=>{
        return DataResponse.success(new LoginResponse(mockAuthToken, mockUser))
    });

    const authServiceFailedLoginFunction = jest.fn().mockImplementation(()=>{
        return DataResponse.failure('Test failure')
    });


    it('Should have an initial state of [LoginStoreStateUnauthenticated]', ()=> {


        const loginStore = new LoginStore();

        expect(loginStore.state).toStrictEqual( new LoginStoreStateUnauthenticated());
    });

    it('After authentication service returns with a success then the state should be [LoginStoreStateAuthenticated].', async () => {

        const authService = new AuthenticationServiceImpl();

        authService.login = req => authServiceSuccessLoginFunction(req);

        const loginStore = new LoginStore(authService);

        const loginRequest = new LoginRequest(
            'example@email.com', 'password'
        );

        await loginStore.login(loginRequest)

        expect(authServiceSuccessLoginFunction).toHaveBeenLastCalledWith(loginRequest);


        expect(loginStore.state).toStrictEqual( new LoginStoreStateAuthenticated());
    });

    it('After authentication service returns with a failure then the state should be [LoginStoreStateUnauthenticated].', async () => {


        const authService = new AuthenticationServiceImpl();

        authService.login = req => authServiceFailedLoginFunction(req);

        const loginStore = new LoginStore(authService);

        const loginRequest = new LoginRequest(
            'example@email.com', 'password'
        );

        await loginStore.login(loginRequest)

        expect(authServiceFailedLoginFunction).toHaveBeenLastCalledWith(loginRequest);


        expect(loginStore.state).toStrictEqual( new LoginStoreStateError('Test failure'));
    });

    it('when state is [LoginStoreStateError] we can call [dismissError()] to change state to [LoginStoreStateUnauthenticated].', async () => {


        const authService = new AuthenticationServiceImpl();

        authService.login = req => authServiceFailedLoginFunction(req);

        const loginStore = new LoginStore(authService);

        const loginRequest = new LoginRequest(
            'example@email.com', 'password'
        );

        await loginStore.login(loginRequest)

        expect(loginStore.state).toStrictEqual( new LoginStoreStateError('Test failure'));

        loginStore.dismissError()

        expect(loginStore.state).toStrictEqual( new LoginStoreStateUnauthenticated());
    });


});