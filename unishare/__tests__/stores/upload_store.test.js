
import {UploadStore, UploadStoreState} from 'unishare/src/stores/upload_store.ts';
import {UploadResponse} from "unishare/src/entities/responses/upload_response.ts";
import {UploadRequest} from "unishare/src/entities/requests/upload_request.ts";
import { DataResponse } from "src/entities/data/data_response";
import {DocumentService} from "unishare/src/services/document_service.ts";
import {Blob} from "buffer";

describe('[UploadStore]', () => {

    const documentServiceUploadSuccess = jest.fn().mockImplementation(() => {
        return DataResponse.success(new UploadResponse({message: "Successfully uploaded document."}))
    });

    const uploadDocumentErrorState = {"_error": {"error": "Failed to upload document."}, "_isLoading": false, "_success": false, "file": null};
    const documentServiceUploadFailure = jest.fn().mockImplementation(() => {
        return DataResponse.failure({error: "Failed to upload document."})
    });


    it ('Should enter initial state when store is first created', async () => {
            
        const documentService = new DocumentService();
        const uploadStore = new UploadStore(documentService);
        
        expect(uploadStore.state).toStrictEqual(UploadStoreState.initial());

    });

    it ('Should be able to upload a PDF file to the state', async () => {

        const documentService = new DocumentService();
        documentService.upload = req => documentServiceUploadSuccess(req);
        
        const someValues = [{ name: 'test user' }];
        const str = JSON.stringify(someValues);
        const file = new Blob([str], {type: 'application/pdf'});
                
        const uploadStore = new UploadStore(documentService);
        
        uploadStore.file = file;
        expect(uploadStore.state.file).toEqual(file);

    });

    it ('Should not be able to upload a PDF file without a title', async () => {

        const documentService = new DocumentService();
        documentService.upload = req => documentServiceUploadSuccess(req);
        
        const someValues = [{ name: 'test user' }];
        const str = JSON.stringify(someValues);
        const file = new Blob([str], {type: 'application/pdf'});
                
        const uploadStore = new UploadStore(documentService);
        
        uploadStore.file = file;
        
        const uploadRequest = new UploadRequest({title: "", description: "Test Description", moduleCode: "Test Module Code", file: file});

        await uploadStore.upload(uploadRequest);
        
        expect(uploadStore.state).toEqual({"_error": "You need to enter a title.", "_isLoading": false, "_success": false, "file": file});

    });


    it ('Should not be able to upload a file that is not type PDF to the state', async () => {

        const documentService = new DocumentService();
        documentService.upload = req => documentServiceUploadSuccess(req);
        
        const someValues = [{ name: 'test user' }];
        const str = JSON.stringify(someValues);
        const file = new Blob([str], {type: 'application/json'});
                
        const uploadStore = new UploadStore(documentService);
        
        uploadStore.file = file;
        expect(uploadStore.state.file).toEqual(null);
        
        const uploadRequest = new UploadRequest({title: "Test Title", description: "Test Description", moduleCode: "Test Module Code", file: file});

        await uploadStore.upload(uploadRequest);
        
        expect(uploadStore.state).toEqual({"_error": "You need to upload a file to send it.", "_isLoading": false, "_success": false, "file": null});

    });
    
    it ('Should return a successful upload response', async () => {

        const documentService = new DocumentService();
        documentService.upload = req => documentServiceUploadSuccess(req);
        
        const someValues = [{ name: 'test user' }];
        const str = JSON.stringify(someValues);
        const file = new Blob([str], {type: 'application/pdf'});
        
        
        const uploadStore = new UploadStore(documentService);
        
        const uploadRequest = new UploadRequest({title: "Test Title", description: "Test Description", moduleCode: "Test Module Code", file: file});
        
        uploadStore.file = file;
        expect(uploadStore.state.file).toEqual(file);

        await uploadStore.upload(uploadRequest);
        expect(uploadStore.state.success).toStrictEqual(true);

    });

    it ('Should enter an error state after a failed upload response', async () => {

        const documentService = new DocumentService();
        documentService.upload = req => documentServiceUploadFailure(req);
        
        const someValues = [{ name: 'test user' }];
        const str = JSON.stringify(someValues);
        const file = new Blob([str], {type: 'application/pdf'});
        
        
        const uploadStore = new UploadStore(documentService);
        
        uploadStore.file = file;

        const uploadRequest = new UploadRequest({title: "Test Title", description: "Test Description", moduleCode: "Test Module Code", file: file});

        await uploadStore.upload(uploadRequest);
        expect(uploadStore.state).toEqual(uploadDocumentErrorState);

    });

    // as the loading state can only reached at the start of the upload function midway through an async function call that does not return the store with the loading state, we will test that the state can be set to loading instead
    it ('Should be able to set the state to loading', async () => {

        const documentService = new DocumentService();
        const uploadStore = new UploadStore(documentService);
        uploadStore.state = UploadStoreState.loading();

        expect(uploadStore.state).toStrictEqual(UploadStoreState.loading());

    });

});