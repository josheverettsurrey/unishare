
import {AuthenticationServiceImpl} from "../../src/services/authentication_service";
import {DataResponse} from "../../src/entities/data/data_response";

import {User} from "../../src/entities/core/user";
import {University} from "../../src/entities/core/university";
import {RegisterResponse} from "../../src/entities/responses/register_response";
import {
    RegisterStore,
    RegisterStoreStateError,
    RegisterStoreStateInitial,
    RegisterStoreStateSuccess
} from "../../src/stores/register_store";


describe('[RegisterStore]', () => {

    const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));
    const mockAuthToken = 'token';
    const mockRegisterRequest = new RegisterResponse(mockAuthToken, mockUser, 'Test message' );

    const authServiceSuccessRegisterFunction = jest.fn().mockImplementation(() => {
        return DataResponse.success(mockRegisterRequest)
    });

    const authServiceFailedRegisterFunction = jest.fn().mockImplementation(() => {
        return DataResponse.failure('Test failure')
    });


    it('Should have an initial state of [RegisterStoreStateInitial]', () => {

        const registerStore = new RegisterStore();

        expect(registerStore.state).toStrictEqual(new RegisterStoreStateInitial());
    });

    it('Should have a state of [RegisterStoreStateSuccess] if authentication service register is success', async () => {

        const authService = new AuthenticationServiceImpl();

        authService.register = req => authServiceSuccessRegisterFunction(req);

        const registerStore = new RegisterStore(authService);

        await registerStore.register(mockRegisterRequest);

        expect(registerStore.state).toStrictEqual(new RegisterStoreStateSuccess(mockRegisterRequest.message));
    });

    it('Should have a state of [RegisterStoreStateError] if authentication service register fails', async () => {

        const authService = new AuthenticationServiceImpl();

        authService.register = req => authServiceFailedRegisterFunction(req);

        const registerStore = new RegisterStore(authService);

        await registerStore.register(mockRegisterRequest);

        expect(registerStore.state).toStrictEqual(new RegisterStoreStateError('Test failure'));
    });

    it('State is [RegisterStoreStateInitial] after dismiss error', async () => {

        const authService = new AuthenticationServiceImpl();

        authService.register = req => authServiceFailedRegisterFunction(req);

        const registerStore = new RegisterStore(authService);

        await registerStore.register(mockRegisterRequest);

        expect(registerStore.state).toStrictEqual(new RegisterStoreStateError('Test failure'));

        registerStore.dismissError();

        expect(registerStore.state).toStrictEqual(new RegisterStoreStateInitial());

    });



});