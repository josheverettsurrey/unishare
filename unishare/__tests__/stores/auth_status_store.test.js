
import {
    AuthStatusStore
} from "../../src/stores/auth_status_store";
import { AuthenticationServiceImpl } from "../../src/services/authentication_service";

import { User } from "../../src/entities/core/user";
import { University } from "../../src/entities/core/university";
import { RegisterResponse } from "../../src/entities/responses/register_response";
import { ApiClientUnauthenticated } from "../../src/entities/api_clients/api_client_base";
import { AppConfig } from "../../src/entities/config/config";
import { LoginRequest } from "../../src/entities/requests/login_request";
import { CurrentUserService } from "../../src/services/current_user_service";
import { RegisterRequest } from "../../src/entities/requests/register_request";


describe('[AuthStatusStore]', () => {

    const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));
    const mockAuthToken = 'token';
    const mockLoginResponse = {
        "data": {
            "payload": {
                "token": {
                    "authToken": mockAuthToken,
                },
                "user": {
                    "email": mockUser.email,
                    "displayName": mockUser.displayName,
                    "university": {
                        "university": mockUser.university.university,
                        "domain": mockUser.university.domain,
                    },
                },
                "message": "Welcome to Unishare",
            }
        },
        status: 200,
        statusText: 'All good',
        headers: {},
        config: {},
    };
    const mockFailureResponse = {
        "data": {
            "error": "test_error",
            "message": "test_message"
        },
        status: 401,
        statusText: 'All good',
        headers: {},
        config: {},
    }

    const loginRequest = new LoginRequest("email@email.com", "password");
    const registerRequest = new RegisterRequest(mockUser, 'password')
    const registerResponse = new RegisterResponse(mockAuthToken, mockUser, 'Welcome to Unishare')

    const apiClientLoginFunctionSuccess = jest.fn().mockImplementation(() => mockLoginResponse);
    const apiClientLoginFunctionFailed = jest.fn().mockImplementation(() => mockFailureResponse);
    const apiClientError = jest.fn().mockImplementation(() => { throw new Error('Error') });


    it('Auth status should be false when unauthenticated', () => {
        const authenticationService = new AuthenticationServiceImpl();
        
        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(false);
    });


    it('Auth status should be true when login is successful', async () => {
        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(true);
    });

    it('Auth status should be unauthenticated when login fails', async () => {
        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionFailed(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(false);
    });

    it('Auth status should be true when register is successful', async () => {
        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.register(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(true);
    });

    it('Auth status should be unauthenticated when register fails', async () => {
        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionFailed(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.register(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(false);
    });

});