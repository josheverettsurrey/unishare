
import {
    ProfileStore,
} from "../../src/stores/profile_store";
import { DataResponse } from "../../src/entities/data/data_response";
import {CurrentUserService} from "../../src/services/current_user_service";
import {UserUpdateService} from "../../src/services/user_update_service";
import {UpdateUserRequest} from "unishare/src/entities/requests/update_user_request.ts";
import {UpdateUserResponse} from "unishare/src/entities/responses/update_user_request.ts";
import {
    AuthStatusStore
} from "../../src/stores/auth_status_store";
import { AuthenticationServiceImpl } from "../../src/services/authentication_service";

import { ProfileStoreState } from "../../src/stores/profile_store";
import { User } from "../../src/entities/core/user";
import { University } from "../../src/entities/core/university";
import { RegisterResponse } from "../../src/entities/responses/register_response";
import { ApiClientUnauthenticated } from "../../src/entities/api_clients/api_client_base";
import { AppConfig } from "../../src/entities/config/config";
import { LoginRequest } from "../../src/entities/requests/login_request";

describe('[ProfileStore]', () => {


    const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));
    const mockUser2 = new User('email@email.com', 'andrew', new University('University of Surrey', 'surrey.ac.uk'));

    const mockAuthToken = 'token';
    const mockLoginResponse = {
        "data": {
            "payload": {
                "token": {
                    "authToken": mockAuthToken,
                },
                "user": {
                    "email": mockUser.email,
                    "displayName": mockUser.displayName,
                    "university": {
                        "university": mockUser.university.university,
                        "domain": mockUser.university.domain,
                    },
                },
                "message": "Welcome to Unishare",
            }
        },
        status: 200,
        statusText: 'All good',
        headers: {},
        config: {},
    };
    const mockLogoutResponse = {
        "success": true,
    };
    const mockFailureResponse = {
        "error": "Unable to log you out", 
        "loading": false, 
        "user": undefined
    }

    const loginRequest = new LoginRequest("email@email.com", "password");
    const apiClientLoginFunctionSuccess = jest.fn().mockImplementation(() => mockLoginResponse);
    const LogoutFunctionSuccess = jest.fn().mockImplementation(() => mockLogoutResponse);
    const LogoutFunctionFailed = jest.fn().mockImplementation(() => mockFailureResponse);

    const mockUpdateSuccessResponse = {

    };
    const UserUpdateServiceSuccess = jest.fn().mockImplementation(() => {
        const updateUserResponse = new UpdateUserResponse(mockUser2);
        return DataResponse.success(updateUserResponse);
    });
    
    it('Should not be in loading state', async () => {
    
        const currentUserService = new CurrentUserService();
        const updateUserService = new UserUpdateService();

        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser(mockUser);
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(true);

        const profileStore = new ProfileStore(new ProfileStoreState(undefined, undefined, false));

        expect(profileStore.state).toEqual({"error": undefined, "loading": false, "user": undefined});
    });
    
    it('Should be able to update user profile display name', async () => {
    
        const currentUserService = new CurrentUserService();
        const updateUserService = new UserUpdateService();

        updateUserService.updateUser = req => UserUpdateServiceSuccess(req);

        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser(mockUser);
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(true);

        const profileStore = new ProfileStore(currentUserService, updateUserService, authenticationService);

        // verify that display name changes from 'test' to 'andrew'
        expect(profileStore.state.user.displayName).toStrictEqual("test");

        const updateUserRequest = new UpdateUserRequest({user: mockUser2, password: "password"});
        
        await profileStore.updateProfile(updateUserRequest, "password");
        expect(profileStore.state.user.displayName).toStrictEqual("andrew");

    });

    // profile store


    it('Should be able to logout', async () => {
    
        const currentUserService = new CurrentUserService();
        const updateUserService = new UserUpdateService();
        
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser(mockUser);
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(true);
        
        const profileStore = new ProfileStore(currentUserService, updateUserService, authenticationService);

        expect(profileStore.state).toBeDefined();

        profileStore.logout = req => LogoutFunctionSuccess(req);

        const result = await profileStore.logout();

        expect(result).toStrictEqual({"success": true});
    });

    it('Should be unable to logout', async () => {
    
        const currentUserService = new CurrentUserService();
        const updateUserService = new UserUpdateService();
        
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser(mockUser);
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        const authStatusStore = new AuthStatusStore(authenticationService);
        expect(authStatusStore.isAuthenticated).toBe(true);
        
        const profileStore = new ProfileStore(currentUserService, updateUserService, authenticationService);

        expect(profileStore.state).toBeDefined();

        profileStore.logout = req => LogoutFunctionFailed(req);

        const result = await profileStore.logout();

        expect(result).toBe(mockFailureResponse);
    });

});