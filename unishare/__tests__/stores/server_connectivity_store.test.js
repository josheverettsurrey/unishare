
import {
    ServerConnectivityStore,
    ServerConnectivityState,
    ServerConnectivityStateSuccess,
    ServerConnectivityStateError
} from 'unishare/src/stores/server_connectivity_store.ts';
import {ServerConnectivityEvent, ServerConnectivityService} from 'unishare/src/services/server_connectivity_service.ts';

describe('[ServerConnectivityStore]', () => {

    it('Should have have an initial state of ServerConnectivityStateSuccess', () => {
        const serverConnStore = new ServerConnectivityStore(new ServerConnectivityService());

        expect(serverConnStore.state).toStrictEqual(new ServerConnectivityStateSuccess());
    });

    it('Should be able to set state to error state', () => {
        const serverConnStore = new ServerConnectivityStore(new ServerConnectivityService());

        serverConnStore.state = new ServerConnectivityStateError();

        expect(serverConnStore.state).toStrictEqual(new ServerConnectivityStateError());
    });

});