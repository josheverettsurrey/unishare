
import {SearchStore, SearchStoreState} from 'unishare/src/stores/search_store.ts';
import {SearchResponse} from "unishare/src/entities/responses/search_response.ts";
import { DataResponse } from "src/entities/data/data_response";
import {DocumentService} from "unishare/src/services/document_service.ts";

describe('[SearchStore]', () => {
    
    const results = {id: 1};
    const documentServiceSuccessSearch = jest.fn().mockImplementation(()=>{ 
        return DataResponse.success(new SearchResponse({results: results }))
    });
    const documentServiceFailedSearch = jest.fn().mockImplementation(()=>{ 
        return DataResponse.failure({error: "An error occurred during the search." });
    });
    const searchSuccessResults = {"results": {"id": 1}};


    it ('Should enter initial state when store is first created', async () => {
            
        const documentService = new DocumentService();
        const searchStore = new SearchStore(documentService);
        
        expect(searchStore.state).toStrictEqual(SearchStoreState.initial());

    });
    
    it ('Should return a successful search response', async () => {
            
        const documentService = new DocumentService();
        documentService.search = req => documentServiceSuccessSearch(req);
        const searchStore = new SearchStore(documentService);
        await searchStore.search("TestQuery");

        expect(searchStore.state.results).toEqual(searchSuccessResults);

    });

    it ('Should enter an error state after a failed search', async () => {
            
        const documentService = new DocumentService();
        documentService.search = req => documentServiceFailedSearch(req);
        const searchStore = new SearchStore(documentService);
        await searchStore.search("TestQuery");

        expect(searchStore.state).toStrictEqual(SearchStoreState.error( {"error": "An error occurred during the search." }));

    });

    // as the loading state can only reached at the start of the search function midway through an async function call that does not return the store with the loading state, we will test that the state can be set to loading instead
    it ('Should be able to set the state to loading', async () => {

        const documentService = new DocumentService();
        const searchStore = new SearchStore(documentService);
        searchStore.state = SearchStoreState.loading();

        expect(searchStore.state).toStrictEqual(SearchStoreState.loading());

    });

});