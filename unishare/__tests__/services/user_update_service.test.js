
import { User } from "../../src/entities/core/user";
import { University } from "../../src/entities/core/university";
import { DataResponse } from "src/entities/data/data_response";
import { UpdateUserRequest } from "src/entities/requests/update_user_request";
import {CurrentUserService} from "../../src/services/current_user_service";
import {UserUpdateService} from "../../src/services/user_update_service";

import { AppConfig } from "../../src/entities/config/config";
import { ApiClientUnauthenticated } from "../../src/entities/api_clients/api_client_base";

describe('[UpdateUserService]', () => {
    

    const updateServiceSuccessResponse = {"user": {"_displayName": "test", "_email": "email@email.com", "_university": {"domain": "surrey.ac.uk", "university": "University of Surrey"}}}
    
    const updateServiceSuccess = jest.fn().mockImplementation(() => updateServiceSuccessResponse);
        
    const updateServiceFailure = jest.fn().mockImplementation(() => {
        return DataResponse.success({results: "failure"});
    });

    const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));
    const mockUser2 = new User('email@email.com', 'andrew', new University('University of Surrey', 'surrey.ac.uk'));

    const mockAuthToken = 'token';
    const mockLoginResponse = {
        "data": {
            "payload": {
                "token": {
                    "authToken": mockAuthToken,
                },
                "user": {
                    "email": mockUser.email,
                    "displayName": mockUser.displayName,
                    "university": {
                        "university": mockUser.university.university,
                        "domain": mockUser.university.domain,
                    },
                },
                "message": "Welcome to Unishare",
            }
        },
        status: 200,
        statusText: 'All good',
        headers: {},
        config: {},
    };

    const mockUpdateFailureResponse = {
        "data": {
            "error": "Unable to update user",
        }
    };
    const apiClientLoginFunctionSuccess = jest.fn().mockImplementation(() => mockLoginResponse);

    const updateFailure = jest.fn().mockImplementation(() => mockUpdateFailureResponse);

    // test that updateuser works
    it('Should be able to update user', async () => {
       
        const currentUserService = new CurrentUserService();
        
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);
        
        currentUserService.setUser(mockUser);
        apiClient.post = req => apiClientLoginFunctionSuccess(req);
        
        const updateUserService = new UserUpdateService(apiClient);

        const updateUserRequest = new UpdateUserRequest(mockUser2, "password");

        const result = await updateUserService.updateUser(updateUserRequest);

        expect(result).toEqual(DataResponse.success(updateServiceSuccessResponse));
    });

    it('Should give an error if updating user fails', async () => {
       
        const currentUserService = new CurrentUserService();
        
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);
        
        currentUserService.setUser(mockUser);
        apiClient.post = req => apiClientLoginFunctionSuccess(req);
        
        const updateUserService = new UserUpdateService(apiClient);

        const updateUserRequest = new UpdateUserRequest(mockUser2, "password");

        updateUserService.updateUser = req => updateFailure(req);

        const result = await updateUserService.updateUser(updateUserRequest);

        expect(result).toEqual(mockUpdateFailureResponse);
    });

});