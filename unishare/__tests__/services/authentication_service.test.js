import { AuthenticationServiceImpl } from "../../src/services/authentication_service";

import { LoginResponse } from "../../src/entities/responses/login_response";
import { User } from "../../src/entities/core/user";
import { University } from "../../src/entities/core/university";
import { RegisterResponse } from "../../src/entities/responses/register_response";
import { ApiClientUnauthenticated } from "../../src/entities/api_clients/api_client_base";
import { AppConfig } from "../../src/entities/config/config";
import { LoginRequest } from "../../src/entities/requests/login_request";
import AxiosResponse from 'axios';
import { CurrentUserService } from "../../src/services/current_user_service";
import { RegisterRequest } from "../../src/entities/requests/register_request";

describe('[AuthenticationService]', () => {

    const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));
    const mockAuthToken = 'token';
    const mockLoginResponse = {
        "data": {
            "payload": {
                "token": {
                    "authToken": mockAuthToken,
                },
                "user": {
                    "email": mockUser.email,
                    "displayName": mockUser.displayName,
                    "university": {
                        "university": mockUser.university.university,
                        "domain": mockUser.university.domain,
                    },
                },
                "message": "Welcome to Unishare",
            }
        },
        status: 200,
        statusText: 'All good',
        headers: {},
        config: {},
    };
    const mockFailureResponse = {
        "data": {
            "error": "test_error",
            "message": "test_message"
        },
        status: 401,
        statusText: 'All good',
        headers: {},
        config: {},
    }

    const loginRequest = new LoginRequest("email@email.com", "password");
    const registerRequest = new RegisterRequest(mockUser, 'password')
    const registerResponse = new RegisterResponse(mockAuthToken, mockUser, 'Welcome to Unishare')

    const apiClientLoginFunctionSuccess = jest.fn().mockImplementation(() => mockLoginResponse);
    const apiClientLoginFunctionFailed = jest.fn().mockImplementation(() => mockFailureResponse);
    const apiClientError = jest.fn().mockImplementation(() => { throw new Error('Error') });
    it('Authentication token is null when created and [isAuthenticated] is false', () => {

        const authenticationService = new AuthenticationServiceImpl();

        expect(authenticationService.isAuthenticated).toBe(false);
        expect(authenticationService.authenticationToken).toBe(null);
    });

    it('Authentication token is updated when authentication was successful', async () => {

        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        expect(authenticationService.isAuthenticated).toBe(true);
        expect(authenticationService.authenticationToken).toBe(mockAuthToken);
    });

    it('Authentication token is not updated if login failed', async () => {

        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionFailed(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        expect(authenticationService.isAuthenticated).toBe(false);
        expect(authenticationService.authenticationToken).toBe(null);

    });

    it('Authentication token is not updated if api client throws exception during login', async () => {

        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientError(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.login(loginRequest);

        expect(authenticationService.isAuthenticated).toBe(false);
        expect(authenticationService.authenticationToken).toBe(null);
    });

    it('Authentication token is updated after successful registration', async () => {

        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionSuccess(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.register(registerRequest);

        expect(authenticationService.isAuthenticated).toBe(true);
        expect(authenticationService.authenticationToken).toBe(mockAuthToken);
    });


    it('Authentication token is not updated if registration failed', async () => {

        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientLoginFunctionFailed(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.register(loginRequest);

        expect(authenticationService.isAuthenticated).toBe(false);
        expect(authenticationService.authenticationToken).toBe(null);
    });

    it('Authentication token is not updated if api client throws exception during registration', async () => {

        const currentUserService = new CurrentUserService();
        const config = new AppConfig();
        const apiClient = new ApiClientUnauthenticated(config);

        currentUserService.setUser = req => null;
        apiClient.post = req => apiClientError(req);

        const authenticationService = new AuthenticationServiceImpl(apiClient, currentUserService);
        await authenticationService.register(loginRequest);

        expect(authenticationService.isAuthenticated).toBe(false);
        expect(authenticationService.authenticationToken).toBe(null);
    });



});