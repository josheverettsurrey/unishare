
import {ServerConnectivityEvent, ServerConnectivityService} from 'unishare/src/services/server_connectivity_service.ts';

describe('[ServerConnectivityService]', () => {

    it('Stream should be disposed of when dispose is run', () => {
        const serverConnectivityService = new ServerConnectivityService();
        expect(serverConnectivityService._isDisposed).toBe(false);
        serverConnectivityService.dispose();
        expect(serverConnectivityService._isDisposed).toBe(true);
    });

});