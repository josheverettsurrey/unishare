
import {CurrentUserService} from 'unishare/src/services/current_user_service.ts';

import { User } from "../../src/entities/core/user";
import { University } from "../../src/entities/core/university";

describe('[CurrentUserService]', () => {

    const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));

    it('Retrieving the user should result in undefined initially', () => {
        const currentUserService = new CurrentUserService();
        
        expect(currentUserService.user).toBe(undefined);
    });

    // can't test retrieving user without setting it first as it uses browser storage
    it('Should be able to set the user and retrieve it', () => {
        const currentUserService = new CurrentUserService();

        currentUserService.setUser(mockUser);

        expect(currentUserService.user).toBe(mockUser);
    });

    it('Should be able to retrieve user stream controller', () => {
        const currentUserService = new CurrentUserService();
        
        expect(currentUserService.userStreamController).toBeDefined();
        
    });

});