import {DocumentService} from '../../src/services/document_service';
import { DataResponse } from "src/entities/data/data_response";
import {SearchResponse} from "../../src/entities/responses/search_response";
import {UploadRequest} from "../../src/entities/requests/upload_request";
import {UploadResponse} from "../../src/entities/responses/upload_response";
import {FetchRequest} from "../../src/entities/requests/fetch_request";
import {FetchResponse} from "../../src/entities/responses/fetch_response";
import { ApiClientAuthenticated,ApiClientUnauthenticated } from "../../src/entities/api_clients/api_client_base";
import { AuthenticationServiceImpl } from "../../src/services/authentication_service";
import { AppConfig } from "../../src/entities/config/config";
import {UnishareDocument} from "../../src/entities/core/unishare_document";
import {Blob} from "buffer";
import { User } from 'src/entities/core/user';
import { University } from 'src/entities/core/university';


describe('[DocumentService]', () => {

    const someValues = [{ name: 'test user' }];
    const str = JSON.stringify(someValues);
    const mockUser = new User('email@email.com', 'test', new University('University of Surrey', 'surrey.ac.uk'));
    const file = new Blob([str], { type: 'application/json' });

    const unishareDocument = new UnishareDocument({title: "Test Title", description: "Test Description",  creator: mockUser, uuid: "1", id: "1", university: "University of Surrey"});
    
    // The search response returns a Unishare Document, in this case we've used a mock document that only has an id associated with it.

    const uploadDocumentSuccess = {"_data": {"message": {"message": "Successfully uploaded document."}}};
    const documentServiceUploadSuccess = jest.fn().mockImplementation(() => {
        return DataResponse.success(new UploadResponse("Successfully uploaded document."))
    });

    const uploadDocumentFailure = {"_failure": "ServerError - We ran into a problem on our end. Please try again later."};
    const documentServiceUploadFailure = jest.fn().mockImplementation(() => {
        return DataResponse.failure("Failed to upload document.")
    });

    // As this is just a test fetch, the document returned will be undefined.
    // The fetch response returns a Unishare Document and the PDF file, as this is for testing purposes we've left the document undefined and the file is an object that contains a response message
   
    const mockSearchFailureResponse = {
        "data": {
            "error": "search_error",
            "message": "test_message"
        },
        status: 400,
        statusText: 'Not good!',
        headers: {},
        config: {},
    }

    const mockSearchSuccessResponse = {
        "data": {
            "payload": {
                "results": [{
                    
                    "title": 'Test Title',
                    "description": 'Test Description',
                    "creator": {
                        "email": 'email@email.com',
                        "displayName": 'test',
                        "university": {
                            "university": 'University of Surrey',
                            "domain": 'surrey.ac.uk'
                        }
                    },
                    "uuid": '1',
                    "id": '1',
                    "university": {"university": "University of Surrey"},
                }],
            }
        },
        status: 200,
        statusText: 'All good',
        headers: {},
        config: {},
    };
    const mockFetchSuccessResponse = {
        "data": {
            "payload": {
                "document": {
                    "title": 'Test Title',
                    "description": 'Test Description',
                    "creator": {
                        "email": 'email@email.com',
                        "displayName": 'test',
                        "university": {
                            "university": 'University of Surrey',
                            "domain": 'surrey.ac.uk'
                        }
                    },
                    "uuid": '1',
                    "id": '1',
                    "university": {"university": "University of Surrey"},
                },
                "file": file
            }
        },
        status: 200,
        statusText: 'All good',
        headers: {},
        config: {},
    };

    const mockUploadSuccessResponse = {
        "data": {
            "payload": {
                "message": "Upload complete"
            }
        },
        status: 200,
        statusText: 'All good',
        headers: {},
        config: {},
    };
    
    const mockFailureResponse = {
        "_failure": "ServerError - We ran into a problem on our end. Please try again later.",
    }

    const ApiClientSearchSuccess = jest.fn().mockImplementation(() => mockSearchSuccessResponse);
    
    const ApiClientFetchSuccess = jest.fn().mockImplementation(() => mockFetchSuccessResponse
    );
    const ApiClientFetchFailure = jest.fn().mockImplementation(() => mockFailureResponse);

    const ApiClientUploadSuccess = jest.fn().mockImplementation(() => mockUploadSuccessResponse
    );

    // test search success and failure
    it('Should return a successful search response', async () => {

        const config = {
            baseUrl: 'http://localhost:3000',
        } ;
        const apiClient = new  ApiClientAuthenticated(config);
        apiClient.get = req => ApiClientSearchSuccess(req);
        const documentService = new DocumentService(apiClient);
        
        const result = await documentService.search("TestQuery");
        expect(result.isSuccessful).toBe(true);
        expect(result.content.results.length).toBe(1);
    });

    it('Should return a failed search response when an error arises', async () => {

        const config = {
            baseUrl: 'http://localhost:3000',
        } ;
        const apiClient = new  ApiClientAuthenticated(config);
        apiClient.get = req => ApiClientFetchFailure(req);
        const documentService = new DocumentService(apiClient);
        const result = await documentService.search("TestQuery");

        expect(result.isSuccessful).toBe(false);
    });

    // test upload success and failure
    it('Should return a successful upload response', async () => {

        const config = new AppConfig();
        const apiClient = new ApiClientAuthenticated(config);
        apiClient.file = req => ApiClientUploadSuccess(req);
        const documentService = new DocumentService(apiClient);
        
        const someValues = [{ name: 'test user' }];
        const str = JSON.stringify(someValues);
        const file = new Blob([str], {type: 'application/pdf'});
        
        const uploadRequest = new UploadRequest({title: "Test Title", description: "Test Description", moduleCode: "Test Module Code", file: file});
        const result = await documentService.upload(uploadRequest);

        const expected = UploadResponse.fromJson({
            "message": "Upload complete"
        })

        expect(result.content.message).toEqual(expected.message);
    });

    it('Should return a failed upload response when an error arises', async () => {

        const documentService = new DocumentService();
        documentService.file = req => documentServiceUploadFailure(req);
        
        const someValues = [{ name: 'test user' }];
        const str = JSON.stringify(someValues);
        const file = new Blob([str], {type: 'application/pdf'});
        
        
        const uploadRequest = new UploadRequest({title: "Test Title", description: "Test Description", moduleCode: "Test Module Code", file: file});
        
        const result = await documentService.upload(uploadRequest);

        expect(result).toEqual(uploadDocumentFailure);
    });

    // // test fetch success and failure
    it('Should return a successful fetch response', async () => {

        const config = new AppConfig();
        const apiClient = new ApiClientAuthenticated(config);
        apiClient.post = req => ApiClientFetchSuccess(req);

        const documentService = new DocumentService(apiClient);
        
        const fetchRequest = new FetchRequest("TestId");
        const result = await documentService.fetch(fetchRequest);
        const expected =  FetchResponse.fromJson({
            "document": {
                "title": 'Test Title',
                "description": 'Test Description',
                "creator": {
                    "email": 'email@email.com',
                    "displayName": 'test',
                    "university": {
                        "university": 'University of Surrey',
                        "domain": 'surrey.ac.uk'
                    }
                },
                "uuid": '1',
                "id": '1',
                "university": {"university": "University of Surrey"},
            },
            "file": file
        });        
        
        expect(result.content.document.title).toBe(expected.document.title);
    });

    it('Should return a failed fetch response when an error arises', async () => {

        const config = new AppConfig();
        const apiClient = new ApiClientAuthenticated(config);
        apiClient.post = req => ApiClientFetchFailure(req);
        const documentService = new DocumentService(apiClient);
        
        const fetchRequest = new FetchRequest("TestId");
        const result = await documentService.fetch(fetchRequest);

        expect(result.isSuccessful).toBe(false);
    });

});