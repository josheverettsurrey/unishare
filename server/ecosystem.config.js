module.exports = {
    apps: [
        {
            name: 'unishare-server',
            script: 'yarn',
            args: 'start',
            watch: '.',
            env: {
                NODE_ENV: 'production',
            },
        },
    ],

    deploy: {
        // production: {
        //     user: 'app_runner',
        //     host: 'apollo-1-runner',
        //     ref: 'origin/master',
        //     repo: 'git@bitbucket',
        //     path: '/home/app_runner/unishare/',
        //     'pre-deploy-local': '',
        //     'post-deploy':
        //         'cd server && yarn && pm2 reload ecosystem.config.js --env production',
        //     'pre-setup': '',
        // },
    },
};
