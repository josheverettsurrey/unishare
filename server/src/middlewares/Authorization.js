"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OnlyAuthorized = exports.MaybeAuthorized = void 0;
const Session_1 = require("../models/Session");
async function MaybeAuthorized(ctx, next) {
    const token = ctx.headers['authorization'];
    await _checkAuthorization(ctx, token);
    return next();
}
exports.MaybeAuthorized = MaybeAuthorized;
async function OnlyAuthorized(ctx, next) {
    const token = ctx.headers['authorization'];
    if (await _checkAuthorization(ctx, token)) {
        return next();
    }
    return ctx.error(401, 'ERR_UNAUTHORIZED', 'You must be logged in to do that.');
}
exports.OnlyAuthorized = OnlyAuthorized;
async function _checkAuthorization(ctx, token) {
    const sessionRepo = ctx.getEntityManager().getRepository(Session_1.Session);
    token = token?.replace('Bearer ', '');
    const session = await sessionRepo.findOne({ token }, ['user']);
    if (!session || !session.user) {
        return false;
    }
    ctx.session = session;
    ctx.user = session.user;
    return true;
}
