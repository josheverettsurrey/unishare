"use strict";
/* eslint-disable no-console */
Object.defineProperty(exports, "__esModule", { value: true });
exports.WebSocketHandler = void 0;
const cinnamon_1 = require("@apollosoftwarexyz/cinnamon");
const mongodb_1 = require("@mikro-orm/mongodb");
const Session_1 = require("../models/Session");
const ChatMessage_1 = require("../models/ChatMessage");
const ChatRoom_1 = require("../models/ChatRoom");
class ActiveRoom {
    constructor() {
        this.connectedSession = new Set();
    }
    get connectedSessionArray() {
        return Array.from(this.connectedSession);
    }
    newSession(connectedSession) {
        // Check if there is a connected socket with the same user that
        // is connecting to the active room.
        this.connectedSession.forEach((connectedSocket) => {
            if (connectedSocket.user._id.toString() == connectedSession.user._id.toString()) {
                this.connectedSession.delete(connectedSocket);
            }
        });
        // Then just add the new connected socket
        this.connectedSession.add(connectedSession);
    }
    getConnectedSession(socket) {
        for (let i = 0; i < this.connectedSessionArray.length; i++) {
            // Check that the sockets are the same
            if (this.connectedSessionArray[i].socket == socket) {
                return this.connectedSessionArray[i];
            }
        }
    }
}
class WebSocketHandler {
    constructor(framework) {
        // Room id to an active room
        this.rooms = new Map();
        this.framework = framework;
    }
    async routeHandler(socket, request) {
        socket.on('message', async (msg) => {
            const message = JSON.parse(msg);
            switch (message.type) {
                case 'CONNECT':
                    await this.handleConnectionMessage(message, socket);
                    break;
                case 'MESSAGE':
                    await this.handleMessageMessage(message, socket);
                    break;
                default:
                    break;
            }
        });
    }
    async handleConnectionMessage(message, socket) {
        /** Authenticate the user first*/
        // Get the token from the message
        const token = message.token;
        // If the token is null then they are not authenticated
        if (token == null) {
            await socket.send(JSON.stringify({
                type: 'ERROR',
                failure: 'You are using the chat without logging in. Please log in and come back.'
            }), () => socket.close());
            return;
        }
        const em = await this.framework.getModule(cinnamon_1.Database.prototype).em;
        const sessionRepo = await em.getRepository(Session_1.Session);
        const session = await sessionRepo.findOne({ token: token });
        if (session == null) {
            await socket.send(JSON.stringify({
                type: 'ERROR',
                failure: 'You are using the chat without logging in. Please log in and come back.'
            }), () => socket.close());
            return;
        }
        const roomId = new mongodb_1.ObjectId(message.roomId[0]);
        const roomRepo = await em.getRepository(ChatRoom_1.ChatRoom);
        const chatRoom = await roomRepo.findOne({ _id: roomId });
        if (chatRoom == null) {
            await socket.send(JSON.stringify({
                type: 'ERROR',
                failure: 'Could not find chat room.',
            }), () => socket.close());
            return;
        }
        let activeRoom = this.rooms.get(message.roomId[0]);
        // If there is no active room then we need to create one
        if (activeRoom == null) {
            this.rooms.set(message.roomId[0], new ActiveRoom());
            activeRoom = this.rooms.get(message.roomId[0]);
        }
        // Adding the new connected session to the correct active room.
        activeRoom.newSession({
            socket,
            user: session.user,
            room: chatRoom,
        });
        // Get the other members of the room.
        const members = this.rooms.get(message.roomId[0]).connectedSessionArray;
        // Connect then send an update with the messages
        socket.send(JSON.stringify({
            type: 'CONNECTED',
            room: roomId.toString(),
            members: members.map((member) => member.user),
        }));
        const messagesRepo = em.getRepository(ChatMessage_1.ChatMessage);
        const messages = await messagesRepo.find({ room: roomId }, { populate: ['sender'] });
        socket.send(JSON.stringify({
            type: 'UPDATE',
            messages: messages
        }));
    }
    async handleMessageMessage(message, socket) {
        if (message.body == null || message.roomId[0] == null) {
            socket.send(JSON.stringify({
                type: 'ERROR',
                failure: 'Message did not have a body or the room id was null',
            }));
            return;
        }
        const activeRoom = this.rooms.get(message.roomId[0]);
        if (activeRoom == null || activeRoom.connectedSession.size == 0) {
            socket.send(JSON.stringify({
                type: 'ERROR',
                failure: 'The room you were looking for has not been created or has no participants.',
            }));
            return;
        }
        // Let's get the user that is sending the message
        const sender = activeRoom.getConnectedSession(socket);
        if (sender == null) {
            socket.send(JSON.stringify({
                type: 'ERROR',
                failure: 'Could not find the sender for the message',
            }));
            return;
        }
        const messageModel = new ChatMessage_1.ChatMessage({ sender: sender.user, body: message.body, room: sender.room });
        const repo = await this.framework.getModule(cinnamon_1.Database.prototype).em.getRepository(ChatMessage_1.ChatMessage);
        const m = await repo.create(messageModel);
        await repo.persistAndFlush(m);
        const populatedMessage = await repo.populate(m, ['sender']);
        for (let i = 0; i < activeRoom.connectedSessionArray.length; i++) {
            activeRoom.connectedSessionArray[i].socket.send(JSON.stringify({
                type: 'UPDATE',
                messages: [populatedMessage],
            }));
        }
    }
}
exports.WebSocketHandler = WebSocketHandler;
