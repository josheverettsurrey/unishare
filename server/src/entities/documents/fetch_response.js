"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.FetchResponse = void 0;
class FetchResponse {
    constructor(success, file) {
        this.file = file;
        this.success = success;
    }
}
exports.FetchResponse = FetchResponse;
