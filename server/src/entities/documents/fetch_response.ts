export class FetchResponse {
    public file?: string;
    public success: boolean;
    constructor(success: boolean,file: any) {
        this.file = file;
        this.success = success;
    }

}
