export class UploadResponse {

    public success: boolean;
    public wasCharged: boolean;

    constructor(success: boolean, wasCharged = false) {
        this.success = success;
        this.wasCharged = wasCharged;
    }

}
