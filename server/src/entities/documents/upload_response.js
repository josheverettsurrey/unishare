"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadResponse = void 0;
class UploadResponse {
    constructor(success, wasCharged = false) {
        this.success = success;
        this.wasCharged = wasCharged;
    }
}
exports.UploadResponse = UploadResponse;
