"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cinnamon_1 = __importDefault(require("@apollosoftwarexyz/cinnamon"));
const cinnamon_plugin_asl_protocol_1 = require("@apollosoftwarexyz/cinnamon-plugin-asl-protocol");
const cinnamon_plugin_cors_1 = require("@apollosoftwarexyz/cinnamon-plugin-cors");
const utils_1 = require("./utils");
const chat_1 = require("./realtime/chat");
const WebsocketPlugin_1 = __importDefault(require("./plugins/WebsocketPlugin"));
(async () => {
    await cinnamon_1.default.initialize({
        async load(framework) {
            framework.use(new cinnamon_plugin_asl_protocol_1.ApolloProtocol(framework));
            framework.use(new cinnamon_plugin_cors_1.CinnamonCors(framework));
            framework.use(new WebsocketPlugin_1.default({ framework: framework, handler: new chat_1.WebSocketHandler(framework) }));
            await utils_1.universityRepository.initialize();
        }
    });
})();
