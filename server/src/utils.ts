import { UniversityRepository } from './utils/university_repository';
import { argon2id, Options as Argon2HashingOptions } from 'argon2';

export const uniShareHashingOptions: Argon2HashingOptions & { raw?: false } = {
    type: argon2id,
    timeCost: 6,
    parallelism: 6,
    hashLength: 64,
    memoryCost: 8192,
    raw: false,
};

export const uniShareRawHashingOptions: Argon2HashingOptions & { raw: true } = {
    ...uniShareHashingOptions,
    raw: true
};


export const universityRepository: UniversityRepository = new UniversityRepository();
