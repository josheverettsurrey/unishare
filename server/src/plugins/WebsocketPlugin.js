"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cinnamon_1 = require("@apollosoftwarexyz/cinnamon");
const koa_websocket_1 = __importDefault(require("koa-websocket"));
class WebsocketPlugin extends cinnamon_1.CinnamonPlugin {
    constructor(params) {
        super(params.framework, WebsocketPlugin.__organisation, WebsocketPlugin.__name);
        this.handler = params.handler;
    }
    /**
     * On initialize we will just wrap the WebServers Koa instance with
     * koa-websocket and register all the websocket routes that are
     * passed into the constructor.

     */
    async onInitialize() {
        return;
    }
    async beforeRegisterControllers() {
        const webServer = this.framework.getModule(cinnamon_1.WebServer.prototype).server;
        this.websocketServer = (0, koa_websocket_1.default)(webServer);
        this.websocketServer.ws.onConnection = async (socket, message) => {
            await this.handler.routeHandler(socket, message);
        };
    }
    async afterRegisterControllers() {
        return;
    }
}
exports.default = WebsocketPlugin;
WebsocketPlugin.__organisation = 'JoshEverett';
WebsocketPlugin.__name = 'WebsocketPlugin';
