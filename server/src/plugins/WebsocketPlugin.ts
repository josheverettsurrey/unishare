import Cinnamon, {
    CinnamonPlugin,
    CinnamonWebServerModulePlugin, Context,
    Koa,
    Logger,
    WebServer
} from '@apollosoftwarexyz/cinnamon';
import {DefaultContext, DefaultState, Middleware} from 'koa';
import {WebSocketHandler} from '../realtime/chat';
import websockify from 'koa-websocket';
import * as console from 'console';
import {ChatMessage} from '../models/ChatMessage';

interface WebSocketPluginParams {
    framework: Cinnamon;
    handler: WebSocketHandler;
}

export default class WebsocketPlugin extends CinnamonPlugin implements CinnamonWebServerModulePlugin {
    private static readonly __organisation: string = 'JoshEverett';
    private static readonly __name: string = 'WebsocketPlugin';

    private readonly handler: WebSocketHandler;

    /**
     * Instance of the Koa webserver wrapped with koa-websocket.
     * This is initialized on the onInitialize function
     */
    public websocketServer?:  websockify.App;


    constructor(params: WebSocketPluginParams) {
        super(params.framework, WebsocketPlugin.__organisation, WebsocketPlugin.__name);
        this.handler = params.handler;
    }

    /**
     * On initialize we will just wrap the WebServers Koa instance with
     * koa-websocket and register all the websocket routes that are
     * passed into the constructor.

     */
    async onInitialize(): Promise<boolean | void> {

        return;
    }

    async beforeRegisterControllers(): Promise<void> {
        const webServer=this.framework.getModule<WebServer>(WebServer.prototype).server ;

        this.websocketServer = websockify(webServer);

        this.websocketServer.ws.onConnection = async (socket, message) => {
            await this.handler.routeHandler(socket, message);
        };
    }

    async afterRegisterControllers(): Promise<void>{
        return;
    }

}
