"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const cinnamon_1 = require("@apollosoftwarexyz/cinnamon");
let IndexController = class IndexController {
    async index(ctx) {
        ctx.redirect(cinnamon_1.Config.get('default_redirect_uri'));
    }
};
__decorate([
    (0, cinnamon_1.Route)(cinnamon_1.Method.GET, '/')
], IndexController.prototype, "index", null);
IndexController = __decorate([
    (0, cinnamon_1.Controller)('/')
], IndexController);
exports.default = IndexController;
