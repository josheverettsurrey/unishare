/* eslint-disable no-console */
import {$, Body, Context, Controller, Method, Middleware, Route} from '@apollosoftwarexyz/cinnamon';
import { EntityRepository, wrap } from '@mikro-orm/core';
import { User } from '../../models/User';
import { Session } from '../../models/Session';
import argon2 from 'argon2';
import { uniShareHashingOptions, universityRepository } from '../../utils';
import { OnlyAuthorized } from '../../middlewares/Authorization';
import { UniversityEntry } from '../../utils/university_repository';

@Controller('v1', 'auth')
export default class AuthController {


    @Middleware(Body())
    @Route(Method.POST, '/login')
    public async login(ctx: Context): Promise<void> {

        // Get and validate the email and the password from the request body.
        // const [validationStatus, { email, password }] = $({
        //     email: {
        //         type: 'string',
        //         required: true,
        //         minLength: 1,
        //         maxLength: 20,
        //         matches: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/
        //     },
        //     password: {
        //         type: 'string',
        //         required: true,
        //     },
        // }).validate(ctx.request.body);
        const {email, password } = ctx.request.body;

        // If the validation fails, exit early with an error.
        // if (!validationStatus.success) {
        //     return ctx.error(400, 'ERR_INVALID_DATA', validationStatus.message);
        // }

        if (!email.includes('.ac.uk')) {
            return ctx.error(400, 'ERR_INVALID_DATA', 'Please use your university email address.');
        }

        // Load the user repository.
        const userRepo = ctx.getEntityManager()!.getRepository(User);

        // Fetch the user from the user repository.
        const user = await userRepo.findOne({ email }, [
        ]);
        if (user == null) {

            return ctx.error(
                401,
                'ERR_UNAUTHORIZED',
                'You entered an invalid email or password combination.'
            );
        }

        // Verify the entered password against the password in the database.
        try {
            if (
                !(await argon2.verify(
                    user.passwordHash,
                    password,
                    uniShareHashingOptions
                ))
            ) {
                return ctx.error(
                    401,
                    'ERR_UNAUTHORIZED',
                    'You entered an invalid email or password combination.'
                );
            }
        } catch (ex) {
            return ctx.error(
                401,
                'ERR_UNAUTHORIZED',
                'There was a problem checking your password. If the issue persists, please get in touch.'
            );
        }

        // If we're here, all the above checks were valid, so let's start a
        // session for the current user and return the details.
        const session = await AuthController._startSession(
            ctx,
            user,
        );

        return ctx.success({
            user: wrap(user).toJSON(),
            token: { authToken: session.token, expiresIn: 0 },
        });
    }

    @Middleware(OnlyAuthorized)
    @Middleware(Body())
    @Route(Method.POST, '/session')
    public async session(ctx: Context): Promise<void> {


        const session: Session | null = ctx.session;
        const user = session?.user;

        if (user == null) {
            return ctx.error(401, 'ERR_UNAUTHORIZED', 'You are not logged in.');
        }

        const newSession = await AuthController._startSession(
            ctx,
            user,
        );

        if (newSession == null) {
            return ctx.error(500, 'ERR_UNKNOWN');
        }

        return ctx.success({ token: { authToken: newSession.token, expiresIn: 0 }, });

    }

    @Middleware(Body())
    @Route(Method.POST, '/register')
    public async register(ctx: Context): Promise<void> {
        // Get and validate the user details from the request body.
        const [validationStatus, requestPayload] = $({
            user: {
                email: {
                    type: 'string',
                    required: true,
                    minLength: 1,
                    maxLength: 20,
                    matches: /[a-z0-9]+@[a-z]+\.[a-z]{2,3}/
                },
                displayName: {
                    type: 'string',
                    required: true,
                    maxLength: 30,
                },
            },
            password: {
                type: 'string',
                required: true,
                minLength: 8,
                maxLength: 60,
            },
        }).validate(ctx.request.body);

        // If the validation fails, exit early with an error.
        if (!validationStatus.success) {
            return ctx.error(400, 'ERR_INVALID_DATA', validationStatus.message);
        }

        // Otherwise, extract the useful data from the request.
        const {
            user: { email, displayName },
            password,
        } = requestPayload;

        const domain = '@' + ctx.request.body.user.email.split('@')[1];

        const university: UniversityEntry | undefined = universityRepository.getUniversity(domain);

        if (!university) {
            return ctx.error(400, 'ERR_INVALID_DATA', 'Please use a valid university address.');
        }

        const userRepo: EntityRepository<User> = ctx
            .getEntityManager()!
            .getRepository(User);

        if ((await userRepo.count({ email })) > 0) {
            return ctx.error(
                403, 'ERR_NOT_PERMITTED',
                'A user already exists with that email.'
            );
        }

        const userModel = new User({
            email,
            displayName,
            passwordHash: await argon2.hash(password, uniShareHashingOptions),
            university: university,
        });

        const user = await userRepo.create(userModel);
        await userRepo.persistAndFlush(user);

        const session = await AuthController._startSession(
            ctx,
            user,
        );
        return ctx.success({
            user,
            token: { authToken: session.token, expiresIn: 0 },
            message: 'Your account was created successfully. Welcome to UniShare!',
        });
    }

    private static async _startSession(
        ctx: Context,
        user: User,
    ): Promise<Session> {
        const ipAddress = (ctx.headers['cf-connecting-ip'] as string)
            ?? ctx.ip;

        const sessionRepo: EntityRepository<Session> = ctx
            .getEntityManager()!
            .getRepository(Session);

        // Create a new session for the user.
        const session = new Session({
            user,
            ipAddress,
        });

        // Delete any existing sessions for the user, and persist the new
        // session to the database.
        await sessionRepo.nativeDelete({ user });
        await sessionRepo.persist(sessionRepo.create(session)).flush();
        return session;
    }

}
