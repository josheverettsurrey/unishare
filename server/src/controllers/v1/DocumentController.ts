import {Body, Config, Context, Controller, Method, Middleware, Route} from '@apollosoftwarexyz/cinnamon';
import {UnishareDocument} from '../../models/UnishareDocument';
import FuzzySearch from 'fuzzy-search';
import {wrap} from '@mikro-orm/core';
import {OnlyAuthorized} from '../../middlewares/Authorization';
import fs from 'fs';
import {fetchFile, uploadFileToBucket} from '../../utils/s3Client';
import multer from '@koa/multer';
import os from 'os';
import {EntityRepository} from '@mikro-orm/core';
const upload = multer({ dest: os.tmpdir() });

import {GetObjectCommandInput} from '@aws-sdk/client-s3';
import {ObjectId} from '@mikro-orm/mongodb';

@Controller('v1', '/document')
export default class DocumentController{

    @Middleware(Body())
    @Route(Method.GET, '/search')
    public async search(ctx: Context): Promise<void> {
        const criteria: string = ctx.request.query['query'] as string;

        if (!criteria ) {
            return ctx.success({
                results: [],
            });
        }

        const entityManager = ctx.getEntityManager()!;
        const documents = await entityManager.getRepository(UnishareDocument).findAll({ populate: ['creator']});

        // Use the fuzzy search to order usernames in order of relevance
        const searcher = new FuzzySearch(documents, ['title'], {
            sort: false,
        });

        const results = searcher.search(criteria);

        return ctx.success({
            results: results.map((res) => wrap(res)),
        });
    }

    @Middleware(upload.single('file'))
    @Middleware(Body())
    @Middleware(OnlyAuthorized)
    @Route(Method.POST, '/upload')
    public async upload(ctx: Context): Promise<void> {

        const {title, description} : {title: string, description?: string} = ctx.request.body;

        const file = ctx.file;

        if (!file) {
            return ctx.error(
                400,
                'ERR_INVALID_DATA',
                'No file was uploaded'
            );
        }

        // If file greater than 50mb
        if (file.size > 52428800) {
            return ctx.error(404, 'ERR_INVALID_DATA', 'File is too large');
        }

        const actualFile = fs.readFileSync(file.path);

        if (!actualFile) {
            return ctx.error(
                400,
                'ERR_INVALID_DATA',
                'File was not found or was corrupted - please upload again'
            );
        }

        // Before we upload the file to the Amazon s3 bucket
        // we want to cache it in the database.

        // We can assume that the user is not null as this is an authenticated route.
        // The fetch will contain the [String] id of the user that uploaded it, the title and description
        // that was sent to the server with the file.

        const unishareDocument = new UnishareDocument({
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            // @ts-ignore
            creator: ctx.user!._id,
            description: description,
            title: title,
            university: ctx.user!.university.university,
        });

        if (unishareDocument == null) {
            return ctx.error(400, 'UNABLE_TO_CREATE', 'We were unable to store your fetch. Please try again.');
        }

        const bucketParams = {
            // Name of the bucket the file is uploaded to
            Bucket: Config.get('s3_bucket'),
            // UUID of the file so that it is unique.
            Key: unishareDocument.uuid + '.pdf',
            // Content of the new object
            Body: actualFile,
        };


        const response = await uploadFileToBucket(bucketParams);

        if (!response.success) {
            return ctx.error(400, 'ERR_INVALID_DATA', 'We had trouble uploading your file. Please try again.');
        }

        const docRepo : EntityRepository<UnishareDocument> = ctx.getEntityManager()!.getRepository(UnishareDocument);
        await docRepo.persistAndFlush(docRepo.create(unishareDocument));

        ctx.success({
            message: 'Upload Successful',
        });
    }

    @Middleware(Body())
    @Route(Method.POST, '/fetch')
    public async fetch(ctx: Context) : Promise<void> {
        const {id} = ctx.request.body;

        if (id == null){
            return ctx.error(400,'ERR_INVALID_DATA','Search criteria was empty' );
        }

        // get unishare fetch repo
        const repo = ctx.getEntityManager()!.getRepository(UnishareDocument);

        // Find a single fetch in the database with the id that has been requested to be fetched.
        const doc : UnishareDocument | null = await repo.findOne({_id: new ObjectId(id),}, { populate: ['creator']});

        // if there doesn't exist a file in the database with the uuid that we are searching for then we shouldn't
        // try and find it in the bucket as it probably doesn't exist.
        if (doc == null) {
            return ctx.error(400, 'ERR_INVALID_DATA', "We couldn't find this file in our database please try another file.");
        }

        const uploadParams : GetObjectCommandInput= {
            // Name of the bucket the file is uploaded to
            Bucket: Config.get('s3_bucket'),
            // UUID of the file so that it is unique.
            Key: doc.uuid + '.pdf',
        };

        // make the call to the aws-s3 bucket to retrieve the file.
        const response =  await fetchFile(uploadParams);

        // if the response was not successful then send an error to the client.
        if (!response.success) {
            return ctx.error(400, 'ERR_INVALID_DATA', "We couldn't find this file in our file store.");
        }

        // response was successful but the file was null
        if (response.file == null) {
            return ctx.error(400, 'ERR_INVALID_DATA', 'We made the request to get your file, and it worked ... but theres no file :(');
        }

        // if the response was successful we should have a file
        return ctx.success({
            document: wrap(doc),
            file: response.file,
        });
    }
}
