
import {Context, Body, Controller, Method, Middleware, Route, $} from '@apollosoftwarexyz/cinnamon';
import {OnlyAuthorized} from '../../middlewares/Authorization';
import {ChatRoom} from '../../models/ChatRoom';
import {wrap} from '@mikro-orm/core';
import * as console from 'console';

@Controller('v1', '/chat')
export default class ChatController {

    @Middleware(OnlyAuthorized)
    @Middleware(Body())
    @Route(Method.POST, '/create')
    public async create(ctx: Context) : Promise<void> {

        const [validationStatus, requestPayload] = $({
            name: {
                type: 'string',
                required: true,
                minLength: 1,
                maxLength: 10
            },
            moduleCode: {
                type: 'string',
                required: true,
                minLength: 1,
                maxLength: 10,
            },
        }).validate(ctx.request.body);


        // If the validation fails, exit early with an error.
        if (!validationStatus.success) {
            return ctx.error(400, 'ERR_INVALID_DATA', validationStatus.message);
        }

        // Otherwise, extract the useful data from the request.
        const { name, moduleCode } = ctx.request.body;

        // Get the repository
        const repo = ctx.getEntityManager()!.getRepository(ChatRoom);

        // Check if a room already exists for the module with the name given
        const existingRooms = await repo.find({name: name, module: moduleCode});

        // If there is a room / multiple rooms with the given name and module then we want to return an error.
        if (existingRooms != null && existingRooms.length > 0) {
            return ctx.error(400, 'ERR_ALREADY_EXIST', 'A room already exists with the name provided for that module.');
        }

        const university = ctx.user!.university.university;
        // Now we can make the group
        const roomModel = repo.create(new ChatRoom({name: name, module: moduleCode, university: university}));

        try{
            await repo.persistAndFlush(roomModel);
        }catch(e){
            return ctx.error(400, 'UNABLE TO CREATE ROOM', `We were unable to create a room with model ${roomModel}`);
        }

        // Now we can return all the groups, this now will include the newly created room.
        const allRooms = await ChatController.getAllRooms(ctx, university);

        return ctx.success({
            rooms: allRooms,
        });
    }

    @Middleware(OnlyAuthorized)
    @Route(Method.GET, '/fetch')
    public async fetch(ctx: Context) : Promise<void> {
        const university = ctx.user!.university.university;

        const allRooms =  await ChatController.getAllRooms(ctx, university);

        if (allRooms == null ) {
            return ctx.error(400, `Could not fetch the chat rooms for this university: ${university}`);
        }

        return ctx.success({
            rooms: allRooms,
        });

    }

    private static async getAllRooms(ctx: Context, university: string): Promise<ChatRoom[]>{
        return  await ctx.getEntityManager()!.getRepository(ChatRoom).findAll({university: university});
    }
}
