import { Body, Context, Controller, Method, Middleware, Route, } from '@apollosoftwarexyz/cinnamon';
import { OnlyAuthorized } from '../../middlewares/Authorization';
import { uniShareHashingOptions } from '../../utils';
import argon2 from 'argon2';

@Controller('v1', 'user')
export default class UserController {

    @Middleware(OnlyAuthorized)
    @Middleware(Body())
    @Route(Method.POST, '/update')
    public async update(ctx: Context): Promise<void> {

        // To keep track of whether we need to flush the DB with the new user data.
        let updated = false;

        // Get the user from the Middleware authentication
        const user = ctx.user!;

        // Get the display name and the password from the request body.
        const displayName = ctx.request.body.user._displayName;
        const password = ctx.request.body.user._password;

        // If the display name is any different from the current display name, update the user.
        if (user.displayName != displayName) {
            user.displayName = displayName;
            updated = true;
        }

        // If the password has been changed, update the user.
        if (password != null) {
            user.passwordHash = await argon2.hash(password, uniShareHashingOptions);
            updated = true;
        }

        // If there were any changes to the user then the user needs to be flushed to the DB.
        if (updated) {
            await ctx.getEntityManager()?.persistAndFlush(user!);
        }

        // Return the new user back in the response.
        return ctx.success({ user });
    }
}
