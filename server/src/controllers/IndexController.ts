import { Config, Context, Controller, Method, Route } from '@apollosoftwarexyz/cinnamon';

@Controller('/')
export default class IndexController {

    @Route(Method.GET, '/')
    public async index(ctx: Context): Promise<void> {

        ctx.redirect(Config.get('default_redirect_uri'));

    }

}
