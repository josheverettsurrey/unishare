
import file from './universities.json';

/**
 * Definition of a UniversityEntry that the user will have when the register
 * this provides the UserModel with the university and its domain which can be used in the
 * application.
 */
export class UniversityEntry {

    /**
     * The name of the university that the user goes to
     */
    public university: string;

    /**
     * The university's domain such as @surrey.ac.uk
     */
    public domain: string;
    constructor(university: string, domain: string) {
        this.university = university;
        this.domain = domain;
    }
}

export class UniversityRepository {

    // Map domain to university
    public universities: Map<string, UniversityEntry> = new Map();

    async initialize() {
        for (const entry of file) {
            this.universities.set(entry.key, new UniversityEntry(entry.value, entry.key));
        }
    }

    checkEmailExists(domain: string): boolean {
        return this.universities.has(domain);
    }

    getUniversity(domain: string): UniversityEntry | undefined {
        if (!this.universities.has(domain)) return undefined;
        return this.universities.get(domain);
    }

}
