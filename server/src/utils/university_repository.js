"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UniversityRepository = exports.UniversityEntry = void 0;
const universities_json_1 = __importDefault(require("./universities.json"));
/**
 * Definition of a UniversityEntry that the user will have when the register
 * this provides the UserModel with the university and its domain which can be used in the
 * application.
 */
class UniversityEntry {
    constructor(university, domain) {
        this.university = university;
        this.domain = domain;
    }
}
exports.UniversityEntry = UniversityEntry;
class UniversityRepository {
    constructor() {
        // Map domain to university
        this.universities = new Map();
    }
    async initialize() {
        for (const entry of universities_json_1.default) {
            this.universities.set(entry.key, new UniversityEntry(entry.value, entry.key));
        }
    }
    checkEmailExists(domain) {
        return this.universities.has(domain);
    }
    getUniversity(domain) {
        if (!this.universities.has(domain))
            return undefined;
        return this.universities.get(domain);
    }
}
exports.UniversityRepository = UniversityRepository;
