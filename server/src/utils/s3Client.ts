/* eslint-disable no-console */
import { Config } from '@apollosoftwarexyz/cinnamon';
import {
    GetObjectCommandInput,
    PutObjectCommand,
    PutObjectCommandInput,
    S3Client
} from '@aws-sdk/client-s3';
import * as AWS from 'aws-sdk';
import { UploadResponse } from '../entities/documents/upload_response';
import {FetchResponse} from '../entities/documents/fetch_response';


const REGION = Config.get('region');
const ACCESS_KEY = Config.get('unishare_access_key');
const SECRET_KEY = Config.get('unishare_secret_key');
AWS.config.update({
    region: REGION,
    credentials: {
        accessKeyId: ACCESS_KEY,
        secretAccessKey: SECRET_KEY
    },
    signatureVersion: 'v4',
});
const s3 = new AWS.S3();

export const s3Client = new S3Client({
    region: REGION,
    credentials: {
        accessKeyId: ACCESS_KEY,
        secretAccessKey: SECRET_KEY
    },
});

export async function uploadFileToBucket(uploadParams: PutObjectCommandInput): Promise<UploadResponse> {
    try {
        const data = await s3Client.send(new PutObjectCommand(uploadParams));
        return new UploadResponse(true, data.RequestCharged != null);
    } catch (e) {
        return new UploadResponse(false);
    }
}

export async function fetchFile(params: GetObjectCommandInput): Promise<FetchResponse> {

    try{
        const data = await s3.getSignedUrlPromise('getObject', params);
        return new FetchResponse(true, data);
    } catch(e){
        return new FetchResponse(false, null);
    }

}
