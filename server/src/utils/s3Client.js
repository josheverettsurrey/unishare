"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetchFile = exports.uploadFileToBucket = exports.s3Client = void 0;
/* eslint-disable no-console */
const cinnamon_1 = require("@apollosoftwarexyz/cinnamon");
const client_s3_1 = require("@aws-sdk/client-s3");
const AWS = __importStar(require("aws-sdk"));
const upload_response_1 = require("../entities/documents/upload_response");
const fetch_response_1 = require("../entities/documents/fetch_response");
const REGION = cinnamon_1.Config.get('region');
const ACCESS_KEY = cinnamon_1.Config.get('unishare_access_key');
const SECRET_KEY = cinnamon_1.Config.get('unishare_secret_key');
AWS.config.update({
    region: REGION,
    credentials: {
        accessKeyId: ACCESS_KEY,
        secretAccessKey: SECRET_KEY
    },
    signatureVersion: 'v4',
});
const s3 = new AWS.S3();
exports.s3Client = new client_s3_1.S3Client({
    region: REGION,
    credentials: {
        accessKeyId: ACCESS_KEY,
        secretAccessKey: SECRET_KEY
    },
});
async function uploadFileToBucket(uploadParams) {
    try {
        const data = await exports.s3Client.send(new client_s3_1.PutObjectCommand(uploadParams));
        return new upload_response_1.UploadResponse(true, data.RequestCharged != null);
    }
    catch (e) {
        return new upload_response_1.UploadResponse(false);
    }
}
exports.uploadFileToBucket = uploadFileToBucket;
async function fetchFile(params) {
    try {
        const data = await s3.getSignedUrlPromise('getObject', params);
        return new fetch_response_1.FetchResponse(true, data);
    }
    catch (e) {
        return new fetch_response_1.FetchResponse(false, null);
    }
}
exports.fetchFile = fetchFile;
