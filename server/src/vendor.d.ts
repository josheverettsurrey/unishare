import { Session } from './models/Session';
import { User } from './models/User';

declare module 'koa' {
    interface Context {

        session: Session?;
        user: User?;

    }
}
