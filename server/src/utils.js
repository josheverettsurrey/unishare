"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.universityRepository = exports.uniShareRawHashingOptions = exports.uniShareHashingOptions = void 0;
const university_repository_1 = require("./utils/university_repository");
const argon2_1 = require("argon2");
exports.uniShareHashingOptions = {
    type: argon2_1.argon2id,
    timeCost: 6,
    parallelism: 6,
    hashLength: 64,
    memoryCost: 8192,
    raw: false,
};
exports.uniShareRawHashingOptions = {
    ...exports.uniShareHashingOptions,
    raw: true
};
exports.universityRepository = new university_repository_1.UniversityRepository();
