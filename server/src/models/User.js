"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.User = void 0;
const core_1 = require("@mikro-orm/core");
const BaseEntity_1 = require("./BaseEntity");
const Session_1 = require("./Session");
let User = class User extends BaseEntity_1.TrackedBaseEntity {
    /**
     * Initializes a user object.
     * @param fields The fields to initialize on the user object.
     */
    constructor(fields) {
        super();
        this.email = fields.email;
        this.displayName = fields.displayName;
        this.passwordHash = fields.passwordHash;
        this.university = fields.university;
        if (fields.profilePicture)
            this.profilePicture = fields.profilePicture;
    }
    /**
     * Returns the user's display name, if it is set, otherwise returns their
     * email. This should be used anywhere one of the two is not
     * specifically desired.
     */
    get smartName() {
        return this.displayName ?? `${this.email}`;
    }
};
__decorate([
    (0, core_1.Property)(),
    (0, core_1.Unique)()
], User.prototype, "email", void 0);
__decorate([
    (0, core_1.Property)()
], User.prototype, "displayName", void 0);
__decorate([
    (0, core_1.Property)({ persist: false })
], User.prototype, "smartName", null);
__decorate([
    (0, core_1.Property)()
], User.prototype, "profilePicture", void 0);
__decorate([
    (0, core_1.Property)({ hidden: true })
], User.prototype, "passwordHash", void 0);
__decorate([
    (0, core_1.OneToOne)(() => Session_1.Session, (session) => session.user, { hidden: true })
], User.prototype, "currentSession", void 0);
__decorate([
    (0, core_1.Property)()
], User.prototype, "university", void 0);
User = __decorate([
    (0, core_1.Entity)()
], User);
exports.User = User;
