"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Session = void 0;
const BaseEntity_1 = require("./BaseEntity");
const core_1 = require("@mikro-orm/core");
const crypto_1 = require("crypto");
let Session = class Session extends BaseEntity_1.BaseEntity {
    constructor(fields) {
        super();
        /**
         * The session token passed to the client to authenticate
         * with the server.
         */
        this.token = (0, crypto_1.randomBytes)(256).toString('hex');
        /**
         * The date the session was started.
         */
        this.started = new Date();
        this.user = fields.user;
        if (fields.ipAddress)
            this.ipAddress = fields.ipAddress;
    }
};
__decorate([
    (0, core_1.OneToOne)()
], Session.prototype, "user", void 0);
__decorate([
    (0, core_1.Property)()
], Session.prototype, "ipAddress", void 0);
__decorate([
    (0, core_1.Property)(),
    (0, core_1.Unique)()
], Session.prototype, "token", void 0);
__decorate([
    (0, core_1.Property)()
], Session.prototype, "started", void 0);
Session = __decorate([
    (0, core_1.Entity)()
], Session);
exports.Session = Session;
