import {TrackedBaseEntity} from './BaseEntity';
import {Entity, ManyToOne, OneToOne, Property} from '@mikro-orm/core';
import {User} from './User';
import {ChatRoom} from './ChatRoom';

@Entity()
export class ChatMessage extends TrackedBaseEntity {

    /**
     * The body of the message
     */
    @Property()
    body: string;

    /**
     * The id of the user that uploaded the fetch.
     */
    @ManyToOne(() => User)
    sender: User;

    @ManyToOne(() => ChatRoom)
    room: ChatRoom;

    constructor(fields: {
        body: string;
        sender: User;
        room: ChatRoom;
    }) {
        super();
        this.body = fields.body;
        this.sender = fields.sender;
        this.room = fields.room;
    }
}
