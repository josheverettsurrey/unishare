"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrackedBaseEntity = exports.BaseEntity = void 0;
const core_1 = require("@mikro-orm/core");
/**
 * Defines a base set of properties that are common to more
 * or less any entity in the database.
 */
let BaseEntity = class BaseEntity {
};
__decorate([
    (0, core_1.PrimaryKey)()
], BaseEntity.prototype, "_id", void 0);
BaseEntity = __decorate([
    (0, core_1.Entity)({ abstract: true })
], BaseEntity);
exports.BaseEntity = BaseEntity;
/**
 * Similar to {@see BaseEntity}, however this class has
 * createdAt and updatedAt timestamps that are set and
 * updated automatically.
 */
let TrackedBaseEntity = class TrackedBaseEntity extends BaseEntity {
    constructor() {
        super(...arguments);
        this.createdAt = new Date();
        this.updatedAt = new Date();
    }
};
__decorate([
    (0, core_1.Property)()
], TrackedBaseEntity.prototype, "createdAt", void 0);
__decorate([
    (0, core_1.Property)({ onUpdate: () => new Date() })
], TrackedBaseEntity.prototype, "updatedAt", void 0);
TrackedBaseEntity = __decorate([
    (0, core_1.Entity)({ abstract: true })
], TrackedBaseEntity);
exports.TrackedBaseEntity = TrackedBaseEntity;
