"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UnishareDocument = void 0;
const core_1 = require("@mikro-orm/core");
const BaseEntity_1 = require("./BaseEntity");
const User_1 = require("./User");
let UnishareDocument = class UnishareDocument extends BaseEntity_1.TrackedBaseEntity {
    constructor(fields) {
        super();
        this.title = fields.title;
        this.description = fields.description;
        this.creator = fields.creator;
        this.university = fields.university;
        this.uuid = uuid();
    }
};
__decorate([
    (0, core_1.Property)()
], UnishareDocument.prototype, "title", void 0);
__decorate([
    (0, core_1.Property)()
], UnishareDocument.prototype, "description", void 0);
__decorate([
    (0, core_1.OneToOne)(() => User_1.User)
], UnishareDocument.prototype, "creator", void 0);
__decorate([
    (0, core_1.Property)()
], UnishareDocument.prototype, "uuid", void 0);
__decorate([
    (0, core_1.Property)()
], UnishareDocument.prototype, "university", void 0);
UnishareDocument = __decorate([
    (0, core_1.Entity)()
], UnishareDocument);
exports.UnishareDocument = UnishareDocument;
/**
 * Function to generate a new uuid for each document that is made so that we
 * have a unique id that is generated for each document that will be used as the key
 * for the file on AWS S3 bucket.
 */
function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
