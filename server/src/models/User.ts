import { Entity, OneToOne, Property, Unique } from '@mikro-orm/core';
import { UniversityEntry } from '../utils/university_repository';
import { TrackedBaseEntity } from './BaseEntity';
import { Session } from './Session';

@Entity()
export class User extends TrackedBaseEntity {

    /**
     * Unique to a user, and used to identify a user's profile when a
     * user is being searched for or during authentication.
     */
    @Property()
    @Unique()
    email!: string;

    /**
     * A nickname used to help a user be recognized by their friends.
     */
    @Property()
    displayName?: string;

    /**
     * Returns the user's display name, if it is set, otherwise returns their
     * email. This should be used anywhere one of the two is not
     * specifically desired.
     */
    @Property({ persist: false })
    get smartName(): string {
        return this.displayName ?? `${this.email}`;
    }

    /**
     * An optional profile picture URL. If set this is the full URL for
     * the user's profile picture.
     */
    @Property()
    profilePicture?: string;

    /**
     * The hashed password for the user.
     */
    @Property({ hidden: true })
    passwordHash: string;

    /**
     * The user's current session singleton (foreign key).
     * This is here as a convenience attribute.
     */
    @OneToOne(() => Session, (session) => session.user, { hidden: true })
    currentSession!: Session;

    @Property()
    university!: UniversityEntry;

    /**
     * Initializes a user object.
     * @param fields The fields to initialize on the user object.
     */
    constructor(fields: {
        email: string;
        displayName: string;
        passwordHash: string;
        profilePicture?: string;
        university: UniversityEntry;
    }) {
        super();
        this.email = fields.email;
        this.displayName = fields.displayName;
        this.passwordHash = fields.passwordHash;
        this.university = fields.university;

        if (fields.profilePicture) this.profilePicture = fields.profilePicture;
    }
}
