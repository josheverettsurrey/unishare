import { Entity, PrimaryKey, Property } from '@mikro-orm/core';
import { ObjectId } from '@mikro-orm/mongodb';

/**
 * Defines a base set of properties that are common to more
 * or less any entity in the database.
 */
@Entity({ abstract: true })
export abstract class BaseEntity {

    @PrimaryKey()
    _id!: ObjectId;

}

/**
 * Similar to {@see BaseEntity}, however this class has
 * createdAt and updatedAt timestamps that are set and
 * updated automatically.
 */
@Entity({ abstract: true })
export abstract class TrackedBaseEntity extends BaseEntity {

    @Property()
    createdAt = new Date();

    @Property({ onUpdate: () => new Date() })
    updatedAt = new Date();

}
