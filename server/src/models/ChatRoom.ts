import {TrackedBaseEntity} from './BaseEntity';
import {Entity, Property} from '@mikro-orm/core';

@Entity()
export class ChatRoom extends TrackedBaseEntity {

    /**
     * The name of the chat room.
     */
    @Property()
    name: string;

    /**
     * The module that the chat belongs to.
     */
    @Property()
    module: string;

    /**
     * The name of the university that the group is associated with
     */
    @Property()
    university: string;

    constructor(fields: {
        name: string;
        module: string;
        university: string;
    }) {
        super();
        this.name = fields.name;
        this.module = fields.module;
        this.university = fields.university;
    }
}
