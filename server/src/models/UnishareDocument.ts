import { Entity, ManyToOne, Property } from '@mikro-orm/core';
import { TrackedBaseEntity } from './BaseEntity';
import { User } from './User';

@Entity()
export class UnishareDocument extends TrackedBaseEntity {

    /**
     * The title of the fetch.
     */
    @Property()
    title: string;

    /**
     * A description of the fetch.
     */
    @Property()
    description?: string;

    /**
     * The id of the user that uploaded the fetch.
     */
    @ManyToOne()
    creator: User;

    @Property()
    uuid: string;

    @Property()
    university: string;

    constructor(fields: {
        title: string;
        description?: string;
        creator: User;
        university: string;
    }) {
        super();
        this.title = fields.title;
        this.description = fields.description;
        this.creator = fields.creator;
        this.university = fields.university;
        this.uuid = uuid();
    }
}

/**
 * Function to generate a new uuid for each document that is made so that we
 * have a unique id that is generated for each document that will be used as the key
 * for the file on AWS S3 bucket.
 */
function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
