"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ChatMessage = void 0;
const BaseEntity_1 = require("./BaseEntity");
const core_1 = require("@mikro-orm/core");
const User_1 = require("./User");
const ChatRoom_1 = require("./ChatRoom");
let ChatMessage = class ChatMessage extends BaseEntity_1.TrackedBaseEntity {
    constructor(fields) {
        super();
        this.body = fields.body;
        this.sender = fields.sender;
        this.room = fields.room;
    }
};
__decorate([
    (0, core_1.Property)()
], ChatMessage.prototype, "body", void 0);
__decorate([
    (0, core_1.ManyToOne)(() => User_1.User)
], ChatMessage.prototype, "sender", void 0);
__decorate([
    (0, core_1.ManyToOne)(() => ChatRoom_1.ChatRoom)
], ChatMessage.prototype, "room", void 0);
ChatMessage = __decorate([
    (0, core_1.Entity)()
], ChatMessage);
exports.ChatMessage = ChatMessage;
