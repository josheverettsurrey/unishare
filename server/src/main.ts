import Cinnamon from '@apollosoftwarexyz/cinnamon';
import { ApolloProtocol } from '@apollosoftwarexyz/cinnamon-plugin-asl-protocol';
import { CinnamonCors } from '@apollosoftwarexyz/cinnamon-plugin-cors';
import { universityRepository } from './utils';
import { WebSocketHandler } from './realtime/chat';
import WebsocketPlugin from './plugins/WebsocketPlugin';

(async () => {

    await Cinnamon.initialize({
        async load(framework) {

            framework.use(new ApolloProtocol(framework));

            framework.use(new CinnamonCors(framework, { allowCredentials: true, allowOrigins: '*', allowHeaders: '*', allowMethods: '*' }));

            framework.use(new WebsocketPlugin({ framework: framework, handler: new WebSocketHandler(framework) }));

            await universityRepository.initialize();
        }
    });

})();
